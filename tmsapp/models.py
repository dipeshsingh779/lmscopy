from umapp.models import TimeStamp, City, LogisticsBranch
from django.db import models

# Create your models here.


class TransportationCompany(TimeStamp):
    company_name = models.CharField(max_length=200)
    company_slug = models.SlugField(max_length=220)
    company_logo = models.ImageField(upload_to="company/logo")
    company_profile = models.ImageField(
        upload_to="company/profile", null=True, blank=True)
    company_description = models.TextField(null=True, blank=True)
    company_city = models.ForeignKey(City, on_delete=models.RESTRICT)
    company_website = models.URLField(null=True, blank=True)
    company_email = models.EmailField(null=True, blank=True)
    document1 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    document2 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    document3 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)

    def __str__(self):
        return self.company_name


class VehicleType(TimeStamp): # heavy or light
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="vehicles")
    capacity = models.CharField(max_length=20, help_text="Less than 1 Ton.")

    def __str__(self):
        return self.title


class LogisticsVehicleModel(TimeStamp): # vehicle model
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.CASCADE, null=True, blank=True)
    transport_company = models.ForeignKey(
        TransportationCompany, on_delete=models.CASCADE, null=True, blank=True)
    vehicle_type = models.ForeignKey(VehicleType, on_delete=models.RESTRICT)
    vehicle_model = models.CharField(max_length=50)
    other_details = models.TextField(null=True, blank=True)
    available_numbers = models.PositiveIntegerField(default=1)

    def __str__(self):
        return self.vehicle_model


class LogisticsVehicleCharge(TimeStamp):
    logistics_vehicle = models.ForeignKey(LogisticsVehicleModel, on_delete=models.CASCADE)
    km_from = models.PositiveIntegerField()
    km_to = models.PositiveIntegerField()
    charge = models.PositiveIntegerField()

    def __str__(self):
        return str(self.km_from) + " to " + str(self.km_to)


class LogisticsVehicle(TimeStamp):
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.CASCADE, null=True, blank=True)
    transport_company = models.ForeignKey(
        TransportationCompany, on_delete=models.CASCADE, null=True, blank=True)
    vehicle_type = models.ForeignKey(VehicleType, on_delete=models.RESTRICT)
    vehicle_model = models.ForeignKey(LogisticsVehicleModel, on_delete=models.RESTRICT)
    number_plate = models.CharField(max_length=50, unique=True)