from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import permissions
from django.core.mail import EmailMessage
import threading
from umapp.models import LogisticsCustomer



def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }


class OperatorOnlyPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        user = request.user
        try:
            operator = user.branchoperator
            if operator.status == "Active" and operator.logistics_branch.status == "Active":
                has_perm = True
            else:
                has_perm = False
        except Exception as e:
            print(e)
            has_perm = False
        return has_perm

class EmailThread(threading.Thread):
    
    def __init__(self, email):
        self.email = email
        threading.Thread.__init__(self)

    def run(self):
        self.email.send()


class Util:
    @staticmethod
    def send_email(data):
        email = EmailMessage(
            subject=data['email_subject'], body=data['email_body'], to=[data['to_email']])
        EmailThread(email).start()