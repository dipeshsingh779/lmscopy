from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from umapp.models import BranchOperator, City, LogisticsCompany, LogisticsBranch, LogisticsAccountType, DeliveryType, \
    LogisticsCustomer, LogisticsBranchShippingZone, DeliveryType, ShippingZoneDeliveryType, LogisticsCustomerContract, LogisticCustomerZonalContract
from lmsapp.models import CustomerSupportTicket, CustomerTicketFollowup, LogisticsNotice
from rest_framework import serializers
from django.contrib.auth.models import User
from django.utils.text import slugify
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.conf import settings
from django.core.mail import send_mail


class OperatorLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()


class OperatorProfileSerializer(serializers.ModelSerializer):
    email = serializers.SerializerMethodField()
    address_city = serializers.StringRelatedField()
    logistics_branch = serializers.StringRelatedField()

    class Meta:
        model =  BranchOperator
        fields = ["logistics_branch", "email", "full_name", "address_city", "address_city"]

    def get_email(self, obj):
        return obj.user.username


class OperatorProfileUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BranchOperator
        fields = ['full_name', 'image', 'mobile', 'alt_mobile', 'document1', 'document2', 'document3', 'address_city']


class CompanyRegistrationSerializer(serializers.ModelSerializer):
    branch_contact = serializers.IntegerField()
    branch_alternative_contact = serializers.IntegerField()
    branch_address = serializers.CharField()
    branch_email = serializers.EmailField()
    email = serializers.EmailField()
    password = serializers.CharField()
    full_name = serializers.CharField()
    class Meta:
        model = LogisticsCompany
        fields = ['company_name', 'company_city','company_logo', 'company_profile', 'company_description', 'company_website' , 'company_email', \
             'branch_contact', 'branch_alternative_contact', 'branch_email', 'branch_address', 'email', 'password', 'full_name']

    def create(self, validated_data):
        company_name = validated_data.get('company_name')
        company_city = validated_data.get("company_city")
        email = validated_data.get("email")
        if User.objects.filter(username=email).exists():
            raise serializers.ValidationError({'email':"email already exists"})
        else:
            user = User.objects.create_user(username = email, email=email)
        lc = LogisticsCompany.objects.create(company_name = company_name, company_city=company_city)
        lc.status = "Pending"
        lc.account_type = LogisticsAccountType.objects.first()
        lc.company_slug = slugify(lc.company_name)
        lc.save()
        branch_contact = validated_data.get('branch_contact')
        branch_alternative_contact = validated_data.get("branch_alternative_contact")
        branch_email = validated_data.get("branch_email")
        lc_branch = LogisticsBranch.objects.create(status="Pending",logistics_company=lc, branch_contact=branch_contact, branch_alternative_contact=branch_alternative_contact, branch_email=branch_email, \
            branch_city=lc.company_city)
        BranchOperator.objects.create(status="Pending", operator_type="SuperAdmin", user=user,logistics_branch=lc_branch, full_name=validated_data['full_name'], mobile=lc_branch.branch_alternative_contact)
        user.set_password(validated_data['password'])
        user.save()
        DeliveryType.objects.create(status='Active',logistics_branch=lc_branch, delivery_type='Standard')

        send_mail(
            'Signup Succesful',
            'Hi! uYo have successfully signed up in lms system. We will get back to you soon.',
            # 'sarobarcomp@gmail.com',
            settings.EMAIL_HOST_USER,
            [email],
            fail_silently=False,
        )
        return validated_data

    def validate(self, attrs):
        email = attrs['email']
        if User.objects.filter(email=email).exists():
            raise serializers.ValidationError({"email":"email already exists"})
        return attrs
    
    def validate_password(self, value):
        if value.isalnum():
            raise serializers.ValidationError('password must have atleast one special character.')
        return value


class ChangeOperatorPasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True, min_length = 4)
    new_password = serializers.CharField(required=True, min_length = 4)
    confirm_new_password = serializers.CharField(required=True, min_length = 4)

    def validate(self, attrs):
        if attrs['new_password'] != attrs['confirm_new_password']:
            raise serializers.ValidationError({'new_password':"password didn't match !!"})
        return attrs


class OperatorResetPasswordEmailRequestSerializer(serializers.Serializer):
    email = serializers.EmailField(min_length=2)
    class Meta:
        fields = ['email']


class OperatorSetNewPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(
        min_length=6, max_length=68, write_only=True)
    token = serializers.CharField(
        min_length=1, write_only=True)
    uidb64 = serializers.CharField(
        min_length=1, write_only=True)

    class Meta:
        fields = ['password', 'token', 'uidb64']

    def validate(self, attrs):
        try:
            password = attrs.get('password')
            token = attrs.get('token')
            uidb64 = attrs.get('uidb64')

            id = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)
            if not PasswordResetTokenGenerator().check_token(user, token):
                raise AuthenticationFailed('The reset link is invalid', 401)

            user.set_password(password)
            user.save()

            return (user)
        except Exception as e:
            raise serializers.ValidationError('The reset link is invalid')
        return super().validate(attrs)


class CustomerListSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsCustomer
        fields = ['full_name', 'contact_number', 'address']


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ['id', 'name']


class CustomerDetailSerializer(serializers.ModelSerializer):
    city = CitySerializer()
    class Meta:
        model = LogisticsCustomer
        fields = ['customer_type', 'full_name', 'company_name', 'contact_number', 'alternative_contact_number', 'city', 'address', \
            'document1', 'document2', 'document3']

class CustomerCreateSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    password = serializers.CharField(min_length=8)
    class Meta:
        model = LogisticsCustomer
        fields = ['id','full_name', 'company_name', 'contact_number', 'city', 'address', \
            'email', 'password']
        
    def validate(self, attrs):
        email = attrs['email']
        if User.objects.filter(email=email).exists():
            raise serializers.ValidationError({"email":"email already exists"})
        return attrs
    
    def validate_password(self, value):
        if value.isalnum():
            raise serializers.ValidationError('password must have atleast one special character.')
        return value

class CustomerUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsCustomer
        fields = ['full_name', 'contact_number', 'company_name', 'city', 'address']


class CustomerPasswordChangeByCreatorBranchSerializer(serializers.Serializer):
    new_password = serializers.CharField(min_length=8)
    confirm_new_password = serializers.CharField(min_length=8)

    def validate_password(self, value):
        if value.isalnum():
            raise serializers.ValidationError('password must have atleast one special character.')
        return value
    
    def validate(self, attrs):
        if attrs['new_password'] != attrs['confirm_new_password']:
            raise serializers.ValidationError({'new_password':"password didn't match !!"})
        return attrs


class OperatorStaffListSerializer(serializers.ModelSerializer):
    class Meta:
        model = BranchOperator
        fields = ['operator_type', 'full_name', 'mobile', 'address_city']


class OperatorStaffDetailSerializer(serializers.ModelSerializer):
    address_city = CitySerializer()
    class Meta:
        model = BranchOperator
        fields = ['operator_type', 'full_name', 'address_city', 'image', 'mobile', 'alt_mobile', 'document1', 'document2', 'document3']

Operator_Type=(
    ("Admin", "Admin"),
    ("Staff", "Staff") 
)
class OperatorCreateStaffSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    password = serializers.CharField(min_length=8)
    operator_type = serializers.ChoiceField(choices=Operator_Type)
    class Meta:
        model = BranchOperator
        fields = ['full_name', 'image', 'mobile', 'alt_mobile', 'document1', 'document2', 'document3', 'address_city', 'email', 'password','operator_type']
    
    def validate_password(self, value):
        if value.isalnum():
            raise serializers.ValidationError('password must have atleast one special character.')
        return value
    
    def validate(self, attrs):
        email = attrs['email']
        if User.objects.filter(email=email).exists():
            raise serializers.ValidationError({"email":"email already exists"})
        return attrs


class OperatorpasswordChangeBySuperAdminSerializer(serializers.Serializer):
    new_password = serializers.CharField(min_length=8)
    confirm_new_password = serializers.CharField(min_length=8)

    def validate_password(self, value):
        if value.isalnum():
            raise serializers.ValidationError('password must have atleast one special character.')
        return value
    
    def validate(self, attrs):
        if attrs['new_password'] != attrs['confirm_new_password']:
            raise serializers.ValidationError({'new_password':"password didn't match !!"})
        return attrs


class OperatorUpdateBySuperAdminSerializer(serializers.ModelSerializer):
    operator_type = serializers.ChoiceField(choices=Operator_Type)
    class Meta:
        model = BranchOperator
        fields = ['operator_type', 'full_name', 'address_city', 'mobile', 'alt_mobile', 'image', 'document1', 'document2', 'document3']


class DeliveryTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeliveryType
        fields = ['delivery_type']


class DeliveryTypeCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeliveryType
        fields = ['delivery_type', 'image']


class ShippingZoneDeliveryTypeSerializer(serializers.ModelSerializer):
    delivery_type = DeliveryTypeSerializer()
    class Meta:
        model = ShippingZoneDeliveryType
        fields = ['delivery_type','shipping_charge_per_kg']



class ShippingZoneListSerializer(serializers.ModelSerializer):
    shippng_zone = ShippingZoneDeliveryTypeSerializer(source='shippingzonedeliverytype_set', many=True)
    class Meta:
        model = LogisticsBranchShippingZone
        fields = ['shipping_zone', 'shippng_zone']


class ShippingZoneDeliveryTypeDetailSerializer(serializers.ModelSerializer):
    delivery_type = DeliveryTypeSerializer()
    class Meta:
        model = ShippingZoneDeliveryType
        fields = ['delivery_type','shipping_charge_per_kg', 'additional_shipping_charge_per_kg', 'cod_handling_charge', 'rejection_handling_charge']


class ShippingZoneDetailSerializer(serializers.ModelSerializer):
    shippng_zone = ShippingZoneDeliveryTypeDetailSerializer(source='shippingzonedeliverytype_set', many=True)
    class Meta:
        model = LogisticsBranchShippingZone
        fields = ['shipping_zone', 'shippng_zone']


class CustomerInforSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsCustomer
        fields = ['full_name', 'address', 'contact_number']


class CustomerSupportTicketListSerializer(serializers.ModelSerializer):
    raised_by = CustomerInforSerializer()
    class Meta:
        model = CustomerSupportTicket
        fields = ['raised_by', 'issue_detail']


class CustomerTicketFollowupSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerTicketFollowup
        fields = ['followup_by_type','followup_message']
        depth = 1

class CustomerSupportTicketDetailSerializer(serializers.ModelSerializer):
    ticket = CustomerTicketFollowupSerializer(source='customerticketfollowup_set', many=True)
    class Meta:
        model = CustomerSupportTicket
        fields = ['id', 'raised_by', 'ticket_status', 'issue_detail', 'related_file', 'ticket']
    

class CustomerTicketFollowupCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerTicketFollowup
        fields = ['followup_message']


#serializer for notice detailing
class LogisticsCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsCompany
        fields = ['company_name']


class LogisticsBranchSerializer(serializers.ModelSerializer):
    logistics_company = LogisticsCompanySerializer()
    branch_city = CitySerializer()
    class Meta:
        model = LogisticsBranch
        fields = ['branch_city', 'logistics_company']


class OperatorDetailSerializer(serializers.ModelSerializer):
    logistics_branch = LogisticsBranchSerializer()
    class Meta:
        model = BranchOperator
        fields = ['full_name', 'operator_type', 'logistics_branch']


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsCustomer
        fields = ['id','full_name', 'company_name']

class LogisticsOperatorNoticeListSerializer(serializers.ModelSerializer):
    created_by = OperatorDetailSerializer()
    class Meta:
        model = LogisticsNotice
        fields = ['id','created_by', 'notice', 'created_at']


class LogisticsOperatorNoticeDetailSerializer(serializers.ModelSerializer):
    excluded_customers = CustomerSerializer(many=True)
    class Meta:
        model = LogisticsNotice
        fields = ['notice', 'excluded_customers']


class LogisticsOperatorNoticeCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsNotice
        fields = ['notice', 'excluded_customers']


#helper serializer for customer contract
class LogisticsCustomerSerializer(serializers.ModelSerializer):
    city = CitySerializer()
    class Meta:
        model = LogisticsCustomer
        fields = ['full_name', 'company_name', 'city']


class LogisticsContractRequestedByCustomerSerializer(serializers.ModelSerializer):
    second_party = LogisticsCustomerSerializer()
    class Meta:
        model = LogisticsCustomerContract
        fields = ['id','second_party', 'created_at']
