from rest_framework_simplejwt.views import TokenRefreshView
from django.urls import path
from .views import *


app_name = "lbapiapp"
urlpatterns = [
     path("get-token-pair/", OperatorGetTokenPairView.as_view(), name="operatorgettokenpair"),
     path('refresh-token/', TokenRefreshView.as_view(), name='operatorrefreshtoken'),


     #Company and Operator
     path("profile/", OperatorProfileAPIView.as_view(), name="operatorprofileapi"),
     path("profile/update/", OperatorProfileUpdateAPIView.as_view(), name="operatorprofileupdateapi"),
     path("company/registration/", CompanyRegistrationApiView.as_view(), name="companyregistration"),
     path('operator/password/change/', OperatorPasswordChangeAPIView.as_view(), name='operatorpasswordchange'),
     
     
     #Operator Reset Password
     path('request-reset-email/', OperatorRequestPasswordResetEmail.as_view(),
          name="operator-request-reset-email"),
     path('password-reset/check/token/<uidb64>/<token>/',
          OperatorPasswordTokenCheckAPI.as_view(), name='operator-token-check'),
     path('password-reset-complete/', OperatorSetNewPasswordAPIView.as_view(),
          name='operator-password-reset-complete'),

     #Customer Info In RelatedBranch
     path("customer/create/", CustomerCreateFromOperatorAPIView.as_view(), name='logisticscustomercreate'),
     path("customer/list/", CustomerInfoListAPIView.as_view(), name='customerlist'),
     path("customer/detail/<int:pk>/", CustomerInforDetailAPIView.as_view(), name='customerdetail'),
     path("customer/update/<int:pk>/", CustomerUpdateFromOperatorAPIView.as_view(), name='customerupdate'),
     path('customer/password/change/<int:pk>/', CustomerPasswordChangeByCreatorBranchAPIView.as_view(), name='customerpasswordchange'),


     #Branch Staff Management
     path('operator/staff/list/', OperatorStaffListAPIView.as_view(), name='operatorlist'),
     path('operator/staff/detail/<int:pk>/', OperatorStaffDetailAPIView.as_view(), name='operatordetail'),
     path('operator/create/staff/', OperatorCreateStaffAPIView.as_view(), name='operatorcreatestaff'),
     path('operator/password/change-by-superadmin/<int:pk>/', OperatorPasswordChangeBySuperAdminAPIView.as_view(), name='changeoperatorpasswordbysuperadmin'),
     path('operator/update-by-superadmin/<int:pk>/', BranchOperatorUpdateBySuperAdminAPIView.as_view(), name='operatorupdatebysuperadmin'),
     

     #DeliveryType
     path('delivery-type/create/', DeliveryTypeCreateAPIView.as_view(), name='deliverytypecreate'),
     path('delivery-type/update/<int:pk>/', DeliveryTypeUpdateAPIView.as_view(), name='deliverytypeupdate'),


     #Shipping Zone
     path('shippingzone/list/', ShippingZoneListAPIView.as_view(), name = 'shippingzonelist'),
     path('shippingzone/detail/<int:pk>/', ShippingZoneDetailAPIView.as_view(), name = 'shippingzonedetail'),


     #Customer Support Ticket and followup message
     path('customer/support/ticket/list/', CustomerSupportTicketListAPIView.as_view(), name='customersupportticketlist'),
     path('customer/support/ticket/detail/<int:pk>/', CustomerSupportTicketDetailAPIView.as_view(), name='customersupportticketdetail'),
     path('customer/support/ticket/<int:pk>/followup/', CustomerTicketFollowupAPIView.as_view(), name='customersupportticketfollowup'),


     #Logistics Notice Info
     path('notice/list/', LogisticsNoticeListAPIView.as_view(), name='noticelist'),
     path('notice/detail/<int:pk>/', LogisticsNoticeDetailAPIView.as_view(), name='noticedetail'),
     path('notice/create/', LogisticsNoticeCreateAPIView.as_view(), name='noticecreate'),
     path('notice/update/<int:pk>/', LogisticsNoticeUpdateAPIView.as_view(), name='noticeupdate'),


     #Customer Contract
     path('contract-requested-by/customer/list/', LogisticsContractRequestedByCustomerListAPIView.as_view(), name='contractlistrequestedbycustomer'),
     

]