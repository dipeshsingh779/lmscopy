from django.apps import AppConfig


class LbapiappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lbapiapp'
