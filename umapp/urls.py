from django.urls import path, include
from .views import *

app_name = "umapp"
urlpatterns = [
    
#     path("operator/reset-password/",
#          LogisticsAdminResetPasswordView.as_view(), name="laresetpassword"),
    # path("operator/login/", LogisticsAdminLoginView.as_view(),
    #      name="logisticsadminlogin"),
    


    # Country
    path('operator/country/create',
         CountryCreateView.as_view(), name='countrycreate'),
    path('operator/country/<int:pk>/edit/',
         CountryUpdateView.as_view(), name='countryedit'),


    # province
    # path('operator/province/list/', ProvinceView.as_view(), name='provincelist'),
    path('operator/province/create/',
         ProvinceCreateView.as_view(), name='provincecreate'),
    path('operator/province/<int:pk>/edit/',
         ProvinceUpdateView.as_view(), name='provinceedit'),
    path('operator/province/delete/<int:pk>/',
         ProvinceDelete.as_view(), name='provincedelete'),


    # #District url
    # path('operator/district/list/', DistrictView.as_view(), name="districtlist"),
    path('operator/district/create/',
         DistrictCreateView.as_view(), name="districtcreate"),
    path('operator/district/update/<int:pk>/',
         DistrictUpdate.as_view(), name="districtedit"),


    # #City Url
    # path('operator/cities/list/',CityView.as_view(), name='citylist'),
    path('operator/cities/create/', CityCreateView.as_view(), name='citycreate'),
    path('operator/city/update/<int:pk>/',
         CityUpdateView.as_view(), name="cityedit"),

     path("initial-setup/", InitialSetupView.as_view(), name="initialsetup"),


    # branchoperator
    path('operator/branch-operator/list',
         BranchOperatorListView.as_view(), name='operatorlist'),
    path('operator/branch-operator/create',
         BranchOperatorCreateView.as_view(), name='operatorcreate'),
    path('operator/branch-operator/update/<int:pk>',
         BranchOperatorUpdateView.as_view(), name='operatorupdate'),


    # Branch operator password change
    path('operator/branch-operator/pass/change/<int:pk>/',
         BranchOperatorPasswordChangeView.as_view(), name='operatorpasschange'),



    # Shipping Zone
    path('operator/branch-operator/shipping/zone/',
         LogisticBranchShippingListZoneView.as_view(), name='shippinglist'),
    path('operator/branch-operator/shipping/zone/create',
         LogisticBranchShippingZoneCreateView.as_view(), name='shippingzonecreate'),
    path('operator/branch-operator/shipping/zone/update/<int:pk>/',
         LogisticBranchShippingZoneUpdateView.as_view(), name='shippingupdate'),
    path('operator/branch-operator/shipping/zone/detail/<int:pk>/',
         logisticsBranchShippingZoneDetailView.as_view(), name='shippingdetail'),
     path('operator/ajax/serch/city/', AjaxCitySearchView.as_view(), name = 'ajaxcitysearach'),
     # path('operator/ajax/list/city/', AjaxCitySearchListView.as_view(), name = 'ajaxcitysearchlist'),


    # DeliveryType/

    path('operator/branch-operator/create/deliverytype/',
         DeliveryTypeView.as_view(), name='deliverytype'),
    path('operator/branch-operator/update/deliverytype/<int:pk>/',
         DeliveryTypeUpdateView.as_view(), name='deliverytypeupdate'),

     #settings
    path('operator/branch-operator/settings/',
         SettingView.as_view(), name='settings'),
     path('operator/branch-operator/settings/ajax/search/city/', AjaxSettingsCitySearchView.as_view(), name = 'ajaxsettingscitysearch'),
     # path('operator/branch-operator/settings/ajax/search/district/', AjaxSettingsDistrictSearchView.as_view(), name = 'ajaxsettingsdistrictsearch'),
     # path('operator/branch-operator/settings/ajax/search/province/', AjaxSettingsProvinceSearchView.as_view(), name = 'ajaxsettingsprovincesearch'),
     # path('operator/branch-operator/settings/ajax/search/country/', AjaxSettingsCountrySearchView.as_view(), name = 'ajaxsettingscountrysearch'),
     


    # ajax
    path('operator/branch-operator/email/validate/',
         validateoperatoremailview, name='checkemail'),

    path('countryname/check/', CountryAjaxCheck, name='checkcountryname'),
    path('provincename/check/', ProvinceAjaxCheck, name='checkprovincename'),
    path('districtname/check/', DistrictAjaxCheck, name='checkdistrictname'),
    path('cityname/check/', CityAjaxCheck, name='checkcityname'),

    # operator customer
    path('operator/customers/list/',
         OperatorCustomersListView.as_view(), name='opcustomerslist'),
    path('operator/customers/create/',
         OperatorCustomersCreateView.as_view(), name='opcustomerscreate'),
    path('operator/customers/update/<int:pk>/',
         OperatorCustomersUpdateView.as_view(), name='opcustomersupdate'),
    path('operator/customers/change-password/<int:pk>',
         OperatorCustomersPasswordChangeView.as_view(), name='opcustomerspc'),
    path('operator/customers/detail/<int:pk>/',
         OperatorCustomersDetailView.as_view(), name='opcustomersdetail'),
    path('operator/customers/payment-list/',
         LogisticAdminLogisticsCustomerPaymentListView.as_view(), name="lalcpaymentlist"),


     # operator rider
     path('operator/deliveryperson/list/',
         OperatorDeliveryPersonListView.as_view(), name='opdeliverypersonlist'),
     path('operator/deliveryperson/create/',
         OperatorDeliveryPersonCreateView.as_view(), name='opdeliverypersoncreate'),
     path('operator/deliveryperson/detail/<int:pk>/',
         OperatorDeliveryPersonDetailView.as_view(), name='opdeliverypersondetail'),
     path('operator/deliveryperson/update/<int:pk>/',
         OperatorDeliveryPersonUpdateView.as_view(), name='opdeliverypersonupdate'),
     path('operator/deliveryperson/change-password/<int:pk>',
         OperatorDeliveryPersonPasswordChangeView.as_view(), name='opdeliverypersonpc'),
     



    #Customer signup/login
    path("customer/signup/", CustomerSignupView.as_view(), name="customersignup"),
    path("customer/login/", CustomerLoginView.as_view(), name="customerlogin"),
    path("customer/logout/", CustomerLogoutView.as_view(), name="customerlogout"),
    path("customer/reset-password/<email>/<token>/",
         CustomerResetPasswordView.as_view(), name="customerpr"),
    path("customer/forgot-password/",
         CustomerForgotPasswordView.as_view(), name="customerfp"),

    path("username-checker/", CustomerUsernameCheckerView.as_view(),
         name="usernamechecker"),

    
    #Operator signup/login
    path("operator/signup/",LogisticCompanySignupView.as_view(), name="logcompanysignup"),
    path("operator/login/", LogisticCompanyLoginView.as_view(), name="logcompanylogin"),
    path("operator/forgot-password/", LogisticCompanyForgotPasswordView.as_view(), name="logcompanyfp"),
    path("operator/reset-password/<email>/<token>/", LogisticCompanyResetPasswordView.as_view(), name="logcompanypr"),
    path("operator/logout/", LogoutView.as_view(), name='logout'),
    path("operator/accounts/detail/",
         OperatorAccountDetailView.as_view(), name='operatoraccount'),
         
    

]
