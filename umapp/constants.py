from django.db.models import Q

STATUS = (
    ("Pending", "Pending"),
    ("Active", "Active"),
    ("Disabled", "Disabled"),
)


SYSTEM_OPERATOR_TYPE = (
    ("SuperAdmin", "SuperAdmin"),  
    ("Staff", "Staff"),
)


OPERATOR_TYPE = (
    ("SuperAdmin", "SuperAdmin"),  
    ("Admin", "Admin"),
    ("Staff", "Staff"),
)
# there must be only one super admin in a logistic company

ATTEMPT_STATUS = (
    ("Success", "Success"),
    ("Failure", "Failure"),
)

SHIPMENT_STATUS = (
    # how shipments arrive at source logistics  
    ("Shipment Requested", "Shipment Requested"), # level 1
    ("Shipment Canceled", "Shipment Canceled"), # level 2
    ("Rider Assigned for Pickup", "Rider Assigned for Pickup"), # level 2
    ("Rider Picked up the Shipment", "Rider Picked up the Shipment"),  # level 3
    ("Shipment Arrived at Warehouse", "Shipment Arrived at Warehouse"),  # level 3a
    ("Sender at Logistics with Shipment", "Sender at Logistics with Shipment"), # level 1

    # how shipments is delivered to the Receiver
    ("Rider Assigned for Dispatch to the Receiver", "Rider Assigned for Dispatch to the Receiver"), # level 4
    ("Rider Assigned for Dispatch to DC", "Rider Assigned for Dispatch to DC"), # level 4
    ("Rider Picked up the Shipment for Dispatch", "Rider Picked up the Shipment for Dispatch"), # level 5
    ("Shipment Delivered to the Receiver", "Shipment Delivered to the Receiver"), # final
    ("Shipment Delivered to the DC", "Shipment Delivered to the DC"), # level 6
    ("Rider Assigned for Dispatch in DC", "Rider Assigned for Dispatch in DC"), # level 7
    ("Shipment Dispatched to the Receiver in DC", "Shipment Dispatched to the Receiver in DC"), # level 8
    ("Shipment Delivered to the Receiver in DC", "Shipment Delivered to the Receiver in DC"), # final stage

    # how shipments are returned to the logistics 
    ("Shipment Rejected", "Shipment Rejected"), # level 9
    ("Shipment Returned to Logistics", "Shipment Returned to Logistics"), # level 10
    ("Shipment Rejected in DC", "Shipment Rejected in DC"),  # level 9
    ("Shipment Ready to Return by DC", "Shipment Ready to Return by DC"), # level 10
    ("Rider Assigned for Return to Logistics by DC", "Rider Assigned for Return to Logistics by DC"), # level 11
    ("Shipment Returned to the Logistics by DC", "Shipment Returned to the Logistics by DC"), # level 12
    # how shipments are returned to the sender
    ("Ready to Return to Sender", "Ready to Return to Sender"),
    ("Rider Assigned for Returning to the Sender", "Rider Assigned for Returning to the Sender"), # level 13
    ("Rider Picked up the Shipment for Return", "Rider Picked up the Shipment for Return"), # level 14
    ("Shipment Returned to the Sender", "Shipment Returned to the Sender"), # final stage
)

SHIPMENT_REMARKS_BY = Q(app_label='umapp', model='branchoperator') | Q(app_label='umapp', model='deliveryperson') | Q(app_label='umapp', model='logisticscustomer')

COD_INFO = (
    ("Not a Business", "Not a Business"),
    ("Already Paid", "Already Paid"),
    ("Cash on Delivery", "Cash on Delivery"),
)

WEIGHT_UNIT = (
    ("Gram", "Gram"),
    ("Kilogram", "Kilogram"),
    ("Quintal", "Quintal"),
    ("Ton", "Ton"),
)

LENGTH_UNIT = (
    ("Cm", "Cm"),
    ("Inches", "Inches"),
    ("Feet", "Feet"),
    ("Meter", "Meter"),
)

CUSTOMER_TYPE = (
    ("Individual Customer", "Individual Customer"),
    ("Business Customer", "Business Customer"),
)

TICKET_STATUS = (
    ("Open", "Open"),
    ("Reviewing", "Reviewing"),
    ("Solved", "Solved"),
)

DP_TASK_TYPE = (
    ("Pick Up", "Pick Up"),
    ("Drop Off", "Drop Off"),
    ("Return", "Return"),
    ("Bundle Pick Up", "Bundle Pick Up"),
    ("Bundle Drop Off", "Bundle Drop Off"),
    ("Bundle Return", "Bundle Return"),
)

DP_TASK_STATUS = (
    ("Canceled", "Canceled"),
    ("Assigned", "Assigned"),
    ("Started", "Started"),
    ("Doing", "Doing"),
    ("Completed Success", "Completed Success"),
    ("Completed Fail", "Completed Fail"),
)


CTICKET_FOLLOWUP_BY = Q(app_label='umapp', model='branchoperator') | Q(app_label='umapp', model='logisticscustomer')
LTICKET_FOLLOWUP_BY = Q(app_label='umapp', model='branchoperator') | Q(app_label='umapp', model='logisticscustomer')
