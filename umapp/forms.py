from django.contrib import messages
from django import forms
from .models import *
from lmsapp.models import CustomerSupportTicket


class LogisticsAdminLoginForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': "Enter email",
        'pattern': '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$'
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': "Password"
    }))


class LogisticsAdminResetPasswordForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': "Enter email",
        'pattern': '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$'
    }))


class OperatorLoginForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control'
    })
    )
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control'
    }))


class CountryForm(forms.ModelForm):
    class Meta:
        model = Country
        fields = ['name', 'slug']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'country_id'
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'country_slug'
            })
        }
    # def __init__(self, *args, **kwargs):
    #     super(CountryForm, self).__init__(*args, **kwargs)
    #     self.fields['slug'].disabled=True


class CountryUpdateForm(forms.ModelForm):
    class Meta:
        model = Country
        fields = ['name', 'slug']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'country-name'
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'country-slug'
            })
        }
    # def __init__(self, *args, **kwargs):
    #     super(CountryUpdateForm, self).__init__(*args, **kwargs)
    #     self.fields['slug'].disabled=True


class ProvinceForm(forms.ModelForm):
    class Meta:
        model = Province
        fields = ['name', 'slug', 'country']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'prov-id'
            }),
            'country': forms.Select(attrs={
                'class': 'form-control',
                'id': 'prov-country-id'
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'prov-slug'
            })
        }
    # def __init__(self, *args, **kwargs):
    #     super(ProvinceForm, self).__init__(*args, **kwargs)
    #     self.fields['slug'].disabled=True


class ProvinceUpdateForm(forms.ModelForm):
    class Meta:
        model = Province
        fields = ['name', 'slug', 'country']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control prov-name',
            }),
            'country': forms.Select(attrs={
                'class': 'form-control',
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control prov-slug',
            })
        }
    # def __init__(self, *args, **kwargs):
    #     super(ProvinceUpdateForm, self).__init__(*args, **kwargs)
    #     self.fields['slug'].disabled=True


class DistrictForm(forms.ModelForm):
    class Meta:
        model = District
        fields = ['name', 'slug', 'province']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control input-sm',
                'id': 'dist-name'
            }),
            'province': forms.Select(attrs={
                'class': 'form-control'
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'dist-slug'
            })
        }
    # def __init__(self, *args, **kwargs):
    #     super(DistrictForm, self).__init__(*args, **kwargs)
    #     self.fields['slug'].disabled=True


class DistrictUpdateForm(forms.ModelForm):
    class Meta:
        model = District
        fields = ['name', 'slug', 'province']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control input-sm dist-name',
                'id': 'dist-name'
            }),
            'province': forms.Select(attrs={
                'class': 'form-control dist-prov',
                'id': 'dist-province'
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control dist-slug',
                'id': 'dist-slug'
            })
        }
    # def __init__(self, *args, **kwargs):
    #     super(DistrictUpdateForm, self).__init__(*args, **kwargs)
    #     self.fields['slug'].disabled=True


class CityForm(forms.ModelForm):
    class Meta:
        model = City
        fields = ['name', 'slug', 'district']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'city-name'
            }),
            'district': forms.Select(attrs={
                'class': 'form-control',
                'id': 'city-dist'
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'city-slug'
            })
        }
    # def __init__(self, *args, **kwargs):
    #     super(CityForm, self).__init__(*args, **kwargs)
    #     self.fields['slug'].disabled=True

    # def clean_name(self, form):
    #     name = self.cleaned_data['name']
    #     print(name)


class CityUpdateForm(forms.ModelForm):
    class Meta:
        model = City
        fields = ['name', 'slug', 'district']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'city-name'
            }),
            'district': forms.Select(attrs={
                'class': 'form-control',
                'id': 'city-dist'
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'city-slug'
            })
        }
    # def __init__(self, *args, **kwargs):
    #     super(CityUpdateForm, self).__init__(*args, **kwargs)
    #     self.fields['slug'].disabled=True


class LogisticsAccountTypeForm(forms.ModelForm):
    class Meta:
        model = LogisticsAccountType
        fields = ['account_type', 'allowed_operators', 'allowed_operators_per_branch', 'allowed_riders', 'allowed_riders_per_branch',
                  'allowed_shipments_per_month', 'per_shipment_charge', 'per_month_charge', 'per_branch_charge', 'per_operator_charge']


class LogisticsCompanyForm(forms.ModelForm):
    class Meta:
        model = LogisticsCompany
        fields = ['account_type', 'company_name', 'company_slug', 'company_logo', 'company_profile', 'company_description', 'company_city',
                  'company_website', 'company_email', 'is_distribution_center', 'connected_logistics', 'document1', 'document2', 'document3']


class LogisticsBranchForm(forms.ModelForm):
    class Meta:
        model = LogisticsBranch
        fields = ['logistics_company', 'branch_city', 'branch_contact', 'branch_alternative_contact',
                  'branch_email', 'branch_coverage', 'branch_address', 'is_main_branch']


class BranchOperatorForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'placeholder': 'enter a valid email'
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder': 'confirm your password'
    }))

    class Meta:
        model = BranchOperator
        fields = ['operator_type', 'full_name', 'image', 'mobile', 'alt_mobile',
                  'document1', 'document2', 'document3', 'address_city', 'email', 'password']
        widgets = {
            'operator_type':forms.Select(attrs={
                'class':'form-control',
            }),
            "logistics_branch": forms.Select(attrs={
                'class': 'form-control',
            }),
            "full_name": forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter your fullname...'
            }),
            "image": forms.FileInput(attrs={
                'class': 'form-control',
            }),
            "mobile": forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter your mobile number'
            }),
            "alt_mobile": forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter your alt mobile number if any..'
            }),
            "document1": forms.FileInput(attrs={
                'class': 'form-control',
            }),
            "document2": forms.FileInput(attrs={
                'class': 'form-control',
            }),
            "document3": forms.FileInput(attrs={
                'class': 'form-control',
            }),
            "address_city": forms.Select(attrs={
                'class': 'form-control',
            }),
            "connected_branches":forms.SelectMultiple(),
            'coverage_cities':forms.SelectMultiple(),

        }

        def clean_email(self):
            email = self.cleaned_data['email']
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError(
                    "email already exists"
                )

    # def __init__(self, comp=None, *args, **kwargs):
    #     super(BranchOperatorForm, self).__init__(*args, **kwargs)
    #     self.fields['connected_branches'].queryset = LogisticsBranch.objects.filter(
    #         logistics_company=comp)


class BranchOperatorUpdateForm(forms.ModelForm):
    class Meta:
        model = BranchOperator
        fields = ['operator_type', 'user', 'full_name', 'image', 'mobile', 'alt_mobile', 'document1',
                  'document2', 'document3', 'address_city']
        widgets = {
            'coverage_cities': forms.SelectMultiple(),
            "logistics_branch": forms.Select(attrs={
                'class': 'form-control',
            }),
            "full_name": forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter your fullname...'
            }),
            "image": forms.FileInput(attrs={
                'class': 'form-control',
            }),
            "mobile": forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter your mobile number'
            }),
            "alt_mobile": forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter your alt mobile number if any..'
            }),
            "document1": forms.FileInput(attrs={
                'class': 'form-control',
            }),
            "document2": forms.FileInput(attrs={
                'class': 'form-control',
            }),
            "document3": forms.FileInput(attrs={
                'class': 'form-control',
            }),
            "address_city": forms.Select(attrs={
                'class': 'form-control',
            }),
            'coverage_cities':forms.SelectMultiple(),

            "connected_branches":forms.SelectMultiple()
        }

    def __init__(self,comp=None, *args, **kwargs):
            super(BranchOperatorUpdateForm, self).__init__(*args, **kwargs)
            self.fields['operator_type'].disabled=True
            self.fields['user'].disabled=True
            self.fields['connected_branches'].queryset = LogisticsBranch.objects.filter(logistics_company=comp)
            # self.fields['coverage_cities'].queryset = City.objects.none()

    def __init__(self, comp=None, *args, **kwargs):
        super(BranchOperatorUpdateForm, self).__init__(*args, **kwargs)
        self.fields['operator_type'].disabled = True
        self.fields['user'].disabled = True
        # self.fields['connected_branches'].queryset = LogisticsBranch.objects.filter(
        #     logistics_company=comp)


class ChangeOperatorPasswordForm(forms.Form):
    new_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control'
    }))

    confirm_new_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control'
    }))

    def clean(self):
        new_password = self.cleaned_data['new_password']
        confirm_new_password = self.cleaned_data['confirm_new_password']
        if new_password != confirm_new_password:
            raise forms.ValidationError("password didn't match")


class LogisticBranchShippingZoneForm(forms.ModelForm):
    class Meta:
        model = LogisticsBranchShippingZone
        fields = ['shipping_zone'   ]
        widgets = {
            'delevery_type': forms.Select(attrs={
                'class': 'form-control'
            }),
            'shipping_zone': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'shipping_charge_per_kg':forms.NumberInput(),
            'additional_shipping_charge_per_kg':forms.NumberInput(),
            'cod_handling_charge':forms.NumberInput(),
            'rejection_handling_charge':forms.NumberInput(),
            # 'cities':forms.SelectMultiple()
        }


class LogisticBranchShippingZoneUpdateForm(forms.ModelForm):
    class Meta:
        model = LogisticsBranchShippingZone
        fields = ['shipping_zone']
        widgets = {
            'delevery_type': forms.Select(attrs={
                'class': 'form-control'
            }),
            'shipping_zone': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'shipping_charge_per_kg': forms.NumberInput(attrs={
                'class': 'form-control'
            }),
            'additional_shipping_charge_per_kg': forms.NumberInput(attrs={
                'class': 'form-control'
            }),
            'cod_handling_charge': forms.NumberInput(attrs={
                'class': 'form-control'
            }),
            'rejection_handling_charge': forms.NumberInput(attrs={
                'class': 'form-control'
            }),
        }

    def __init__(self, comp=None, *args, **kwargs):
        super(LogisticBranchShippingZoneUpdateForm,
              self).__init__(*args, **kwargs)
        # self.fields['delivery_type'].disabled=True
        # self.fields['shipping_zone'].disabled=True


class LogisticBranchShippingZoneDeliveryUpdateForm(forms.ModelForm):
    class Meta:
        model = ShippingZoneDeliveryType
        fields = ['shipping_charge_per_kg', 'additional_shipping_charge_per_kg',
                  'cod_handling_charge', 'rejection_handling_charge']
        widgets = {
            'shipping_charge_per_kg': forms.NumberInput(attrs={
                'class': 'form-control'
            }),
            'additional_shipping_charge_per_kg': forms.NumberInput(attrs={
                'class': 'form-control'
            }),
            'cod_handling_charge': forms.NumberInput(attrs={
                'class': 'form-control'
            }),
            'rejection_handling_charge': forms.NumberInput(attrs={
                'class': 'form-control'
            }),
        }

    def __init__(self, comp=None, *args, **kwargs):
        super(LogisticBranchShippingZoneUpdateForm,
              self).__init__(*args, **kwargs)
        # self.fields['delivery_type'].disabled=True
        # self.fields['shipping_zone'].disabled=True


class DeliveryTypeForm(forms.ModelForm):

    class Meta:
        model = DeliveryType
        fields = ("delivery_type", "image")
        widgets = {
            'delivery_type': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'image': forms.FileInput(attrs={
                'class': 'form-control'
            })
        }


class OperatorCustomersCreateForm(forms.ModelForm):

    email = forms.CharField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter email',
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter password.',

    }))

    class Meta:
        model = LogisticsCustomer
        fields = ['email', 'password', 'full_name',
                  'contact_number', 'company_name', 'address', 'city', ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs["class"] = "form-control"
            field.widget.attrs["placeholder"] = "Enter " + name
            # field.widget.attrs["autocomplete"] = "off"


class OperatorCustomersUpdateForm(forms.ModelForm):

    class Meta:
        model = LogisticsCustomer
        fields = ['full_name', 'contact_number',
                  'company_name', 'address', 'city', ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs["class"] = "form-control"
            field.widget.attrs["placeholder"] = "Enter " + name
            # field.widget.attrs["autocomplete"] = "off"


class OperatorCustomersPasswordUpdateForm(forms.Form):
    # current_password = forms.CharField(widget=forms.PasswordInput(attrs={
    #     'class': 'form-control',
    #     'placeholder': 'Enter current password',
    # }))

    new_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'New Password'
    }))

    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'New Password'
    }))

    def clean(self):
        cleaned_data = super().clean()
        new_password = cleaned_data["new_password"]
        confirm_password = cleaned_data["confirm_password"]
        if new_password != confirm_password:
            raise forms.ValidationError(
                "New Passwords did not match!")


class OperatorDeliveryPersonCreateForm(forms.ModelForm):

    email = forms.CharField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter email',
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter password.',

    }))

    class Meta:
        model = DeliveryPerson
        fields = ['email', 'password', 'full_name','image',
                  'mobile', 'address_city' ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs["class"] = "form-control"
            field.widget.attrs["placeholder"] = "Enter " + name
            # field.widget.attrs["autocomplete"] = "off"


class OperatorDeliveryPersonUpdateForm(forms.ModelForm):

    class Meta:
        model = DeliveryPerson
        fields = ['full_name', 'image', 'mobile',
                   'address_city' ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs["class"] = "form-control"
            field.widget.attrs["placeholder"] = "Enter " + name
            # field.widget.attrs["autocomplete"] = "off"


class OperatorDeliveryPersonPasswordUpdateForm(forms.Form):
    # current_password = forms.CharField(widget=forms.PasswordInput(attrs={
    #     'class': 'form-control',
    #     'placeholder': 'Enter current password',
    # }))

    new_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'New Password'
    }))

    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'New Password'
    }))

    def clean(self):
        cleaned_data = super().clean()
        new_password = cleaned_data["new_password"]
        confirm_password = cleaned_data["confirm_password"]
        if new_password != confirm_password:
            raise forms.ValidationError(
                "New Passwords did not match!")




class LogisticsForgotPasswordForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': "Enter email",
        'pattern': '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$'
    }))

    def clean_email(self):
        print("ffvsdfvsdf")
        email = self.cleaned_data['email']
        if BranchOperator.objects.filter(user__username=email).exists():
            pass
        else:
            raise forms.ValidationError('Email does not exist')
        return email


class LogisticsResetPasswordForm(forms.Form):
    new_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'New Password'
    }))

    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'New Password'
    }))

    def clean_confirm_password(self):
        newpwd = self.cleaned_data['new_password']
        cnfpwd = self.cleaned_data['confirm_password']

        if newpwd != cnfpwd:
            raise forms.ValidationError("Password did not match")

        return cnfpwd


class CustomerForgotPasswordForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': "Enter email",
        'pattern': '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$'
    }))

    def clean_email(self):
        email = self.cleaned_data['email']
        if LogisticsCustomer.objects.filter(user__username=email).exists():
            pass
        else:
            raise forms.ValidationError('Email does not exist. Try again.')

        return email


class CustomerResetPasswordForm(forms.Form):
    new_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'New Password'
    }))

    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'New Password'
    }))

    def clean_confirm_password(self):
        newpwd = self.cleaned_data['new_password']
        cnfpwd = self.cleaned_data['confirm_password']

        if newpwd != cnfpwd:
            raise forms.ValidationError("Password did not match")

        return cnfpwd


class CustomerSignupForm(forms.ModelForm):

    full_name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your full name',
    }))
    email = forms.CharField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your email',
    }))

    password = forms.CharField(min_length=8, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your password. Must contain atleast 8 characters ',

    }))

    company_name = forms.CharField(widget=forms.TextInput({
        'class': 'form-control',
        'placeholder': 'Enter Company Name',
    }))

    contact_number = forms.CharField(widget=forms.NumberInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter Contact Number',
    }))

    # alternative_contact_number = forms.CharField(widget=forms.NumberInput(attrs={
    #     'class':'form-control',
    #     'placeholder': 'Enter Alternative Contact Number',
    # }))

    address = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your address',
    }))

    class Meta:
        model = LogisticsCustomer
        fields = ['email', 'password', 'full_name', 'contact_number',
                  'company_name', 'customer_type',  'address', 'city', ]
        # fields = "__all__"
        widgets = {
            "customer_type": forms.Select(attrs={
                'class': 'form-control'
            }),
            "city": forms.Select(attrs={
                'class': 'form-control'
            })
        }

    def clean_email(self):
        email = self.cleaned_data.get("email")
        user = User.objects.filter(username=email)
        if user.exists():
            raise forms.ValidationError(
                "user with this username already exist")

        return email


class CustomerLoginForm(forms.Form):
    email = forms.CharField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your email',
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your password',
    }))


class LogisticSignupForm(forms.ModelForm):
    class Meta:
        model = LogisticsCompany
        fields = ['company_name', 'company_logo', 'company_profile',
                  'company_description', 'company_website', 'company_city']
        # fields = "__all__"
        # widgets = {
        #     'company_name': forms.TextInput(attrs={
        #         'class':'form-control',
        #         'placeholder': 'Enter Company Name',
        #     }),
        #     'company_logo': forms.FileInput(attrs={
        #         'class': 'form-control',
        #         'placeholder': 'Your Company Logo',
        #     }),
        #     'company_profile':forms.FileInput(attrs={
        #         'class':'form-control',
        #         'placeholder': 'Provide an image that defines your company'

        #     }),
        #     'company_description':forms.TextInput(attrs={
        #         'class': 'form-control',
        #         'placeholder': 'Enter company description',
        #     }),
        #     'company_website': forms.TextInput(attrs={
        #         'class': 'form-control',
        #         'placeholder': 'Enter your company website link'
        #     }),

        # }


class LogisticsLoginForm(forms.Form):
    email = forms.CharField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your email',
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your password',
    }))


class LogisticsBranchSignup(forms.ModelForm):
    class Meta:
        model = LogisticsBranch
        fields = ['branch_city', 'branch_contact', 'branch_alternative_contact', 'branch_email',
                  'branch_address']
        #fields = "__all__"

        # widgets = {
        #     'branch_city':forms.Select(attrs={
        #         'class':'form-control'
        #     }),
        #     'branch_contact': forms.NumberInput(attrs={
        #         'class': 'form-control',
        #         'placeholder': 'Enter Branch contact number'
        #     }),
        #     'branch_alternative_contact':forms.NumberInput(attrs={
        #         'class': 'form-control',
        #         'placeholder': 'Enter alternative contact number'
        #     }),
        #     'branch_email':forms.TextInput(attrs={
        #         'class': 'form-control',
        #         'placeholder': 'Enter Branch email'
        #     }),
        #     'branch_coverage': forms.SelectMultiple(attrs={
        #         'class': 'form-control',
        #         'placeholder': 'Select Branch Coverage'
        #     }),
        #     'branch_address': forms.TextInput(attrs={
        #         'class': 'form-control',
        #         'placeholder': 'Enter branch address'
        #     }),

        # }


class BranchOperatorSignup(forms.ModelForm):
    operator_email = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your email'
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your Password'
    }))

    class Meta:
        model = BranchOperator
        fields = ['operator_email', 'password']


class LogisticsSignupForm(forms.ModelForm):
    branch_contact = forms.CharField(widget=forms.NumberInput(attrs={
        # "class": "form-control  " 
    }))
    branch_alternative_contact = forms.CharField(widget=forms.NumberInput(attrs={
        # "class": "form-control"
    }))
    branch_address = forms.CharField(widget=forms.TextInput(attrs={
        # "class": "form-control"
    }))
    # branch_coverage = forms.ModelMultipleChoiceField(queryset=City.objects.all(), widget=forms.CheckboxSelectMultiple(attrs={
    #     # "class": "form-control"

    #     # 'class': 'mycss'


    # }))
    branch_email = forms.EmailField(widget=forms.EmailInput(attrs={
        # "class": "form-control"
    }))
    operator_full_name = forms.CharField(widget=forms.TextInput(attrs={
        # "class": "form-control"
    }))
    operator_email = forms.EmailField(widget=forms.EmailInput(attrs={
        # "class": "form-control"
    }))
    operator_password = forms.CharField(widget=forms.PasswordInput(attrs={
        # "class": "form-control"
    }))

    class Meta:
        model = LogisticsCompany
        fields = ["company_name", "company_logo", "company_profile", "company_description", "company_city", "company_website", "company_email"]

    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['branch_alternative_contact'].required = False
        for name, field in self.fields.items():
            if name == 'branch_coverage':
                field.widget.attrs["class"] = ""
            else:
                field.widget.attrs["class"] = "form-control"
                field.widget.attrs["placeholder"] = "Enter " + name
                field.widget.attrs["autocomplete"] = "off"

