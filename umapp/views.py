from django.views.generic import View, TemplateView, FormView, DetailView,  ListView, UpdateView, CreateView, DeleteView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect, Http404
from django.urls import reverse, reverse_lazy
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.utils.text import slugify
from django.http import JsonResponse
from django.contrib import messages
from django.conf import settings
from django.db.models import F, Max
from .models import *
from lmsapp.models import Shipment
from .forms import *
from .utils import *
import json
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response


# Login Required Mixins of All users

class OperatorRequiredMixin(SuccessMessageMixin):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            try:
                self.operator = request.user.branchoperator
                self.branch = self.operator.logistics_branch
                self.company = self.branch.logistics_company
            except:
                return redirect(reverse('umapp:logcompanylogin')+'?next='+request.path)
        else:
            return redirect(reverse('umapp:logcompanylogin')+'?next='+request.path)
        return super(OperatorRequiredMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(OperatorRequiredMixin, self).get_context_data(**kwargs)
        context['operator'] = self.operator
        context["current_branch"] = self.branch
        context["current_company"] = self.company
        unsigned_contract = LogisticsCustomerContract.objects.filter(first_party=self.branch, status='Active')
        count = 0
        for contract in unsigned_contract:
            m = contract.logisticcustomerzonalcontract_set.all()
            if len(m) < 1:
                count += 1
        context['contract'] = count
        return context


class CustomerRequiredMixin(SuccessMessageMixin):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            try:
                self.user = request.user
                self.customer = request.user.logisticscustomer
                self.branch = self.customer.creator_branch
            except:
                return redirect('umapp:customerlogin')
        else:
            return redirect('umapp:customerlogin')
        return super(CustomerRequiredMixin, self).dispatch(request, *args, **kwargs)


   
    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     contract = LogisticsCustomerContract.objects.filter(second_party=self.customer, is_signed=False, first_party=self.branch, status='Active').last()
    #     # contract1 = LogisticsCustomerContract.objects.filter(second_party=self.customer, is_signed=False, first_party=self.branch, status='Disabled').last()
    #     if contract is None:
    #         pass
    #     else:
    #         cost = contract.logisticcustomerzonalcontract_set.all()
    #         if cost:
    #             context["counter"] = 1
    #         # elif contract1:

            
    #     context["customer"] = self.customer
    #     context["user"] = self.user
        

    #     return context
    

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        contract = LogisticsCustomerContract.objects.filter(second_party=self.customer, is_signed=False, status='Active')
        if contract is None:
            pass
        else:
            count = 0
            for i in contract:
                cost=i.logisticcustomerzonalcontract_set.all()
                if cost:
                    count += 1
                else:
                    pass

            # cost = contract.logisticcustomerzonalcontract_set.all()
            # if cost:
                # context["counter"] = 1
            # elif contract1:
        if count != 0:
            context["counter"] = count
        else:
            pass
        context["customer"] = self.customer
        context["user"] = self.user

        context['all'] = Shipment.objects.filter(customer=self.customer).count
        context['requested'] = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(customer=self.customer,shipmentactivity__id=F('latest'), shipmentactivity__shipment_status = "Shipment Requested").count()
        context['processing'] = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(customer=self.customer,shipmentactivity__id=F('latest'), shipmentactivity__shipment_status__in = ["Rider Assigned for Pickup", "Rider Picked up the Shipment", "Shipment Arrived at Warehouse", "Rider Assigned for Dispatch to the Receiver", "Rider Assigned for Dispatch to DC"]).count()
        context['dispatched'] = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(customer=self.customer,shipmentactivity__id=F('latest'), shipmentactivity__shipment_status__in = ["Rider Picked up the Shipment for Dispatch", "Shipment Delivered to the DC", "Rider Assigned for Dispatch in DC", "Shipment Dispatched to the Receiver in DC"]).count()
        context['rejected'] = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(customer=self.customer,shipmentactivity__id=F('latest'), shipmentactivity__shipment_status__in = ["Shipment Rejected", "Shipment Rejected in DC"]).count()
        context['delivered'] = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(customer=self.customer,shipmentactivity__id=F('latest'), shipmentactivity__shipment_status__in = ["Shipment Delivered to the Receiver", "Shipment Delivered to the Receiver in DC"]).count()
        context['returned'] = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(customer=self.customer,shipmentactivity__id=F('latest'), shipmentactivity__shipment_status__in = ["Shipment Returned to Logistics", "Shipment Ready to Return by DC", "Rider Assigned for Return to Logistics by DC", "Shipment Returned to the Logistics by DC", "Ready to Return to Sender", "Rider Assigned for Returning to the Sender", "Rider Picked up the Shipment for Return", "Shipment Returned to the Sender"]).count()
        context['canceled'] = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(customer=self.customer,shipmentactivity__id=F('latest'), shipmentactivity__shipment_status = "Shipment Canceled").count()
        

        return context

# Registration, Login and Logout View

class LogisticsAdminLoginView(FormView):
    template_name = "logisticstemplates/logisticsadminlogin.html"
    form_class = LogisticsAdminLoginForm
    success_url = reverse_lazy("lmsapp:lahome")

    def form_valid(self, form):
        a = form.cleaned_data["email"]
        b = form.cleaned_data["password"]
        usr = authenticate(username=a, password=b)
        if usr is not None:
            try:
                branchop = usr.branchoperator
                login(self.request, usr)
                messages.success(
                    self.request, f'Logged in as {usr.username} successfully.')
                
            except:
                messages.error(self.request, 'Invalid Email or Password')

                return render(self.request, self.template_name,
                              {"error": "Invalid Email or Password",
                               "form": form})

        else:
            messages.error(self.request, 'Invalid Email or Password')


            return render(self.request, self.template_name,
                          {"error": "Invalid Email or Password",
                           "form": form})

        return super().form_valid(form)

    def form_invalid(self, form):
        return super(LogisticsAdminLoginView, self).form_invalid(form)

    def get_success_url(self):
        if 'next' in self.request.GET:
            return self.request.GET.get('next')
        else:
            return self.success_url


class LogisticsAdminResetPasswordView(FormView):
    template_name = "logisticstemplates/logisticsadminresetpassword.html"
    form_class = LogisticsAdminResetPasswordForm
    success_url = reverse_lazy("lmsapp:lalogin")

    def form_valid(self, form):
        email = form.cleaned_data.get("email")
        if email is not None:
            return render(self.request, 'logisticstemplates/logisticsadminlogin.html', {"form": LogisticsAdminLoginForm, "message": "Reset link has been sent to your email."})
        else:
            return render(self.request, self.template_name, {"form": form, "error": "This email doesn't exist."})

        return super(LogisticsAdminResetPasswordView, self).form_valid(form)


class LogoutView(OperatorRequiredMixin, View):
    def get(self, request, **kwargs):
        if request.user.is_authenticated:
            logout(request)
            return redirect('umapp:logcompanylogin')
        else:
            raise Http404


class CustomerSignupView(CreateView):
    template_name = "customertemplates/customersignup.html"
    form_class = CustomerSignupForm
    success_url = reverse_lazy("lmsapp:lcdashboard")

    def form_valid(self, form):
        usr = form.cleaned_data["email"]
        pword = form.cleaned_data["password"]
        # print(usr, pword,"***************************   ")
        user = User.objects.create_user(usr, usr, pword)
        form.instance.user = user
        user_=authenticate(username=usr, password=pword)
        if user_ is not None:
            login(self.request, user_)
        else:
            return render(self.request, self.template_name,
                              {"error": "Invalid Email or Password",
                               "form": form})

        return super().form_valid(form)


class LogisticCompanySignupView(FormView):
    template_name = "logisticstemplates/logisticsignup.html"
    form_class = LogisticsSignupForm
    success_url = reverse_lazy("lmsapp:clienthome")
    # success_message = "succesfull"

    def form_valid(self, form):
        # for logistics company
        lc = form.save()
        lc.status = "Pending"
        lc.account_type = LogisticsAccountType.objects.first()
        lc.company_slug = slugify(lc.company_name)
        lc.save()
        # for logistics main branch
        branch_contact = form.cleaned_data.get("branch_contact")
        branch_alternative_contact = form.cleaned_data.get(
            "branch_alternative_contact")
        branch_email = form.cleaned_data.get("branch_email")
        branch_address = form.cleaned_data.get("branch_address")
        # branch_coverage = form.cleaned_data.get("branch_coverage")

        lc_branch = LogisticsBranch.objects.create(
            status="Pending", logistics_company=lc, branch_city=lc.company_city, branch_contact=branch_contact,
            branch_alternative_contact=branch_alternative_contact, branch_email=branch_email, branch_address=branch_address, is_main_branch=True)
        # for i in branch_coverage:
        #     lc_branch.branch_coverage.add(i)
        # lc_branch.branch_coverage.add(branch_coverage)
        # for super admin operator
        operator_email = form.cleaned_data.get("operator_email")
        operator_password = form.cleaned_data.get("operator_password")
        full_name = form.cleaned_data.get("operator_full_name")
        user = User.objects.create_user(
            operator_email, operator_email, operator_password)
        operator = BranchOperator.objects.create(
            status="Pending", logistics_branch=lc_branch, operator_type="SuperAdmin", user=user, full_name=full_name, mobile=lc_branch.branch_alternative_contact)

        delivery_type = DeliveryType.objects.create(logistics_branch=lc_branch, delivery_type='Standard')

        # messages.success(self.request, 'Form submission successful')
        # print(x)
        # return redirect('lmsapp:clienthome')
        messages.success(self.request, 'You have successfully signed up for the service. Right now, your account is in pending state. We will contact you soon. Thank you for choosing Shippers Park.')

        send_mail(
            'Signup Succesful',
            'Hi! uYo have successfully signed up in lms system. We will get back to you soon.',
            # 'sarobarcomp@gmail.com',
            settings.EMAIL_HOST_USER,
            [operator_email],
            fail_silently=False,
        )

        # send_mail(
        #     'New registration',
        #     'New user has been registered. Please verify',
        #     # 'sarobarcomp@gmail.com',
        #     settings.EMAIL_HOST_USER,
        #     ['alishprajapati33@gmail.com'],
        #     fail_silently=False,
        #     )
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)


def validateoperatoremailview(request):
    email = request.GET.get("email")
    user_obj = User.objects.filter(username=email).last()
    if user_obj:
        return JsonResponse({'errormsg': 'email already exists'})
    else:
        return JsonResponse({'success': 'email available'})

class LogisticCompanyLoginView(FormView):
    template_name = "logisticstemplates/logcompanylogin.html"
    form_class = LogisticsLoginForm
    success_url = reverse_lazy("lmsapp:lahome")
    # success_message = "succesfull"

    def form_valid(self, form):
        #print("form valid +++++++++++++")
        a = form.cleaned_data["email"]
        b = form.cleaned_data["password"]

        usr = authenticate(username=a, password=b)
        if usr is not None:
            try:
                branchop = usr.branchoperator
                if branchop.status=="Active":
                    login(self.request, usr)
                    messages.success(self.request, 'You are logged in ')
                else:
                    return render(self.request, self.template_name,
                              {"error": "Oops!! You don't seem to be an authorized user. Please contact administration.",
                               "form": form})

            except:
                messages.error(self.request, 'some eror ')

                return render(self.request, self.template_name,
                              {"error": "Invalid username or password",
                               "form": form})

        else:
            messages.error(self.request, 'some error ')

            return render(self.request, self.template_name,
                          {"error": "Invalid username or password",
                           "form": form})

        return super().form_valid(form)

    def form_invalid(self, form):
        print("Invalid")
        return super().form_invalid(form)

    def get_success_url(self):
        if 'next' in self.request.GET:
            return self.request.GET.get('next')
        else:
            return self.success_url


class LogisticCompanyLogoutView(View):

    def get(self, request):
        logout(request)
        messages.warning(self.request, 'You are logged out')
        # return redirect('lmsapp:logcompanyhome')

        return redirect("/")


class CustomerLoginView(FormView):
    template_name = "customertemplates/customerlogin.html"
    form_class = CustomerLoginForm
    success_url = reverse_lazy("lmsapp:lcdashboard")

    def form_valid(self, form):
        a = form.cleaned_data["email"]
        b = form.cleaned_data["password"]

        usr = authenticate(username=a, password=b)
        if usr is not None:
            try:
                logcust = usr.logisticscustomer
                print(logcust)
                login(self.request, usr)
                messages.success(self.request, 'You are logged in ')
            except:
                messages.error(self.request, 'some errors')
                return render(self.request, self.template_name,
                              {"error": "Invalid username or password", "form": form})
        else:
            messages.error(self.request, 'some errors')

            return render(self.request, self.template_name,
                          {"error": "Invalid username or password", "form": form})

        return super().form_valid(form)

    def form_invalid(self, form):
        print("Invalid")
        return super().form_invalid(form)


class CustomerLogoutView(View):

    def get(self, request):
        logout(request)

        return redirect("/")


class CustomerUsernameCheckerView(View):

    def get(self, request):
        user_input = request.GET["usr"]
        # print(user_input, '\n +++++++')
        if User.objects.filter(username=user_input).exists():
            message = "yes"
        else:
            message = "no"
        return JsonResponse({'response': message})


class LogisticCompanyForgotPasswordView(FormView):
    template_name = 'logisticstemplates/logcompanyforgotpassword.html'
    form_class = LogisticsForgotPasswordForm
    success_url = '/operator/forgot-password/?m=s'

    def form_valid(self, form):
        email = form.cleaned_data['email']
        user = User.objects.get(username=email)
        domain = self.request.META['HTTP_HOST']
        print(domain)
        text_content = "Please click the link below "
        html_content = "http://" + domain + "/operator/reset-password/" + \
            email + "/" + password_reset_token.make_token(user) + "/"
        send_mail(
            'Password Reset Link',
            text_content + html_content,
            settings.EMAIL_HOST_USER,
            [email],
            fail_silently=False,
        )

        return super().form_valid(form)


class LogisticCompanyResetPasswordView(FormView):
    template_name = "logisticstemplates/logcompanypasswordreset.html"
    form_class = LogisticsResetPasswordForm
    success_url = reverse_lazy("lmsapp:logisticsadminlogin")

    def form_valid(self, form):
        password = form.cleaned_data['confirm_password']
        email = self.kwargs['email']
        user = User.objects.get(username=email)
        user.set_password(password)
        user.save()
        user = authenticate(username=email, password=password)
        return super().form_valid(form)


class CustomerResetPasswordView(FormView):
    template_name = "customertemplates/customerresetpassword.html"
    form_class = CustomerResetPasswordForm
    success_url = reverse_lazy("umapp:customerlogin")

    def form_valid(self, form):
        password = form.cleaned_data['confirm_password']
        email = self.kwargs['email']
        user = User.objects.get(username=email)
        user.set_password(password)
        user.save()
        user = authenticate(username=email, password=password)
        return super().form_valid(form)


class CustomerForgotPasswordView(FormView):
    template_name = 'customertemplates/customerforgotpassword.html'
    form_class = CustomerForgotPasswordForm
    success_url = '/customer/forgot-password/?m=s'

    def form_valid(self, form):
        email = form.cleaned_data['email']
        user = User.objects.get(username=email)
        domain = self.request.META['HTTP_HOST']
        print(domain)
        text_content = "Please click the link below "
        html_content = "http://" + domain + "/customer/reset-password/" + \
            email + "/" + password_reset_token.make_token(user) + "/"
        send_mail(
            'Password Reset Link',
            text_content + html_content,
            settings.EMAIL_HOST_USER,
            [email],
            fail_silently=False,
        )

        return super().form_valid(form)


# System Operator Views


# Logistics Operator Views


class SettingView(OperatorRequiredMixin, TemplateView):
    template_name = 'logisticstemplates/settings/settings.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["cities"] = City.objects.all().order_by('name')
        context["province"] = Province.objects.all().order_by('name')
        context["districts"] = District.objects.all().order_by('name')
        context['countries'] = Country.objects.all().order_by('name')
        return context


class AjaxSettingsCitySearchView(View):
    def get(self, request):
        settings = self.request.GET.get('settings')
        keyword = self.request.GET.get('keyword')
        paginate_by = self.request.GET.get("paginate_by")
        try:
            paginate_by = int(paginate_by)
        except Exception:
            paginate_by = 25
        if settings == "district":
            settingslist = District.objects.all()
            settingslist = settingslist.filter(Q(name__icontains=keyword)) if keyword is not None and keyword != "" else settingslist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(settingslist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            # print(results, '+++++++++++++++++++')
            context ={
                'settingslist':results,
                'paginate_by':paginate_by,
            }
            self.template_name = "logisticstemplates/settings/ajaxdistrictsearchlist.html"
        elif settings == "province":
            settingslist = Province.objects.all()
            settingslist = settingslist.filter(Q(name__icontains=keyword)) if keyword is not None and keyword != "" else settingslist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(settingslist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            # print(results, '+++++++++++++++++++')
            context ={
                'settingslist':results,
                'paginate_by':paginate_by,
            }
            self.template_name = "logisticstemplates/settings/ajaxprovincesearchlist.html"
        elif settings == "country":
            settingslist = Country.objects.all()
            settingslist = settingslist.filter(Q(name__icontains=keyword)) if keyword is not None and keyword != "" else settingslist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(settingslist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            # print(results, '+++++++++++++++++++')
            context ={
                'settingslist':results,
                'paginate_by':paginate_by,
            }
            self.template_name = "logisticstemplates/settings/ajaxcountrysearchlist.html"
        else:
            settingslist = City.objects.all()
            settingslist = settingslist.filter(Q(name__icontains=keyword)) if keyword is not None and keyword != "" else settingslist
            page = self.request.GET.get('page', 1)
            # print(settingslist)
            paginator = Paginator(settingslist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            print(results, '+++++++++++++++++++')
            context ={
                'settingslist':results,
                'paginate_by':paginate_by,
            }
            self.template_name = "logisticstemplates/settings/ajaxcitysearchlist.html"
        return render(self.request, self.template_name, context)
        

# class AjaxSettingsDistrictSearchView(View):
#     def get(self, requset):
#         district = self.request.GET['district']
#         district_list = District.objects.filter(Q(name__icontains=district))
#         return render(self.request, "logisticstemplates/settings/ajaxdistrictsearchlist.html", {
#             'districtlist': district_list,
#         })

# class AjaxSettingsProvinceSearchView(View):
#     def get(self, request):
#         province = self.request.GET['province']
#         province_list = Province.objects.filter(Q(name__icontains=province))
#         return render(self.request, "logisticstemplates/settings/ajaxprovincesearchlist.html", {
#             'provincelist': province_list,
#         })

# class AjaxSettingsCountrySearchView(View):
#     def get(self, request):
#         country = self.request.GET['country']
#         country_list = Country.objects.filter(Q(name__icontains=country))
#         return render(self.request, "logisticstemplates/settings/ajaxcountrysearchlist.html", {
#             'countrylist': country_list,
        # })

        


class CountryCreateView(OperatorRequiredMixin, CreateView):
    template_name = 'logisticstemplates/settings/countrycreate.html'
    form_class = CountryForm
    # success_message = 'Country Created Successfully'
    # success_url = reverse_lazy('umapp:settings')

    def post(self, request, *args, **kwargs):
        form = CountryForm(request.POST)
        name = request.POST['name']
        country = Country.objects.filter(name=name)
        if country:
            messages.error(
                request, "Country already exists don't be over smart...")
            return redirect('umapp:settings')
        else:
            form.save()
            messages.success(request, 'Contry created successfully')
            return redirect('umapp:settings')


def CountryAjaxCheck(request):
    country_name = request.GET.get('country_name')
    name = country_name.capitalize()
    country = Country.objects.filter(name=name)
    if country:
        data = {
            'msg': 'country not available',
            'name': str(name)
        }
    else:
        data = {
            'msg': 'country available',
            'name': str(name),
        }
    return JsonResponse({'data': data})


class CountryUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = 'logisticstemplates/settings/countryupdate.html'
    form_class = CountryUpdateForm
    model = Country
    success_message = 'country updated successfully'
    success_url = reverse_lazy('umapp:settings')


class ProvinceCreateView(OperatorRequiredMixin, CreateView):
    template_name = 'logisticstemplates/settings/provincecreate.html'
    form_class = ProvinceForm

    def post(self, request, *args, **kwargs):
        form = ProvinceForm(request.POST)
        name = request.POST['name']
        province = Province.objects.filter(name=name)
        if province:
            messages.error(
                request, "Province already exists don't be over smart...")
            return redirect('umapp:settings')
        else:
            form.save()
            messages.success(request, 'Province created successfully')
            return redirect('umapp:settings')


def ProvinceAjaxCheck(request):
    province_name = request.GET.get('province_name')
    name = province_name.capitalize()
    province = Province.objects.filter(name=name)
    if province:
        data = {
            'msg': 'province not available',
            'name': name
        }
    else:
        data = {
            'msg': 'province available',
            'name': name
        }
    return JsonResponse({'data': data})


class ProvinceUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = "logisticstemplates/settings/provinceupdate.html"
    form_class = ProvinceUpdateForm
    model = Province
    success_url = reverse_lazy('umapp:settings')
    success_message = 'province updated succesfullt'


class ProvinceDelete(OperatorRequiredMixin, DeleteView):
    template_name = "logisticstemplates/settings/provincedelete.html"
    model = Province
    success_url = reverse_lazy('umapp:settings')
    success_message = 'Province deleted successfully'


class DistrictCreateView(OperatorRequiredMixin, CreateView):
    template_name = 'logisticstemplates/settings/districtcreate.html'
    form_class = DistrictForm

    def post(self, request, *args, **kwargs):
        form = DistrictForm(request.POST)
        name = request.POST['name']
        district = District.objects.filter(name=name)
        if district:
            messages.error(
                request, "District already exists don't be over smart...")
            return redirect('umapp:settings')
        else:
            form.save()
            messages.success(request, 'District created successfully')
            return redirect('umapp:settings')


def DistrictAjaxCheck(request):
    district_name = request.GET.get('district_name')
    name = district_name.capitalize()
    district = District.objects.filter(name=name)
    if district:
        data = {
            'msg': 'district not available',
            'name': name
        }
    else:
        data = {
            'msg': 'district available',
            'name': name
        }
    return JsonResponse({'data': data})


class DistrictUpdate(OperatorRequiredMixin, UpdateView):
    template_name = 'logisticstemplates/settings/districtupdate.html'
    model = District
    form_class = DistrictUpdateForm
    # fields = ['id','name', 'province', 'slug']
    success_url = reverse_lazy('umapp:settings')
    success_message = 'district updated successfullt'


class CityCreateView(OperatorRequiredMixin, CreateView):
    template_name = 'logisticstemplates/settings/citycreate.html'
    form_class = CityForm

    def post(self, request, *args, **kwargs):
        form = CityForm(request.POST)
        name = request.POST['name']
        city = City.objects.filter(name=name)
        if city:
            messages.error(
                request, "City already exists don't be over smart...")
            return redirect('umapp:settings')
        else:
            form.save()
            messages.success(request, 'City created successfully')
            return redirect('umapp:settings')


def CityAjaxCheck(request):
    city_name = request.GET.get('city_name')
    name = city_name.capitalize()
    city = City.objects.filter(name=name)
    if city:
        data = {
            'msg': 'city not available',
            'name': name
        }
    else:
        data = {
            'msg': 'city available',
            'name': name
        }
    return JsonResponse({'data': data})


class CityUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = 'logisticstemplates/settings/cityupdate.html'
    form_class = CityUpdateForm
    model = City
    success_url = reverse_lazy('umapp:settings')
    success_message = 'city updated successfully'



class InitialSetupView(View):
    def get(self, request, *args, **kwargs):
        json_file = open("location.json")
        data = json.load(json_file)
        country, c_created = Country.objects.get_or_create(name="Nepal", slug="nepal")
        city_list = []
        for p in data['provinces']:
            province, p_created = Province.objects.get_or_create(country=country, name=p["name"], slug=slugify(p["name"]))
            for d in p["districts"]:
                district, d_created = District.objects.get_or_create(name=d["name"], slug=slugify(d["name"]), province=province)
                for c in d["cities"]:
                    try:
                        city, c_created = City.objects.get_or_create(name=c, slug=slugify(c), district=district)
                    except Exception as e:
                        city, c_created = City.objects.get_or_create(name=c, slug=slugify(c)+ '-' + district.slug, district=district)

        json_file.close()
        resp = {
            "status":"ok"
        }
        return redirect("lmsapp:clienthome")

class BranchOperatorListView(OperatorRequiredMixin, TemplateView):
    template_name = 'logisticstemplates/settings/branchoperatorlist.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        logistics = self.request.user.branchoperator.logistics_branch

        context["operators"] = BranchOperator.objects.filter(
            logistics_branch=self.request.user.branchoperator.logistics_branch).order_by('id')
        return context


class BranchOperatorCreateView(OperatorRequiredMixin, CreateView):
    template_name = 'logisticstemplates/settings/operatorcreate.html'
    form_class = BranchOperatorForm
    success_url = reverse_lazy('umapp:operatorlist')

    # def get_form_kwargs(self):
    #     """ Passes the request object to the form class.
    #      This is necessary to only display members that belong to a given user"""

    #     kwargs = super(BranchOperatorCreateView, self).get_form_kwargs()
    #     kwargs['comp'] = self.request.user.branchoperator.logistics_branch.logistics_company
    #     return kwargs

    def form_valid(self, form):
        email = form.cleaned_data['email']
        password = form.cleaned_data['password']
        usr = User.objects.create_user(email, email, password)
        form.instance.user = usr
        form.instance.logistics_branch = self.request.user.branchoperator.logistics_branch
        form.instance.operator_type = 'Admin'
        form.instance.status = 'Active'
        form.save()
        messages.success(
            self.request, 'new branch operator created successfully')
        return redirect('umapp:operatorlist')
        return super(BranchOperatorCreateView, self).form_valid(form)

    def form_invalid(self, form):
        print(form)
        print('invalid')


class BranchOperatorUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = 'logisticstemplates/settings/operatorupdate.html'
    model = BranchOperator
    form_class = BranchOperatorUpdateForm
    success_message = 'operator updated successfully'
    success_url = reverse_lazy('umapp:operatorlist')

    def dispatch(self, request, *args, **kwargs):
        branch_id = self.request.user.branchoperator.logistics_branch.id
        if branch_id == BranchOperator.objects.get(id=self.kwargs['pk']).logistics_branch.id:
            pass
        else:
            messages.error(request, 'Authorization failed')
            return redirect('umapp:operatorlist')

        return super(BranchOperatorUpdateView, self).dispatch(request, *args, **kwargs)

    # def get_form_kwargs(self):
    #     """ Passes the request object to the form class.
    #      This is necessary to only display branch that belong to a given company"""

    #     kwargs = super(BranchOperatorUpdateView, self).get_form_kwargs()
    #     kwargs['comp'] = self.request.user.branchoperator.logistics_branch.logistics_company
    #     return kwargs


def validateoperatoremailview(request):
    email = request.POST["email"]
    user_obj = User.objects.filter(email=email).last()
    if user_obj:
        return JsonResponse({'errormsg': 'email already exists'})
    else:
        return JsonResponse({'success': 'email available'})


class BranchOperatorPasswordChangeView(OperatorRequiredMixin, FormView):
    template_name = 'logisticstemplates/settings/operatorpasschange.html'
    form_class = ChangeOperatorPasswordForm
    success_url = reverse_lazy('umapp:operatorlist')

    def dispatch(self, request, *args, **kwargs):
        branch_id = self.request.user.branchoperator.logistics_branch.id
        try:
            if branch_id == BranchOperator.objects.get(id=self.kwargs['pk']).logistics_branch.id:
                pass
            else:
                messages.error(request, 'Authorization failed')
                return redirect('umapp:operatorlist')
        except Exception as e:
            messages.error(request, 'Authorization Failed')
            return redirect('umapp:shippinglist')

        return super(BranchOperatorPasswordChangeView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        c_new_password = form.cleaned_data['confirm_new_password']
        branchop_id = BranchOperator.objects.get(id=self.kwargs["pk"])
        usr = branchop_id.user
        if usr is not None:
            usr.set_password(c_new_password)
            usr.save()
        else:
            return render(self.request, self.template_name, {
                'form': form,
                'error': "Invalid Current password!"})
        return super(BranchOperatorPasswordChangeView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BranchOperatorPasswordChangeView,
                        self).get_context_data(**kwargs)
        context["operatorpasschange"] = BranchOperator.objects.get(
            id=self.kwargs['pk'])
        return context


class LogisticBranchShippingListZoneView(OperatorRequiredMixin, TemplateView):
    template_name = 'logisticstemplates/settings/logisticbranchshippingzonelist.html'

    def get_context_data(self, **kwargs):
        context = super(LogisticBranchShippingListZoneView,
                        self).get_context_data(**kwargs)
        l = LogisticsBranchShippingZone.objects.filter(
            logistics_branch=self.request.user.branchoperator.logistics_branch.id)
        context["shippingzone"] = LogisticsBranchShippingZone.objects.filter(
            logistics_branch=self.request.user.branchoperator.logistics_branch)
        a = LogisticsBranchShippingZone.objects.filter(
            logistics_branch=self.request.user.branchoperator.logistics_branch)
        context['shippingzoneform'] = LogisticBranchShippingZoneForm
        context["delivery_types"] = DeliveryType.objects.filter(
            logistics_branch=self.request.user.branchoperator.logistics_branch)
        # annot = LogisticsBranchShippingZone.objects.filter(logistics_branch=self.request.user.branchoperator.logistics_branch.id).values('shipping_zone').distinct()
        # zones = []
        # for b in annot:
        #     zoneobj = {}
        #     n = b['shipping_zone']
        #     m = LogisticsBranchShippingZone.objects.filter(shipping_zone__iexact=n, logistics_branch=self.request.user.branchoperator.logistics_branch.id).values('delivery_type__delivery_type', 'id')
        #     zoneobj['title'] = n
        #     zoneobj['content'] = m
        #     zones.append(zoneobj)
        # context['zones'] = zones
        return context


class LogisticBranchShippingZoneCreateView(OperatorRequiredMixin, CreateView):
    template_name = 'logisticstemplates/settings/logisticbranchshippingzonecreate.html'
    form_class = LogisticBranchShippingZoneForm
    model = LogisticsBranchShippingZone
    success_url = reverse_lazy('umapp:shippinglist')

    def get_context_data(self, **kwargs):
        context = super(LogisticBranchShippingZoneCreateView,
                        self).get_context_data(**kwargs)
        context["delivery_types"] = DeliveryType.objects.filter(
            logistics_branch=self.request.user.branchoperator.logistics_branch)
        return context

    def form_valid(self, form):
        form.instance.logistics_branch = self.request.user.branchoperator.logistics_branch
        form.instance.status = "Active"
        sz = form.save()
        dtypes = DeliveryType.objects.filter(
            logistics_branch=self.request.user.branchoperator.logistics_branch)
        for dt in dtypes:
            if dt.status == 'Active':
                shipping_status = "shipping_zone_status__" + str(dt.id)
                shipping_charge = "shipping_charge__" + str(dt.id)
                additional_shipping_charge = "additional_shipping_charge__" + \
                    str(dt.id)
                cod_handling_charge = "cod_handling__" + str(dt.id)
                rejection_handling_charge = "rejection_charge__" + str(dt.id)
                shippingcharge = self.request.POST.get(shipping_charge, 0)
                additionalshippingcharge = self.request.POST.get(
                    additional_shipping_charge, 0)
                rejectionhandlingcharge = self.request.POST.get(
                    rejection_handling_charge, 0)
                codhandlingcharge = self.request.POST.get(
                    cod_handling_charge, 0)
                status = self.request.POST.get(shipping_status, 0)

                ShippingZoneDeliveryType.objects.create(shippng_zone=sz, delivery_type=dt, shipping_charge_per_kg=shippingcharge, additional_shipping_charge_per_kg=additionalshippingcharge,
                                                        cod_handling_charge=codhandlingcharge, rejection_handling_charge=rejectionhandlingcharge, status='Active')

                messages.success(
                    self.request, 'Shipping Zone Created Successfully !!')
            else:
                ShippingZoneDeliveryType.objects.create(shippng_zone=sz, delivery_type=dt, shipping_charge_per_kg=0,
                                                        additional_shipping_charge_per_kg=0, cod_handling_charge=0, rejection_handling_charge=0, status='Disabled')
                messages.success(
                    self.request, 'Shipping Zone Created Successfully !!')
        return super(LogisticBranchShippingZoneCreateView, self).form_valid(form)


class LogisticBranchShippingZoneUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = 'logisticstemplates/settings/logisticbranchshippingzoneupdate.html'
    model = LogisticsBranchShippingZone
    form_class = LogisticBranchShippingZoneUpdateForm
    success_url = reverse_lazy('umapp:shippinglist')
    success_message = 'logistic zone updated successfully'

    def dispatch(self, request, *args, **kwargs):
        resp = super(LogisticBranchShippingZoneUpdateView,
                     self).dispatch(request, *args, **kwargs)
        branch_id = self.branch.id
        try:
            branch_ids = LogisticsBranchShippingZone.objects.get(
                id=self.kwargs['pk'])
            if branch_id == branch_ids.logistics_branch.id:
                pass
            else:
                messages.error(request, 'Authorization Failed')
                return redirect('umapp:shippinglist')
        except Exception as e:
            messages.error(request, 'Authorization Failed')
            return redirect('umapp:shippingdetail')

        return resp

    def get_context_data(self, **kwargs):
        context = super(LogisticBranchShippingZoneUpdateView,
                        self).get_context_data(**kwargs)
        context["delevery_type"] = DeliveryType.objects.filter(
            logistics_branch=self.request.user.branchoperator.logistics_branch)
        context['zone'] = LogisticsBranchShippingZone.objects.get(
            id=self.kwargs['pk'])
        context['form2'] = LogisticBranchShippingZoneDeliveryUpdateForm
        return context

    def form_valid(self, form):
        form.instance.logistics_branch = self.request.user.branchoperator.logistics_branch
        form.instance.status = "Active"
        sz = form.save()
        dtypes = DeliveryType.objects.filter(
            logistics_branch=self.request.user.branchoperator.logistics_branch)
        shiping = ShippingZoneDeliveryType.objects.filter(shippng_zone=sz)
        for dt in shiping:
            if dt.delivery_type.status == 'Active':
                shipping_status = "shipping_zone_status__" + str(dt.id)
                delivery_type = str(dt.delivery_type)
                shipping_charge = "shipping_charge__" + str(dt.id)
                additional_shipping_charge = "additional_shipping_charge__" + \
                    str(dt.id)
                cod_handling_charge = "cod_handling__" + str(dt.id)
                rejection_handling_charge = "rejection_charge__" + str(dt.id)
                shippingcharge = self.request.POST.get(shipping_charge, 0)
                additionalshippingcharge = self.request.POST.get(
                    additional_shipping_charge, 0)
                rejectionhandlingcharge = self.request.POST.get(
                    rejection_handling_charge, 0)
                codhandlingcharge = self.request.POST.get(
                    cod_handling_charge, 0)
                delivery_type = self.request.POST.get(delivery_type, 0)
                status = self.request.POST.get(shipping_status, 0)
                dt.shipping_charge_per_kg = shippingcharge
                dt.additional_shipping_charge_per_kg = additionalshippingcharge
                dt.cod_handling_charge = codhandlingcharge
                dt.rejection_handling_charge = rejectionhandlingcharge
                if status == 'on':
                    dt.status = 'Active'
                else:
                    dt.status = 'Disabled'
                dt.save()
            else:
                dt.shipping_charge_per_kg = dt.shipping_charge_per_kg
                dt.additional_shipping_charge_per_kg = dt.additional_shipping_charge_per_kg
                dt.cod_handling_charge = dt.cod_handling_charge
                dt.rejection_handling_charge = dt.rejection_handling_charge
                dt.status = 'Disabled'
                dt.save()
            messages.success(
                self.request, 'Shipping Zone updated Successfully !!')
        return super(LogisticBranchShippingZoneUpdateView, self).form_valid(form)
    

    def get_success_url(self, **kwargs):
        return reverse_lazy("umapp:shippingdetail", kwargs={'pk': self.object.id})




class logisticsBranchShippingZoneDetailView(OperatorRequiredMixin, DetailView):
    template_name = 'logisticstemplates/settings/logisticbranchshippingzonedetail.html'
    model = LogisticsBranchShippingZone
    context_objects_name = 'shipdetail'

    def dispatch(self, request, *args, **kwargs):
        branch_id = request.user.branchoperator.logistics_branch
        try:
            if branch_id == LogisticsBranchShippingZone.objects.get(id=self.kwargs['pk']).logistics_branch:
                pass
            else:
                messages.error(request, 'Authorization Failed')
                return redirect('umapp:shippinglist')
        except Exception as e:
            messages.error(request, 'Authorization Failed')
            return redirect('umapp:shippinglist')
        return super(logisticsBranchShippingZoneDetailView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        branch_id = self.request.user.branchoperator.logistics_branch.id
        zone_obj = LogisticsBranchShippingZone.objects.get(id=self.kwargs['pk'])
        city_id = request.POST.get('city_id')
        remove_city = request.POST.get('remove_city')
        if LogisticsBranchShippingZone.objects.filter(logistics_branch=self.branch,cities__id = city_id).exists():
            message = "City Already Exists or Exists In Another Shippping Zone"
        else:
            zone_obj.cities.add(city_id)
            message = 'City Added Successfully'
        zone_obj.cities.remove(remove_city)
        rmv_msg = 'City Removed Successfully!!'
        return JsonResponse({
            'message':message,
            'rmv_msg':rmv_msg,
        })
        

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["shippingzone"] = DeliveryType.objects.filter(
            logistics_branch=self.request.user.branchoperator.logistics_branch)
        context['zone'] = LogisticsBranchShippingZone.objects.get(
            id=self.kwargs['pk'])   
        context["zone_cities"] = LogisticsBranchShippingZone.objects.get(id=self.kwargs['pk']).cities.all()
        return context

# class CitySearchSerializerView(serializers.ModelSerializer):
#     value = serializers.SerializerMethodField()

#     class Meta:
#         model = City
#         fields = ["name", "district", 'value']
#     def get_value(self, obj):
#         return obj.name + '(District:' + obj.district.name + ')' + str(obj.id)       

class AjaxCitySearchView(View):
    def get(self, request, *args, **kwargs):
        keyword = self.request.GET.get('term')
        if keyword is not None and keyword != "":
            allcity = City.objects.filter(name__icontains = keyword)
        else:
            allcity = City.objects.none()
        return render(self.request, 'logisticstemplates/settings/ajax_search.html', {
            'allcity':allcity,
        })


class OperatorAccountDetailView(OperatorRequiredMixin, TemplateView):
    template_name = 'logisticstemplates/settings/operatoraccounts.html'


class DeliveryTypeView(OperatorRequiredMixin, TemplateView):
    template_name = 'logisticstemplates/settings/deliverytype.html'

    def get_context_data(self, **kwargs):
        context = super(DeliveryTypeView, self).get_context_data(**kwargs)
        context["deliveryform"] = DeliveryTypeForm
        return context

    def post(self, request, *args, **kwargs):
        form = DeliveryTypeForm(request.POST, request.FILES)
        if form.is_valid():
            form.instance.logistics_branch = self.request.user.branchoperator.logistics_branch
            form.instance.status = 'Active'
            delivery = form.save()
            shiping = LogisticsBranchShippingZone.objects.filter(
                logistics_branch=self.request.user.branchoperator.logistics_branch)
            for ship in shiping:
                ShippingZoneDeliveryType.objects.create(status='Active', shippng_zone=ship, delivery_type=delivery, shipping_charge_per_kg=0,
                                                        additional_shipping_charge_per_kg=0, cod_handling_charge=0, rejection_handling_charge=0)
            return redirect('umapp:shippinglist')


class OperatorCustomersListView(OperatorRequiredMixin,ListView):
    template_name = 'logisticstemplates/opcustomerslist.html'
    paginate_by = 5
    queryset = LogisticsCustomer.objects.all()
    context_object_name = "clist"

    def get_queryset(self, **kwargs):
        queryset = LogisticsCustomer.objects.filter(creator_branch = self.branch)
        # queryset = LogisticsCustomer.objects.filter(connected_logistics=self.branch)
        return queryset


class OperatorCustomersCreateView(OperatorRequiredMixin, CreateView):
    template_name = "logisticstemplates/opcustomerscreate.html"
    form_class = OperatorCustomersCreateForm
    success_url = reverse_lazy("umapp:opcustomerslist")

    def form_valid(self, form):
        usr = form.cleaned_data["email"]
        pword = form.cleaned_data["password"]
        print(usr, pword)
        user = User.objects.create_user(usr, usr, pword)
        form.instance.user = user
        form.instance.creator_branch = self.branch
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "Create"
        return context


class OperatorCustomersUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = "logisticstemplates/opcustomersupdate.html"
    form_class = OperatorCustomersUpdateForm
    model = LogisticsCustomer
    success_url = reverse_lazy("umapp:opcustomerslist")

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context["page"] = "Update"
    #     return context


class OperatorCustomersPasswordChangeView(OperatorRequiredMixin, FormView):
    template_name = "logisticstemplates/opcustomerspc.html"
    form_class = OperatorCustomersPasswordUpdateForm
    success_url = reverse_lazy("umapp:opcustomerslist")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['id'] = self.kwargs['pk']
        return context

    def form_valid(self, form):
        new_password = form.cleaned_data['new_password']
        confirm_password = form.cleaned_data['confirm_password']
        user_id = self.kwargs['pk']
        print(user_id)
        user = User.objects.get(id=user_id)
        print(user, '++++++++++++++++++++++++++++++++')
        if user is not None:
            user.set_password(confirm_password)
            user.save()
        else:
            return render(self.request, self.template_name, {
                'form': form,
                'error': "Invalid Current password!"})
        return super().form_valid(form)


class OperatorCustomersDetailView(OperatorRequiredMixin, DetailView):
    template_name = "logisticstemplates/opcustomersdetail.html"
    model = LogisticsCustomer
    context_object_name = "cdetail"


class OperatorDeliveryPersonCreateView(OperatorRequiredMixin, CreateView):
    template_name = "logisticstemplates/opdeliverypersoncreate.html"
    form_class = OperatorDeliveryPersonCreateForm
    success_url = reverse_lazy("umapp:opdeliverypersonlist")

    def form_valid(self, form):
        usr = form.cleaned_data["email"]
        pword = form.cleaned_data["password"]
        print(usr, pword)
        user = User.objects.create_user(usr, usr, pword)
        form.instance.user = user
        form.instance.logistics_branch = self.branch
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page"] = "Create"
        return context


class OperatorDeliveryPersonListView(OperatorRequiredMixin,ListView):
    template_name = 'logisticstemplates/opdeliverypersonlist.html'
    paginate_by = 5
    queryset = DeliveryPerson.objects.all()
    context_object_name = "deliverypersonlist"

    def get_queryset(self, **kwargs):
        # queryset = LogisticsCustomer.objects.filter(creator_branch = self.branch)
        queryset = DeliveryPerson.objects.filter(logistics_branch=self.branch)
        return queryset


class OperatorDeliveryPersonDetailView(OperatorRequiredMixin, DetailView):
    template_name = "logisticstemplates/opdeliverypersondetail.html"
    model = DeliveryPerson
    context_object_name = "dpdetail"


class OperatorDeliveryPersonUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = "logisticstemplates/opdeliverypersonupdate.html"
    form_class = OperatorDeliveryPersonUpdateForm
    model = DeliveryPerson
    success_url = reverse_lazy("umapp:opdeliverypersonlist")


class OperatorDeliveryPersonPasswordChangeView(OperatorRequiredMixin, FormView):
    template_name = "logisticstemplates/opdeliverypersonpc.html"
    form_class = OperatorDeliveryPersonPasswordUpdateForm
    success_url = reverse_lazy("umapp:opdeliverypersonlist")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['id'] = self.kwargs['pk']
        return context

    def form_valid(self, form):
        new_password = form.cleaned_data['new_password']
        confirm_password = form.cleaned_data['confirm_password']
        user_id = self.kwargs['pk']
        print(user_id)
        user = User.objects.get(id=user_id)
        print(user, '++++++++++++++++++++++++++++++++')
        if user is not None:
            user.set_password(confirm_password)
            user.save()
        else:
            return render(self.request, self.template_name, {
                'form': form,
                'error': "Invalid Current password!"})
        return super().form_valid(form)


class LogisticAdminLogisticsCustomerPaymentListView(OperatorRequiredMixin, ListView):
    template_name = 'logisticstemplates/lalogisticscustomerslist.html'
    paginate_by = 25
    queryset = LogisticsCustomer.objects.all()
    context_object_name = "logisticscustomerlist"

    def get_queryset(self, **kwargs):
        queryset = super().get_queryset()
        new_queryset = queryset.filter(connected_logistics=self.branch)
        return new_queryset


class DeliveryTypeUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = 'logisticstemplates/settings/deliverytypeupdate.html'
    model = DeliveryType
    form_class = DeliveryTypeForm
    success_message = 'DeliveryType Updated Successfully'
    success_url = reverse_lazy('umapp:shippinglist')


# class checkPasswordMatch(View):

#     def get(self, request):
#         user_input = request.GET["usr"]
#         # print(user_input, '\n +++++++')
#         if User.objects.filter(username=user_input).exists():
#             message = "yes"
#         else:
#             message = "no"
#         return JsonResponse({'response': message})

# Logistics Customer Views






