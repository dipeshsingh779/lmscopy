from django.contrib import admin
from .models import *


admin.site.register([Country, Province, District, City, SystemOperator, LogisticsAccountType, LogisticsCompany, LogisticsBranch,
                     DeliveryPerson, BranchOperator, LogisticsCustomer, SignInAttempt, DeliveryType, LogisticsBranchShippingZone, ShippingZoneDeliveryType, LogisticCustomerZonalContract])

class LogisticsCustomerContractInline(admin.TabularInline):
    model = LogisticCustomerZonalContract
    extra = 1


class LogisticsCustomerContractAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_party', 'second_party')
    inlines = [LogisticsCustomerContractInline]

admin.site.register(LogisticsCustomerContract, LogisticsCustomerContractAdmin)