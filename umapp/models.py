from django.contrib.auth.models import User
from umapp.constants import *
from django.db import models


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=20, choices=STATUS)

    class Meta:
        abstract = True


class Country(TimeStamp):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.name


class Province(TimeStamp):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    country = models.ForeignKey(
        Country, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class District(TimeStamp):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    province = models.ForeignKey(Province, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class City(TimeStamp):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    district = models.ForeignKey(District, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name



class SystemOperator(TimeStamp): # system owner account
    operator_type = models.CharField(
        max_length=50, choices=SYSTEM_OPERATOR_TYPE, null=True, blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=200)
    image = models.ImageField(
        upload_to="operators/profile/", null=True, blank=True)
    mobile = models.CharField(max_length=15)
    alt_mobile = models.CharField(max_length=15, null=True, blank=True)
    document1 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    document2 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    document3 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)

    def __str__(self):
        return self.full_name


class LogisticsAccountType(TimeStamp):
    account_type = models.CharField(max_length=200)
    allowed_operators = models.PositiveIntegerField(null=True, blank=True)
    allowed_operators_per_branch = models.PositiveIntegerField(
        null=True, blank=True)
    allowed_riders = models.PositiveIntegerField(null=True, blank=True)
    allowed_riders_per_branch = models.PositiveIntegerField(
        null=True, blank=True)
    allowed_shipments_per_month = models.PositiveBigIntegerField(
        null=True, blank=True)
    per_shipment_charge = models.IntegerField(null=True, blank=True)
    per_month_charge = models.PositiveBigIntegerField(null=True, blank=True)
    per_branch_charge = models.PositiveBigIntegerField(null=True, blank=True)
    per_operator_charge = models.PositiveBigIntegerField(null=True, blank=True)

    def __str__(self):
        return self.account_type


# company specific information

class LogisticsCompany(TimeStamp):
    account_type = models.ForeignKey(
        LogisticsAccountType, on_delete=models.SET_NULL, null=True, blank=True)
    company_name = models.CharField(max_length=200)
    company_slug = models.SlugField(max_length=220)
    company_logo = models.ImageField(upload_to="company/logo")
    company_profile = models.ImageField(
        upload_to="company/profile", null=True, blank=True)
    company_description = models.TextField(null=True, blank=True)
    company_city = models.ForeignKey(City, on_delete=models.RESTRICT)
    company_website = models.URLField(null=True, blank=True)
    company_email = models.EmailField(null=True, blank=True)
    is_distribution_center = models.BooleanField(default=False)
    connected_logistics = models.ManyToManyField("self", blank=True)
    document1 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    document2 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    document3 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)

    def __str__(self):
        return self.company_name


class LogisticsBranch(TimeStamp):
    logistics_company = models.ForeignKey(
        LogisticsCompany, on_delete=models.CASCADE)
    branch_city = models.ForeignKey(City, on_delete=models.RESTRICT)
    branch_contact = models.CharField(max_length=20)
    branch_alternative_contact = models.CharField(
        max_length=20, null=True, blank=True)
    branch_email = models.EmailField()
    branch_coverage = models.ManyToManyField(
        City, related_name="citybranches")  # local area for pickup and dropoff
    branch_address = models.CharField(max_length=200)
    is_main_branch = models.BooleanField(default=False)

    def __str__(self):
        return self.branch_city.name + "(" + self.logistics_company.company_name + ")"



class BranchOperator(TimeStamp):
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.CASCADE)
    operator_type = models.CharField(
        max_length=50, choices=OPERATOR_TYPE, null=True, blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=200)
    address_city = models.ForeignKey(
        City, on_delete=models.SET_NULL, null=True, blank=True)
    image = models.ImageField(
        upload_to="operators/profile/", null=True, blank=True)
    mobile = models.CharField(max_length=15)
    alt_mobile = models.CharField(max_length=15, null=True, blank=True)
    document1 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    document2 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    document3 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    

    def __str__(self):
        return self.full_name + ' (' + str(self.user.username) + ')'


class DeliveryPerson(TimeStamp):
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.CASCADE)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=200)
    image = models.ImageField(
        upload_to="operators/profile/", null=True, blank=True)
    mobile = models.CharField(max_length=15)
    alt_mobile = models.CharField(max_length=15, null=True, blank=True)
    document1 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    document2 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    document3 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    # for freelance riders
    address_city = models.ForeignKey(
        City, on_delete=models.SET_NULL, null=True, blank=True)
    coverage_cities = models.ManyToManyField(
        City, related_name="cityfreelanceriders", blank=True)
    connected_branches = models.ManyToManyField(
        LogisticsBranch, blank=True, related_name="branchfreelanceriders")

    def __str__(self):
        return self.full_name



class DeliveryType(TimeStamp):
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.CASCADE)
    delivery_type = models.CharField(max_length=200)
    image = models.ImageField(
        upload_to="deliverytypes/", null=True, blank=True)

    def __str__(self):
        return self.delivery_type


class LogisticsBranchShippingZone(TimeStamp):
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.CASCADE)
    shipping_zone = models.CharField(max_length=200)
    cities = models.ManyToManyField(City)

    def __str__(self):
        return self.shipping_zone + "(" + self.logistics_branch.logistics_company.company_name + ")"


class ShippingZoneDeliveryType(TimeStamp):
    shippng_zone = models.ForeignKey(
        LogisticsBranchShippingZone, on_delete=models.CASCADE)
    delivery_type = models.ForeignKey(DeliveryType, on_delete=models.RESTRICT)
    shipping_charge_per_kg = models.IntegerField(default=0)
    additional_shipping_charge_per_kg = models.IntegerField(default=0)
    cod_handling_charge = models.IntegerField(default=0)
    rejection_handling_charge = models.IntegerField(default=0)

    def __str__(self):
        return self.delivery_type.delivery_type


class LogisticsCustomer(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    customer_type = models.CharField(
        max_length=50, choices=CUSTOMER_TYPE, default="Business Customer")
    full_name = models.CharField(max_length=200)
    # profile_image = models.ImageField(upload_to = "logisticcustomer/profile", null=True, blank=True)
    company_name = models.CharField(max_length=200, null=True, blank=True)
    contact_number = models.CharField(max_length=20)
    alternative_contact_number = models.CharField(
        max_length=20, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.RESTRICT)
    address = models.CharField(max_length=200)
    document1 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    document2 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    document3 = models.FileField(
        upload_to="operators/documents/", null=True, blank=True)
    creator_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.SET_NULL, null=True, blank=True, related_name="branchcustomers")
    connected_logistics = models.ManyToManyField(LogisticsBranch, blank=True)

    def __str__(self):
        return self.full_name


class LogisticsCustomerContract(TimeStamp):
    first_party = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT)
    second_party = models.ForeignKey(
        LogisticsCustomer, on_delete=models.RESTRICT)
    expires_at = models.DateField(null=True, blank=True)
    is_signed = models.BooleanField(default=False)
    signed_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.first_party.logistics_company.company_name + " - " + self.second_party.full_name


class LogisticCustomerZonalContract(TimeStamp):
    logistic_customer_contract = models.ForeignKey(LogisticsCustomerContract, on_delete=models.RESTRICT)
    service_provider = models.ForeignKey(LogisticsBranch, on_delete=models.RESTRICT)
    shipping_zone = models.ForeignKey(LogisticsBranchShippingZone, on_delete=models.RESTRICT)
    shipping_charge = models.IntegerField(default=0)
    cod_handling_charge_pct = models.IntegerField(default=0)
    rejection_handling_charge_pct = models.IntegerField(default=0)
    is_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.shipping_zone.shipping_zone



class SignInAttempt(TimeStamp):
    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True)
    email = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    attempt_status = models.CharField(max_length=20, choices=ATTEMPT_STATUS)
    client_ip = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.email + " - " + self.attempt_status + "(" + str(self.created_at) + ")"

