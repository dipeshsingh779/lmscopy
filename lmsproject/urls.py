from django.conf.urls.static import static
from django.urls import path, include
from django.contrib import admin
from django.conf import settings
from lcapiapp.views import *

urlpatterns = [
    path('django-admin/', admin.site.urls),
    path('', include("umapp.urls")),
    path('', include("lmsapp.urls")),

    path('api/v1/customer/', include("lcapiapp.urls")),
    path('api/v1/logistics/', include("lbapiapp.urls")),

    # path('password-reset/check/token/<uidb64>/<token>/',
    #      PasswordTokenCheckAPI.as_view(), name='token-check'),
]


urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)