from umapp.models import LogisticsCustomerContract
from django import forms
from .models import *
import datetime

class ShipmentCreateForm(forms.ModelForm):
    class Meta:
        model = Shipment
        fields = ['weight', 'weight_unit', 'length', 'length_unit', 'breadth', 
        'height', 'cod_info', 'cod_value', 'sender_name', 'sender_address', 'sender_city', 'sender_contact_number',
        'sender_email', 'request_pickup', 'receiver_name', 'receiver_city', 'receiver_address',
        'receiver_contact_number', 'request_home_delivery', 'delivery_type', 'parcel_type', 'pickup_charge', 'dropoff_charge', 'shipping_charge', 'cod_handling_charge', 'rejection_handling_charge', 'extra_charge' ]

        widgets = {
            'weight':forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder': 'Parcel Weight',
                'style': 'width:45%; display: inline-block;'
            }),

            'weight_unit':forms.Select(attrs={
                'class':'form-control',
                'style': 'width:50%; display: inline-block;'
            }),

            'length':forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder': 'Parcel Length',
                'style': 'width:20%; display:inline-block',
            }),

            'length_unit':forms.Select(attrs={
                'class':'form-control',
                'style': 'width: 25%; display:inline-block',
            }),

            'breadth':forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder': 'Parcel Breadth',
                'style': 'width:20%; display:inline-block',
            }),

            'breadth_unit':forms.Select(attrs={
                'class':'form-control',
                'style': 'width:13.3%; display:inline-block',
            }),

            'height':forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder': 'Parcel Height',
                'style': 'width:20%; display:inline-block',
            }),

            'height_unit':forms.Select(attrs={
                'class':'form-control',
                'style': 'width:13.3%; display:inline-block',
            }),

            'parcel_type':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Parcel Type',
            }),

            'cod_info':forms.Select(attrs={
                'class':'form-control',
            }),

            'cod_value':forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder':'Enter Shipment Value',
                'style': 'width:100%;',
            }),

            'parcel_group_id':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Enter Parcel Group Id',
            }),


            'customer':forms.Select(attrs={
                'class':'form-control',
            }),

            'sender_name':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Sender Name'
            }),

            'sender_city':forms.Select(attrs={
                'class':'form-control',
            }),

            'sender_address':forms.TextInput(attrs={
                'id':'sender',
                'class':'form-control',
                'placeholder':'Sender Address'
            }),

            'sender_contact_number':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Enter Sender Contact Number'
            }),

            'sender_email':forms.EmailInput(attrs={
                'class':'form-control',
                'placeholder':'Enter Email'
            }),

            'request_pickup':forms.CheckboxInput(attrs={
                'class': 'custom-control-input'
            }),

            'pickup_charge':forms.NumberInput(attrs={
                'class':'form-control',
                'style': 'width:100%;',
            }),


            'receiver_name':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Enter Receiver Name'
            }),

            'receiver_city':forms.Select(attrs={
                'class':'form-control',
            }),


            'receiver_address':forms.TextInput(attrs={
                'id':'reciver',
                'class':'form-control',
                'placeholder':'Enter Receiver Address'
            }),


            'receiver_contact_number':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Enter Sender Contact Number'
            }),


            'request_home_delivery':forms.CheckboxInput(attrs={
                'class': 'custom-control-input',
            }),


            'dropoff_charge':forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder':'Enter Dropoff Charge',
                'style': 'width:100%;',
            }),


            'delivery_type':forms.Select(attrs={
                'class':'form-control',
            }),


            'shipping_charge':forms.NumberInput(attrs={
                'class':'form-control',
                'style': 'width:100%;',
            }),


            'cod_handling_charge':forms.NumberInput(attrs={
                'class':'form-control',
                'style': 'width:100%;',
            }),

            'rejection_handling_charge':forms.NumberInput(attrs={
                'class':'form-control',
                'style': 'width:100%;',
            }),

            'extra_charge':forms.NumberInput(attrs={
                'class':'form-control',
                'style': 'width:100%;',
            }),

            'service_payment_status':forms.CheckboxInput(attrs={
            }),

            'service_payment_amount':forms.NumberInput(attrs={
                'class':'form-control',
            }),


            'shipment_referred_by':forms.Select(attrs={
                'class':'form-control',
            }),

            'shipment_referral_charge':forms.NumberInput(attrs={
                'class':'form-control',
            }),
        }


class CustomerShipmentCreateForm(forms.ModelForm):
    branch_id = forms.CharField(widget = forms.HiddenInput(), required = False)
    city_id = forms.CharField(widget = forms.HiddenInput(), required = False)
    prices = forms.CharField(widget = forms.HiddenInput(), required = False)

    def clean(self):
        try:
            branch_id = self.cleaned_data['branch_id']
            city_id = self.cleaned_data['city_id']
        except:
            raise forms.ValidationError("Please select cityyyyyyyyyyyyyy")
        


    class Meta:
        model = Shipment
        fields = ['parcel_type', 'weight', 'weight_unit', 'length', 'length_unit', 'breadth', 
        'height', 'cod_info', 'cod_value', 'request_pickup', 'receiver_name', 'receiver_city', 'receiver_address',
        'receiver_contact_number', 'request_home_delivery','branch_id', 'city_id', 'prices']

        widgets = {

            'weight':forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder': 'Parcel Weight',
                'style': 'width: 40%;height:37px; display:inline-block',
                'value': 200
            }),

            'weight_unit':forms.Select(attrs={
                'class':'form-control',
                'style': 'width: 40%; height:37px; display:inline-block',
                'default':'Gram'
            }),

            'length':forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder': 'Parcel Length',
                'style': 'width: 40%;height:37px; display:inline-block',
                'value': 20
            }),

            'length_unit':forms.Select(attrs={
                'class':'form-control',
                'placeholder': 'Select Unit',
                'style': 'width: 40%;height:37px; display:inline-block',
                'default': "Cm"
            }),

            'breadth':forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder': 'Parcel Breadth',
                'style': 'width: 40%;height:37px; display:inline-block',
                'value': 20
            }),

            'height':forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder': 'Parcel Height',
               'style': 'width: 40%; height:37px;display:inline-block',
                'value': 20
            }),

            'parcel_type':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'eg: clothings, gadgets',
                'style': 'width: 40%;height:37px; display:inline-block',
            }),

            'cod_info':forms.Select(attrs={
                'class':'form-control',
                'placeholder': 'Select Cod Info',
                'style': 'width: 40%;height:37px; display:inline-block',
            }),

            'cod_value':forms.NumberInput(attrs={
                'class':'form-control',
                'placeholder':'Enter Shipment Value',
                'style': 'width: 40%;height:37px; display:inline-block',
            }),

            'request_pickup':forms.CheckboxInput(attrs={
                'class': 'custom-control-input',
            }),

            'receiver_name':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Enter Receiver Name',
                'style': 'width: 100%;height:37px; display:inline-block',
            }),

            'receiver_city':forms.Select(attrs={
                'class':'form-control',
                'placeholder': 'Select City',
                'style': 'width: 100%;height:100%; display:inline-block',
            }),


            'receiver_address':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Enter Receiver Address',
                'style': 'width: 100%;height:37px; display:inline-block',
            }),


            'receiver_contact_number':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Enter Sender Contact Number',
                'style': 'width: 100%;height:37px; display:inline-block',
            }),


            'request_home_delivery':forms.CheckboxInput(attrs={
                'class': 'custom-control-input',
            }),
            'delivery_type':forms.Select(attrs={
                'class':'form-control',
                'placeholder': 'Select delivery type',
                'style': 'width: 40%;height:37px;',
            }),
           

        }
    def __init__(self,user=None, *args, **kwargs):
        super(CustomerShipmentCreateForm,self).__init__(*args, **kwargs)
        self.fields['weight_unit'].initial = 'Gram'
        # self.fields['weight'].initial = 200
        self.fields['length_unit'].initial = 'Cm'

    #     self.fields['parcel_origin'].queryset = LogisticsBranch.objects.filter(branch_coverage = user.city)
        # self.fields['shipping_zone'].queryset = LogisticsBranchShippingZone.objects.none()
        # self.fields['delivery_type'].queryset = ShippingZoneDeliveryType.objects.none()
        # if 'receiver_city' in self.data:
        #     try:
        #         city_id = int(self.data.get('receiver_city'))
        #         city = City.objects.get(id=city_id)
        #         self.fields['shipping_zone'].queryset = LogisticsBranchShippingZone.objects.filter(cities=city)
        #         if 'delivery_type' in self.data:
        #             try:
        #                 if int(self.data.get('shipping_zone')) is not None:
        #                     shipping_zone_id = int(self.data.get('shipping_zone'))
        #                     shipping_zone_obj = LogisticsBranchShippingZone.objects.get(id=shipping_zone_id)
        #                     self.fields['delivery_type'].queryset = ShippingZoneDeliveryType.objects.filter(shippng_zone=shipping_zone_obj)
        #                 else:
        #                     print("error")
        #             except (ValueError, TypeError):
        #                 pass
        #         else:
        #             self.fields['delivery_type'].queryset = ShippingZoneDeliveryType.objects.none()
        #     except (ValueError, TypeError):
        #         pass
        # else:
        #     self.fields['shipping_zone'].queryset = LogisticsBranchShippingZone.objects.none()

        
        
    

    # def __init__(self, *args, **kwargs):
    #     super(CustomerShipmentCreateForm, self).__init__(*args, **kwargs)
    #     self.fields['branch_id'].widget.attrs['readonly'] = True
    # def __init__(self, *args, **kwargs):
    #     from django.forms.widgets import HiddenInput
        

#  class CustomerBulkShipmentUploadForm(forms.Form):
#      file = forms.FileField(widget=forms.FileInput(attrs={
#          'class':'form-control'
#          'name':'file',
#          'id':'file-selector',
#          'accept': '.csv'
#      }))   


class CustomerBulkShipmentUploadForm(forms.Form):
    file = forms.FileField(widget=forms.FileInput(attrs={
        'class': 'form-control',
        'name': 'file',
        'id': 'file-selector',
        'accept': '.csv'
    }))



class StaffForm(forms.Form):

    rider = forms.ModelChoiceField(queryset=DeliveryPerson.objects.all(), widget=forms.Select(attrs={
        'class': 'form-control',
    }))

    partner_company = forms.ModelChoiceField(queryset=BranchOperator.objects.all(), widget=forms.Select(attrs={
        'class': 'form-control',
    }))
    
    def __init__(self, branch_id=None, *args, **kwargs):
        super(StaffForm, self).__init__(*args, **kwargs)
        self.fields['rider'].queryset = DeliveryPerson.objects.filter(logistics_branch__id=branch_id)
        self.fields['partner_company'].queryset = LogisticsBranch.objects.exclude(id=branch_id)

class BundleCreateForm(forms.ModelForm):
    class Meta:
        model = ShipmentBundle
        fields = '__all__'

        widgets = {
            # 'weight':forms.NumberInput(attrs={
            #     'class':'form-control',
            #     'placeholder': 'Parcel Weight',
            #     'style': 'width:45%; display: inline-block;'
            # }),

            # 'weight_unit':forms.Select(attrs={
            #     'class':'form-control',
            #     'style': 'width:50%; display: inline-block;'
            # }),

            # 'length':forms.NumberInput(attrs={
            #     'class':'form-control',
            #     'placeholder': 'Parcel Length',
            #     'style': 'width:20%; display:inline-block',
            # }),
        }


class CustomerTicketRaiseForm(forms.ModelForm):
    class Meta:
        model = CustomerSupportTicket
        fields = ('issue_detail', 'related_file', 'raised_to')
        widgets = {
            'issue_detail':forms.Textarea(attrs={
                'class':'form-control',
                'rows':"5"
            }),
            'related_file':forms.FileInput(attrs={
                'class':'form-control'
            }),
            'raised_to':forms.Select(attrs={
                'class':'form-control'
            })
        }

    def __init__(self,user=None, *args, **kwargs):
        super(CustomerTicketRaiseForm,self).__init__(*args, **kwargs)
        customer = LogisticsCustomer.objects.get(id=user.id)
        branches = customer.connected_logistics.all()
        self.fields['raised_to'].queryset = branches


class CustomerSupportTicketUpdateForm(forms.ModelForm):
    class Meta:
        model = CustomerSupportTicket
        fields = ['ticket_status']
        widgets = {
            'ticket_status':forms.Select(attrs={
                'class':'form-control'
            })
        }


class CustomerTicketFollowupForm(forms.ModelForm):
    class Meta:
        model = CustomerTicketFollowup
        fields = ['followup_message']
        widgets = {
            'followup_message':forms.Textarea(attrs={
                'class':'form-control',
                'cols':30,
                'rows':1,
                'style': 'height: 7em;border:None',
                'placeholder':'write your query...'
            })
        }

class ShipmentActivityForm(forms.ModelForm):
    class Meta:
        model = ShipmentActivity
        fields = ['shipment', 'rider', 'shipment_status', 'activity', 'activity_by', 'involved_logistics']

        widgets = {
            'shipment':forms.Select(attrs={
                'class':'form-control',
            }),

            'rider':forms.Select(attrs={
                'class':'form-control',
            }),

            'shipment_status':forms.Select(attrs={
                'class':'form-control-sm',
            }),

            'activity':forms.TextInput(attrs={
                'class':'form-control',
            }),

            'activity_by':forms.Select(attrs={
                'class':'form-control',
            }),

            'involved_logistics':forms.Select(attrs={
                'class':'form-control',
            }),

        }


class LogisticsNoticeForm(forms.ModelForm):
    class Meta:
        model = LogisticsNotice
        fields = ['notice', 'excluded_customers']
        widgets = {
            'notice':forms.Textarea(attrs={
                'class':'form-control',
                'rows':5,
                'col':10
            }),
            'excluded_customers':forms.SelectMultiple()
        }
    
    def __init__(self, branch_id=None, *args, **kwargs):
        super(LogisticsNoticeForm, self).__init__(*args, **kwargs)
        self.fields['excluded_customers'].queryset = LogisticsCustomer.objects.none()
        if 'excluded_customers' in self.data:
            self.fields['excluded_customers'].queryset = LogisticsCustomer.objects.all()
        elif self.instance.pk:
            self.fields['excluded_customers'].queryset = LogisticsCustomer.objects.all().filter(pk__in=self.instance.excluded_customers.all().values('pk'))


class LogisticsCustomerContractForm(forms.ModelForm):
    class Meta:
        model = LogisticsCustomerContract
        fields = ['second_party']
        widgets = {
            'second_party':forms.Select(attrs={
                'class':'form-control col-md-6',
            })
        }
    def __init__(self, comp=None, *args, **kwargs):
        super(LogisticsCustomerContractForm,self).__init__(*args, **kwargs)
        self.fields['second_party'].disabled=True


class CustomerPaymentRequestForm(forms.ModelForm):
    class Meta:
        model = CustomerPaymentRequest
        fields = ['logistic_branch', 'message']
        widgets = {
            'logistic_branch' : forms.Select(attrs={
                'class' : 'form-control'
            }),
            'message' : forms.Textarea(attrs={
                'class' : 'form-control'
            })
        }
    
    def __init__(self, user=None, *args, **kwargs):
        super(CustomerPaymentRequestForm, self).__init__(*args, **kwargs)
        self.fields['logistic_branch'].queryset = user.connected_logistics


class CustomerPaymentForm(forms.ModelForm):
    class Meta:
        model = CustomerPayment
        fields = ['shipments', 'discount_or_extra', 'total_service_charge', 'total_cod_value', 'net_paid_or_received', 'payment_method', \
            'remarks', 'slip']
        widgets = {
            'shipments' : forms.SelectMultiple(attrs={
                'class' : 'form-control'
            }),
            'discount_or_extra' : forms.NumberInput(attrs={
                'class' : 'form-control'
            }),
            'total_service_charge' : forms.NumberInput(attrs={
                'class' : 'form-control'
            }),
            'total_cod_value' : forms.NumberInput(attrs={
                'class' : 'form-control'
            }),
            'net_paid_or_received' : forms.NumberInput(attrs={
                'class' : 'form-control'
            }),
            'payment_method' : forms.NumberInput(attrs={
                'class' : 'form-control'
            }),
            'remarks' : forms.TextInput(attrs={
                'class' : 'form-control'
            }),
            'slip' : forms.FileInput(attrs={
                'class' : 'form-control'
            })
        }
    
    def __init__(self, shipss=None, *args, **kwargs):
        super(CustomerPaymentForm, self).__init__(*args, **kwargs)
        # self.fields['shipments'].queryset = shipss

class CustomerProfileUpdate(forms.ModelForm):
    class Meta:
        model = LogisticsCustomer
        fields = ["customer_type",'full_name', 'contact_number',"alternative_contact_number",
                  'company_name', 'address', 'city',"document1","document2","document3" ]
        widgets = {
            "full_name": forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter your fullname...'
            }),
            "customer_type": forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'enter your fullname...'
            }),
            "company_name": forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter your fullname...'
            }),
            "contact_number": forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter your mobile number'
            }),
            "alternative_contact_number": forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter your alt mobile number if any..'
            }),
            "city": forms.Select(attrs={
                'class': 'form-control',
            }),
            "address": forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter your address...'
            }),
            "document1": forms.FileInput(attrs={
                'class': 'form-control',
            }),
            "document2": forms.FileInput(attrs={
                'class': 'form-control',
            }),
            "document3": forms.FileInput(attrs={
                'class': 'form-control',
            }),
        }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs["style"] = "height:100%"
