from django.shortcuts import render, HttpResponse, redirect
from django.views import View
from django.views.generic import *
from .models import Shipment
from .forms import *
from django.urls import reverse, reverse_lazy
from django.forms import formset_factory
from django.views.generic import *
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.conf import settings
from umapp.models import *
from umapp.views import *
from django.http import JsonResponse
from django.utils.text import slugify
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import send_mail



class ClientMixin(SuccessMessageMixin, object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class OperatorRequiredMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
    

class ClientHomeView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clienthome.html"

class LogisticsAdminHomeView(OperatorRequiredMixin, TemplateView):
    template_name = "logisticstemplates/logisticsadminhome.html"

class LogisticsAdminShipmentListView(OperatorRequiredMixin, TemplateView):
    template_name = "logisticstemplates/logisticsadminshipmentlist.html"

class LogisticsAdminShipmentCreateView(OperatorRequiredMixin, TemplateView):
    template_name = "logisticstemplates/logisticsadminshipmentcreate.html"


class CustomerHome(TemplateView):
    template_name = 'customertemplates/customerhome.html'


class ShipmentView(CreateView):
    def get(self,request):
        form = ShipmentForm
        context = {
             'form':form,
         }
        return render(request, 'customertemplates/shipment.html', context)

    def post(self, request, *args, **kwargs):
        form = ShipmentForm(request.POST)
        print(form)
        if form.is_valid():
            form.instance.sender_name = request.user.logisticscustomer.full_name
            form.instance.sender_city = request.user.logisticscustomer.city
            form.instance.sender_address = request.user.logisticscustomer.address
            form.instance.sender_contact_number = request.user.logisticscustomer.contact_number
            form.instance.sender_email = request.user.email
            form.save()
            print('successfully saved !!!!')
        else:
            print ('Failed !!!!!!')
        return redirect('/shipment/?success')
    



class CustomerDashboardView(View):
    template_name = "clienttemplates/clienthome.html"
    def get(self, request):
        return render(request, 'customertemplates/customer-dashboard.html')

class ClientContactView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clientcontact.html"


#need to modify
class CustomerMixin(SuccessMessageMixin):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            user_id = request.session.get('user_id', None)
            print(user_id)
            if user_id:
                pass
            else:
                return redirect('umapp:customerlogin')
                

        else:
            return redirect('umapp:customerlogin')

        return super().dispatch(request, *args, **kwargs)



        
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class CustomerHomeView(CustomerMixin, TemplateView):
    template_name = "customertemplates/customerhome.html"


class CustomerSignupView(CreateView):
    template_name = "customertemplates/customersignup.html"
    form_class = CustomerSignupForm
    success_url = reverse_lazy("lmsapp:customerhome")

    def form_valid(self, form):
        usr = form.cleaned_data["email"]
        pword = form.cleaned_data["password"]
        print(usr, pword)
        user=User.objects.create_user(usr, usr, pword)
        form.instance.user = user
        return super().form_valid(form)


class LogisticCompanySignupView(FormView):
    template_name = "lctemplates/logisticsignup.html"
    form_class = LogisticsSignupForm
    success_url = reverse_lazy("lmsapp:clienthome")
    # success_message = "succesfull"

    def form_valid(self, form):
        # for logistics company 
        lc = form.save()
        lc.status = "Pending"
        lc.account_type = LogisticsAccountType.objects.first()
        lc.company_slug = slugify(lc.company_name)
        lc.save()
        # for logistics main branch
        branch_contact = form.cleaned_data.get("branch_contact")
        branch_alternative_contact = form.cleaned_data.get("branch_alternative_contact")
        branch_email = form.cleaned_data.get("branch_email")
        branch_address = form.cleaned_data.get("branch_address")
        branch_coverage = form.cleaned_data.get("branch_coverage")

        lc_branch = LogisticsBranch.objects.create(status="Pending", logistics_company=lc, branch_city=lc.company_city, branch_contact=branch_contact,branch_alternative_contact=branch_alternative_contact, branch_email=branch_email, branch_address=branch_address,is_main_branch=True)
        for i in branch_coverage:
            lc_branch.branch_coverage.add(i)

        
        # lc_branch.branch_coverage.add(branch_coverage)
        # for super admin operator 
        operator_email = form.cleaned_data.get("operator_email")
        operator_password = form.cleaned_data.get("operator_password")
        full_name = form.cleaned_data.get("operator_full_name")
        user = User.objects.create_user(operator_email, operator_email, operator_password)
        operator = BranchOperator.objects.create(status="Pending", logistics_branch=lc_branch, operator_type="SuperAdmin", user=user, full_name=full_name)

        # messages.success(self.request, 'Form submission successful')
        # print(x)
        # return redirect('lmsapp:clienthome')
        messages.success(self.request, 'Form submission successful')
        
        send_mail(
            'Signup Succesful',
            'Hi! uYo have successfully signed up in lms system. We will get back to you soon.',
            # 'sarobarcomp@gmail.com',
            settings.EMAIL_HOST_USER, 
            [operator_email],
            fail_silently=False,
            )
        
        # send_mail(
        #     'New registration',
        #     'New user has been registered. Please verify',
        #     # 'sarobarcomp@gmail.com',
        #     settings.EMAIL_HOST_USER, 
        #     ['alishprajapati33@gmail.com'],
        #     fail_silently=False,
        #     )
        return redirect('lmsapp:logcompanyhome')
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)


def validateoperatoremailview(request):
    email=request.GET.get("email")
    user_obj=User.objects.filter(username=email).last()
    if user_obj:
        return JsonResponse({'errormsg':'email already exists'})
    else:
        return JsonResponse({'success':'email available'})
class LogisticCompanyLoginView(FormView):
    template_name = "lctemplates/logisticsadminlogin.html"
    form_class = LogisticsLoginForm
    success_url =reverse_lazy("lmsapp:logcompanyhome")
    # success_message = "succesfull"


    def form_valid(self, form):
        #print("form valid +++++++++++++")
        a = form.cleaned_data["email"]
        b = form.cleaned_data["password"]
        try:
            user=User.objects.get(username=a)
            admin = user.branchoperator
            usr = authenticate(username=a, password=b)
            if usr is not None:
                login(self.request, usr)
                messages.success(self.request, 'You are logged in ')
                return redirect('lmsapp:logcompanyhome')
            else:
                return render(self.request, self.template_name,
                            {"error": "Invalid username or password",
                            "form": form})

        except:
            print("invalid")
            return redirect("lmsapp:logisticsadminlogin")

        

        return super().form_valid(form)
    
    def form_invalid(self, form):
        print("Invalid")
        return super().form_invalid(form) 


class LogisticCompanyLogoutView(CustomerMixin,View):

    def get(self, request):
        logout(request)
        messages.warning(self.request, 'You are logged out')
            # return redirect('lmsapp:logcompanyhome')

        return redirect("/")


class LogisticCompanyResetPasswordView(FormView):
    template_name = "lctemplates/logcompanypasswordreset.html"
    form_class = LogisticsResetPasswordForm
    success_url = reverse_lazy("lmsapp:logisticsadminlogin")

    def form_valid(self, form):
        email = form.cleaned_data.get("email")
        if email is not None:
            return render(self.request, 'logisticstemplates/logisticsadminlogin.html', {"form": LogisticsAdminLoginForm, "message": "Reset link has been sent to your email."})
        else:
            return render(self.request, self.template_name, {"form": form, "error": "This email doesn't exist."})
        

        send_mail(
            'Signup Succesful',
            'This is your new password',
            # 'sarobarcomp@gmail.com',
            settings.EMAIL_HOST_USER, 
            [operator_email],
            fail_silently=False,
            )

        return super().form_valid(form)


class LogisticCompanyHomeView(OperatorRequiredMixin, TemplateView):
    template_name = "logisticstemplates/settings/operatorhome.html"



class CustomerLoginView(FormView):
    template_name = "customertemplates/customerlogin.html"
    form_class = CustomerLoginForm
    success_url =reverse_lazy("lmsapp:customerhome")

    def form_valid(self, form):
        a = form.cleaned_data["email"]
        b = form.cleaned_data["password"]

        try:
            user=User.objects.get(username=a)
            admin =user.logisticscustomer
            print("ghghg")
            usr = authenticate(username=a, password=b)
            if usr is not None:
                # login(self.request, usr)
                login(self.request, usr)
                self.request.session['user_id']=user.id

            else:
                return render(self.request, self.template_name, {"error":"Invalid username or password",
                "form": form})
        except:
            print("invaliddddd")
            return redirect("umapp:customerlogin")
        
        return super().form_valid(form)
    
    def form_invalid(self, form):
        print("Invalid")
        return super().form_invalid(form)


class CustomerLogoutView(View):

    def get(self, request):
        logout(request)

        return redirect("/")


class CustomerUsernameCheckerView(View):  

    def get(self, request):
        user_input = request.GET["usr"]
        # print(user_input, '\n +++++++')
        if User.objects.filter(username=user_input).exists():
            message = "yes"
        else:
            message = "no"
        return JsonResponse({'response':message})


class ShipmentListView(View):
    def get(self, request):
        data = Shipment.objects.all()
        context = {
            'data':data
        }
        return render(request, 'customertemplates/shipment-list.html', context)




    
