from django.contrib import admin
from .models import *


admin.site.register([
    LogisticBranchContract, LogisticBranchZonalContract, Shipment, ShipmentActivity, ShipmentRemark, ShipmentBundle, DeliveryTask,
    CustomerPaymentRequest, CustomerPayment, LogisticsPayment, CustomerSupportTicket, LogisticsSupportTicket, LogisticsMessage, LogisticsNotice, CustomerTicketFollowup, BranchStickyNote, CustomerStickyNote, ShipmentBundleShipment])
