from rest_framework import serializers
from .models import LogisticsCustomer

class LogisticCustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsCustomer
        fields = "__all__"