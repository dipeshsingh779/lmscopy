from umapp.models import (City, DeliveryPerson, ShippingZoneDeliveryType, LogisticsBranch,
                          BranchOperator, LogisticsBranchShippingZone, LogisticsCustomer, TimeStamp, LogisticsCompany)
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from umapp.constants import *
from django.db import models


# logistics branch bonding for pickup and dropoff # always pick the last object
class LogisticBranchContract(TimeStamp):
    first_party = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="contractedfirstparties")
    second_party = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="contractedsecondparties")
    first_party_dropoff_charge = models.PositiveIntegerField()
    second_party_dropoff_charge = models.PositiveIntegerField()
    first_party_referral_percentage = models.PositiveIntegerField(default=0)
    second_party_referral_percentage = models.PositiveIntegerField(default=0)
    expires_at = models.DateField(null=True, blank=True)
    second_party_signed = models.BooleanField(default=False)
    second_party_signed_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.first_party.logistics_company.company_name + " - " + self.second_party.logistics_company.company_name


class LogisticBranchZonalContract(TimeStamp):
    logistic_branch_contract = models.ForeignKey(
        LogisticBranchContract, on_delete=models.RESTRICT)
    service_provider = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT)
    shipping_zone = models.ForeignKey(
        LogisticsBranchShippingZone, on_delete=models.RESTRICT)
    shipping_charge = models.IntegerField(default=0)
    cod_handling_charge_pct = models.IntegerField(default=0)
    rejection_handling_charge_pct = models.IntegerField(default=0)
    is_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.shipping_zone.shipping_zone


class Shipment(TimeStamp):
    # general info
    parcel_origin = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT)
    tracking_code = models.CharField(max_length=50)
    weight = models.DecimalField(max_digits=19, decimal_places=2)
    weight_unit = models.CharField(max_length=100, choices=WEIGHT_UNIT,)
    length = models.DecimalField(max_digits=19, decimal_places=2)
    length_unit = models.CharField(max_length=100, choices=LENGTH_UNIT)
    breadth = models.DecimalField(max_digits=19, decimal_places=2)
    breadth_unit = models.CharField(max_length=100, choices=LENGTH_UNIT)
    height = models.DecimalField(max_digits=19, decimal_places=2)
    height_unit = models.CharField(max_length=100, choices=LENGTH_UNIT)
    parcel_type = models.CharField(
        max_length=200, null=True, blank=True)  # parcel material type
    cod_info = models.CharField(max_length=200, choices=COD_INFO)
    cod_value = models.IntegerField(
        default=0, null=True, blank=True)  # dependent to cod_info
    parcel_group_id = models.CharField(max_length=200, null=True, blank=True)
    # sender
    customer = models.ForeignKey(
        LogisticsCustomer, on_delete=models.RESTRICT, null=True, blank=True)
    sender_name = models.CharField(max_length=200)
    sender_city = models.ForeignKey(
        City, on_delete=models.RESTRICT, related_name="sentshipments")
    sender_address = models.CharField(max_length=200)
    sender_contact_number = models.CharField(max_length=10)
    sender_email = models.EmailField(null=True, blank=True)
    request_pickup = models.BooleanField(default=True)
    pickup_charge = models.IntegerField(default=0)
    # receiver
    receiver_name = models.CharField(max_length=200)
    receiver_city = models.ForeignKey(
        City, on_delete=models.RESTRICT, related_name="receivedshipments")
    receiver_address = models.CharField(max_length=200)
    receiver_contact_number = models.CharField(max_length=10)
    request_home_delivery = models.BooleanField(default=True)
    dropoff_charge = models.IntegerField(default=0)

    # charges
    delivery_type = models.ForeignKey(ShippingZoneDeliveryType, on_delete=models.RESTRICT, null=True, blank=True)
    shipping_charge = models.IntegerField(default=0)
    cod_handling_charge = models.IntegerField(default=0)
    rejection_handling_charge = models.IntegerField(default=0)
    extra_charge = models.IntegerField(default=0)

    # payment information
    # prepayment for individual # may be post payment for business
    service_payment_status = models.BooleanField(default=False)
    service_payment_amount = models.IntegerField(null=True, blank=True)

    # in case of referred shipments
    shipment_referred_by = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, null=True, blank=True, related_name="referredshipments")
    shipment_referral_charge = models.PositiveBigIntegerField(
        default=0, null=True, blank=True)

    def __str__(self):
        return self.tracking_code

    @property
    def total_charge(self):
        shipping_charge, cod_handling_charge, rejection_handling_charge, extra_charge = 0, 0, 0, 0
        shipping_charge = self.shipping_charge if self.shipping_charge else 0
        cod_handling_charge = self.cod_handling_charge if self.cod_info == "Cash on Delivery" else 0
        rejection_handling_charge = self.rejection_handling_charge if self.shipmentactivity_set.last().shipment_status == "Shipment Returned to the Sender" else 0
        extra_charge = self.extra_charge if self.extra_charge else 0
        total_charge = shipping_charge + cod_handling_charge + rejection_handling_charge + extra_charge
        return total_charge

    @property
    def lshipmentstatus(self):
        status = self.shipmentactivity_set.last().shipment_status if self.shipmentactivity_set.all().exists() else None
        return status


class ShipmentActivity(TimeStamp):
    shipment = models.ForeignKey(Shipment, on_delete=models.CASCADE)
    rider = models.ForeignKey(
        DeliveryPerson, on_delete=models.RESTRICT, null=True, blank=True)
    shipment_status = models.CharField(max_length=200, choices=SHIPMENT_STATUS)
    activity = models.CharField(max_length=500, null=True, blank=True)
    activity_by = models.ForeignKey(
        BranchOperator, on_delete=models.RESTRICT, related_name="operatoractivities", null=True, blank=True)
    involved_logistics = models.ForeignKey(
        LogisticsBranch, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.shipment_status


class ShipmentBundle(TimeStamp):
    bundle_code = models.CharField(max_length=100, null=True, blank=True)
    source_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT)
    destination_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, null=True, blank=True, related_name="destinationbundles")
    is_reverse_logistics = models.BooleanField(
        default=False, null=True, blank=True)
    delivery_person = models.ForeignKey(
        DeliveryPerson, on_delete=models.SET_NULL, null=True, blank=True)
    is_sent = models.BooleanField(default=False)
    sent_date = models.DateTimeField(null=True, blank=True)
    is_delivered = models.BooleanField(default=False)
    delivered_date = models.DateTimeField(null=True, blank=True)
    is_confirmed = models.BooleanField(default=False)
    confirmed_date = models.DateTimeField(null=True, blank=True)
    related_file = models.FileField(
        upload_to="shipments/bundles/", null=True, blank=True)
    transit_code = models.CharField(max_length=100, null=True, blank=True)
    transit_charge = models.IntegerField(default=0)

    remarks = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.id)


class ShipmentBundleShipment(TimeStamp):
    shipment_bundle = models.ForeignKey(
        ShipmentBundle, on_delete=models.RESTRICT)
    shipment = models.ForeignKey(Shipment, on_delete=models.RESTRICT)
    shipment_status = models.CharField(
        max_length=200, choices=SHIPMENT_STATUS, null=True, blank=True)
    shipment_passed_by = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="passed_by_shipments")
    shipment_passed_to = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="passed_to_shipments", null=True, blank=True)
    # charge information
    shipping_charge = models.IntegerField(default=0, null=True, blank=True)
    cod_handling_charge = models.IntegerField(default=0, null=True, blank=True)
    rejection_handling_charge = models.IntegerField(
        default=0, null=True, blank=True)
    extra_charge = models.IntegerField(default=0)
    # payment information
    service_payment_status = models.BooleanField(default=False)
    service_payment_amount = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.shipment.tracking_code


class ShipmentBundleActivity(TimeStamp):
    shipment_bundle = models.ForeignKey(
        ShipmentBundle, on_delete=models.CASCADE)
    activity = models.CharField(max_length=512)
    activity_by = models.ForeignKey(
        BranchOperator, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.shipment_bundle.bundle_code


class DeliveryTask(TimeStamp):
    shipment = models.ForeignKey(
        Shipment, on_delete=models.RESTRICT, null=True, blank=True)
    shipment_bundle = models.ForeignKey(
        ShipmentBundle, on_delete=models.RESTRICT, null=True, blank=True)
    delivery_person = models.ForeignKey(
        DeliveryPerson, on_delete=models.RESTRICT)
    task_type = models.CharField(max_length=50, choices=DP_TASK_TYPE)
    task_status = models.CharField(max_length=50, choices=DP_TASK_STATUS)
    task_details = models.TextField(null=True, blank=True)
    scheduled_date = models.DateTimeField()
    completed_date = models.DateTimeField(null=True, blank=True)
    task_remarks = models.TextField(null=True, blank=True)

    # for freelancers
    earned_point = models.IntegerField(default=0, null=True, blank=True)
    earned_amount = models.IntegerField(default=0, null=True, blank=True)
    is_earning_settled = models.BooleanField(default=False)

    def __str__(self):
        return self.delivery_person.full_name


class ShipmentRemark(TimeStamp):
    shipment = models.ForeignKey(Shipment, on_delete=models.CASCADE)
    remarks = models.TextField()
    is_private = models.BooleanField(default=False)

    delivery_task = models.ForeignKey(
        DeliveryPerson, on_delete=models.SET_NULL, null=True, blank=True)
    remarks_by_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, limit_choices_to=SHIPMENT_REMARKS_BY)
    remarks_by_id = models.PositiveIntegerField()
    remarks_by = GenericForeignKey('remarks_by_type', 'remarks_by_id')

    def __str__(self):
        return self.shipment.tracking_code + "(" + str(self.id) + ")"


class CustomerPaymentRequest(TimeStamp):
    logistic_custmer = models.ForeignKey(
        LogisticsCustomer, on_delete=models.CASCADE)
    logistic_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.CASCADE)
    message = models.TextField(null=True, blank=True)
    request_amount = models.BigIntegerField(null=True, blank=True)
    payout_queue = models.PositiveIntegerField(null=True, blank=True)
    queued_date = models.DateTimeField(null=True, blank=True)
    estimated_payout_date = models.DateField(null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)
    is_settled = models.BooleanField(default=False)

    def __str__(self):
        return self.message


class CustomerPayment(TimeStamp):
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT)
    logistics_customer = models.ForeignKey(
        LogisticsCustomer, on_delete=models.RESTRICT)
    shipments = models.ManyToManyField(Shipment)
    total_shipping_charge = models.BigIntegerField()
    total_cod_handling_charge = models.BigIntegerField(default=0)
    total_rejection_handling_charge = models.BigIntegerField(default=0)
    discount_or_extra = models.BigIntegerField(default=0)
    # algebric sum of the above 4 fields
    total_service_charge = models.BigIntegerField()
    # sum of cod amount of those shipments whose cod_info is Cash on Delivery
    total_cod_value = models.BigIntegerField(default=0)
    # algebric sum of the above two fields
    net_paid_or_received = models.BigIntegerField()
    payment_method = models.CharField(max_length=200, null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)
    payment_confirmed_by = models.ForeignKey(
        BranchOperator, on_delete=models.SET_NULL, null=True)
    slip = models.FileField(upload_to="c_payment_slips/", null=True, blank=True)

    def __str__(self):
        return "Payment: " + str(self.id)


class LogisticsPayment(TimeStamp):
    service_user_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT)
    service_provider_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.RESTRICT, related_name="dcbranchpayments")
    bundle_shipments = models.ManyToManyField(ShipmentBundleShipment)
    total_shipping_charge = models.BigIntegerField()
    total_cod_handling_charge = models.BigIntegerField(default=0)
    total_rejection_handling_charge = models.BigIntegerField(default=0)
    discount_or_extra = models.BigIntegerField(default=0)
    # algebric sum of the 4 fields above
    total_service_charge = models.BigIntegerField()
    # sum of cod amount of those shipments whose cod_info is Cash on Delivery
    total_cod_value = models.BigIntegerField(default=0)
    # algebric sum of the two fields above
    net_paid_or_received = models.BigIntegerField()
    payment_method = models.CharField(max_length=200, null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)
    payment_confirmed_by = models.ForeignKey(
        BranchOperator, on_delete=models.SET_NULL, null=True)
    slip = models.FileField(upload_to="c_payment_slips/", null=True, blank=True)

    def __str__(self):
        return "Payment: " + str(self.id)


class CustomerSupportTicket(TimeStamp):
    ticket_status = models.CharField(max_length=50, choices=TICKET_STATUS)
    raised_by = models.ForeignKey(LogisticsCustomer, on_delete=models.RESTRICT)
    raised_to = models.ForeignKey(LogisticsBranch, on_delete=models.RESTRICT)
    issue_detail = models.TextField()
    related_file = models.FileField(
        upload_to="issue_file", null=True, blank=True)

    def __str__(self):
        return self.issue_detail


class CustomerTicketFollowup(TimeStamp):
    ticket = models.ForeignKey(
        CustomerSupportTicket, on_delete=models.RESTRICT)
    followup_by_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, limit_choices_to=CTICKET_FOLLOWUP_BY)
    followup_by_id = models.PositiveIntegerField()
    followup_by = GenericForeignKey('followup_by_type', 'followup_by_id')
    followup_message = models.TextField()

    def __str__(self):
        return self.ticket.issue_detail


class LogisticsSupportTicket(TimeStamp):
    ticket_status = models.CharField(max_length=50, choices=TICKET_STATUS)
    raised_by = models.ForeignKey(LogisticsBranch, on_delete=models.RESTRICT)
    issue_detail = models.TextField()
    related_file = models.FileField(
        upload_to="issue_file", null=True, blank=True)
    created_by = models.ForeignKey(BranchOperator, on_delete=models.RESTRICT)

    def __str__(self):
        return self.issue_detail


class LogisticsTicketFollowup(TimeStamp):
    ticket = models.ForeignKey(
        CustomerSupportTicket, on_delete=models.RESTRICT)
    followup_by_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, limit_choices_to=LTICKET_FOLLOWUP_BY)
    followup_by_id = models.PositiveIntegerField()
    followup_by = GenericForeignKey('followup_by_type', 'followup_by_id')
    followup_message = models.TextField()

    def __str__(self):
        return self.ticket.issue_detail


class LogisticsMessage(TimeStamp):
    logistics_company = models.ForeignKey(
        LogisticsCompany, on_delete=models.CASCADE)
    sender = models.CharField(max_length=200)
    mobile = models.CharField(max_length=20)
    email = models.EmailField(null=True, blank=True)
    message = models.TextField()

    def __str__(self):
        return self.sender


class LogisticsNotice(TimeStamp):
    logistics_company = models.ForeignKey(
        LogisticsCompany, on_delete=models.CASCADE)
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.SET_NULL, null=True, blank=True)
    notice = models.TextField()
    excluded_customers = models.ManyToManyField(LogisticsCustomer, blank=True)
    created_by = models.ForeignKey(BranchOperator, on_delete=models.RESTRICT)

    def __str__(self):
        return str(self.created_by)


class BranchStickyNote(TimeStamp):
    logistics_branch = models.ForeignKey(
        LogisticsBranch, on_delete=models.CASCADE)
    operator = models.ForeignKey(
        BranchOperator, on_delete=models.SET_NULL, null=True, blank=True)
    note = models.TextField()

    def __str__(self):
        return self.note


class CustomerStickyNote(TimeStamp):
    customer = models.ForeignKey(
        LogisticsCustomer, on_delete=models.CASCADE)
    note = models.TextField()

    def __str__(self):
        return self.note
