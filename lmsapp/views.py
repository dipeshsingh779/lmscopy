from umapp.views import CustomerRequiredMixin, OperatorRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.contrib.auth.models import User
from django.db.models import Q, F, Max, Avg, Count, Sum
from django.core.mail import send_mail
from django.utils.text import slugify
from django.http import JsonResponse
from django.contrib import messages
from django.views.generic import *
from django.views import View
from django.utils import timezone
from django.conf import settings
from umapp.models import *
from umapp.forms import OperatorCustomersUpdateForm
from .models import *
from .forms import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import random
import json
import csv
import io
from rest_framework.generics import *
from rest_framework.views import APIView
from .serializers import LogisticCustomerSerializer
from rest_framework.response import Response
from rest_framework import status
from django.utils.dateparse import parse_date
from django.http import HttpResponse

# 1 System Operator Views


# 2 Logistics Operator Views


class LogisticsAdminSettingsView(OperatorRequiredMixin, TemplateView):
    template_name = "logisticstemplates/logisticsadminsettings.html"


class LogisticsAdminHomeView(OperatorRequiredMixin, TemplateView):
    template_name = "logisticstemplates/logisticsadminhome.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class LogisticsAdminShipmentListView(OperatorRequiredMixin, TemplateView):
    template_name = "logisticstemplates/logisticsadminshipmentlist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['staffform'] = StaffForm(branch_id=self.branch.id)
        return context


class CmpLogisticsAdminLoadShipmentListView(OperatorRequiredMixin, View):

    def get(self, request):
        status = self.request.GET.get('status')
        city = self.request.GET.get('cityId')
        keyword = self.request.GET.get('keyword')
        paginate_by = self.request.GET.get("paginate_by")
        try:
            paginate_by = int(paginate_by)
        except Exception:
            paginate_by = 25
        if status == 'staff-assigned':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=self.branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Rider Assigned for Pickup')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentliststaffassigned.html"
        elif status == 'staff-picked-up':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=self.branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Rider Picked up the Shipment')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentliststaffpickedup.html"
        elif status == 'at-warehouse':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(Q(shipmentactivity__shipment_status='Shipment Arrived at Warehouse') | Q(
                shipmentactivity__shipment_status='Shipment Returned to Logistics'), parcel_origin=self.branch, shipmentbundleshipment=None, shipmentactivity__id=F('latest'))
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            citylist = City.objects.filter(
                id__in=shipmentlist.values('receiver_city'))
            shipmentlist = shipmentlist.filter(
                receiver_city__id=city) if city is not None and city != "" else shipmentlist

            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'citylist': citylist,
                'city': city,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentlistatwarehouse.html"
        elif status == 'staff-assigned-for-delivery':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(parcel_origin=self.branch, shipmentactivity__id=F(
                'latest'), shipmentactivity__shipment_status='Rider Assigned for Dispatch to the Receiver')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentliststaffassignedreceiver.html"
        elif status == 'pickedup-for-delivery':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(parcel_origin=self.branch, shipmentactivity__id=F(
                'latest'), shipmentactivity__shipment_status='Rider Picked up the Shipment for Dispatch')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentlistpickedupfordelivery.html"
        elif status == 'shipmentdelivered':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=self.branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Shipment Delivered to the Receiver')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentlistdelivered.html"
        elif status == 'canceled':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=self.branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Shipment Canceled')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentlistcanceled.html"
        elif status == 'shipment-rejected':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=self.branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Shipment Rejected')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentlistrejected.html"
        elif status == 'ready-to-return':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=self.branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Ready to Return to Sender')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            citylist = City.objects.filter(
                id__in=shipmentlist.values('sender_city'))
            shipmentlist = shipmentlist.filter(
                receiver_city__id=city) if city is not None and city != "" else shipmentlist

            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'citylist': citylist,
                'city': city,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentlistreturnedtowarehouse.html"
        elif status == 'assign-staff-for-return':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(parcel_origin=self.branch, shipmentactivity__id=F(
                'latest'), shipmentactivity__shipment_status='Rider Assigned for Returning to the Sender')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentlistassignforreturn.html"
        elif status == 'pickedup-for-return':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(parcel_origin=self.branch, shipmentactivity__id=F(
                'latest'), shipmentactivity__shipment_status='Rider Picked up the Shipment for Return')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentlistpickedupforreturn.html"
        elif status == 'shipment-returned':
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=self.branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Shipment Returned to the Sender')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentlistshipmentreturned.html"
        else:
            shipmentlist = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
                parcel_origin=self.branch, shipmentactivity__id=F('latest'), shipmentactivity__shipment_status='Shipment Requested')
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(customer__full_name__icontains=keyword) | Q(sender_name__icontains=keyword) | Q(sender_city__name=keyword) | Q(sender_address__icontains=keyword) | Q(
                sender_contact_number__icontains=keyword) | Q(sender_email__icontains=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword)) if keyword is not None and keyword != "" else shipmentlist
            page = self.request.GET.get('page', 1)
            paginator = Paginator(shipmentlist, paginate_by)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                results = paginator.page(1)
            except EmptyPage:
                results = paginator.page(paginator.num_pages)
            context = {
                'shipmentlist': results,
                'paginate_by': paginate_by,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadshipmentlistrequested.html"

        return render(request, self.template_name, context)


class AjaxLogisticsAdminShipmentChangeStatusView(OperatorRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        shipments_ids = json.loads(request.GET.get("shipment_ids"))
        shipmentlist = Shipment.objects.filter(id__in=shipments_ids)
        shipments_count = shipmentlist.count()
        return JsonResponse({"status": "success", 'shipments_count': shipments_count, 'shipment_ids': [shipments_ids], 'shipmentlistvalues': list(shipmentlist.values('id', 'receiver_name', 'receiver_contact_number', 'receiver_address', 'tracking_code', 'receiver_city__name', 'shipping_charge', 'cod_handling_charge', 'rejection_handling_charge', 'weight', 'weight_unit', 'length', 'length_unit', 'breadth', 'height', 'cod_value', 'sender_name', 'sender_city__name', 'sender_address', 'sender_contact_number'))
                             })

    def post(self, request, *args, **kwargs):
        shipment_ids = eval(request.POST.get("shipment_ids"))
        shipmentlist = Shipment.objects.filter(id__in=shipment_ids)
        status = request.POST.get("status")
        time_now = timezone.localtime(timezone.now())
        if status == 'assignstaff':
            staff = request.POST.get("staff")
            rider = DeliveryPerson.objects.get(id=staff)
            # for shipment in shipmentlist:
            #     ShipmentActivity.objects.create(
            #         shipment=shipment, rider=rider, shipment_status='Rider Assigned for Pickup')
            [ShipmentActivity.objects.create(shipment=shipment, rider=rider, shipment_status='Rider Assigned for Pickup', status="Active") and DeliveryTask.objects.create(
                shipment=shipment, delivery_person=rider, task_type="Pick Up", task_status="Assigned", scheduled_date=time_now, status="Active") for shipment in shipmentlist]

            return JsonResponse({"status": "success"})

        elif status == 'riderpickedupshipment':
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Rider Picked up the Shipment')

            return JsonResponse({"status": "success"})

        elif status == 'cancelshipment':
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Shipment Canceled', status="Active")

            return JsonResponse({"status": "success"})

        elif status == 'pickedupshipment':
            DeliveryTask.objects.filter(shipment__id__in=shipment_ids, task_type="Pick Up").update(
                task_status='Completed Success', completed_date=time_now)
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Shipment Arrived at Warehouse', status="Active")

            return JsonResponse({"status": "success"})

        elif status == 'assignstafffordelivery':
            staff = request.POST.get("staff")
            rider = DeliveryPerson.objects.get(id=staff)
            # for shipment in shipmentlist:
            #     ShipmentActivity.objects.create(
            #         shipment=shipment, rider=rider, shipment_status='Rider Assigned for Dispatch to the Receiver', status="Active")
            [ShipmentActivity.objects.create(shipment=shipment, rider=rider, shipment_status='Rider Assigned for Dispatch to the Receiver', status="Active") and DeliveryTask.objects.create(
                shipment=shipment, delivery_person=rider, task_type="Drop Off", task_status="Assigned", scheduled_date=time_now, status="Active") for shipment in shipmentlist]

            return JsonResponse({"status": "success"})

        elif status == 'pickedupfordelivery':
            DeliveryTask.objects.filter(
                shipment__id__in=shipment_ids, task_type="Drop Off").update(task_status='Doing')
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Rider Picked up the Shipment for Dispatch', status="Active")

            return JsonResponse({"status": "success"})

        elif status == 'confirmdelivered':
            DeliveryTask.objects.filter(shipment__id__in=shipment_ids, task_type="Drop Off").update(
                task_status='Completed Success', completed_date=time_now)
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Shipment Delivered to the Receiver', status="Active")

            return JsonResponse({"status": "success"})

        elif status == 'returnedtowarehouse':
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Ready to Return to Sender', status="Active")

            return JsonResponse({"status": "success"})

        elif status == 'reasignshipmentfordelivery':
            staff = request.POST.get("staff")
            rider = DeliveryPerson.objects.get(id=staff)
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Shipment Returned to Logistics', status="Active")

            return JsonResponse({"status": "success"})

        elif status == 'assignshipmentforreturn':
            staff = request.POST.get("staff")
            rider = DeliveryPerson.objects.get(id=staff)
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, rider=rider, shipment_status='Rider Assigned for Returning to the Sender', status="Active")

            return JsonResponse({"status": "success"})

        elif status == 'pickedupshipmentforreturn':
            for shipment in shipmentlist:
                ShipmentActivity.objects.create(
                    shipment=shipment, shipment_status='Rider Picked up the Shipment for Return', status="Active")

            return JsonResponse({"status": "success"})

        # elif status == 'shipmentbundlecreate':
        #     shipmentbundle = ShipmentBundle.objects.create(
        #         source_branch=self.branch, status="Active")
        #     shipmentbundle.bundle_code = 'bundle-' + str(shipmentbundle.id)
        #     for shipment in shipmentlist:
        #         ShipmentBundleShipment.objects.create(shipment_bundle=shipmentbundle, shipment=shipment, shipment_passed_by=self.branch, status="Active")
        #     shipmentbundle.save()

        #     return JsonResponse({"status": "success"})

        else:
            return JsonResponse({"status": "error"})


class AjaxLogisticsAdminShipmentChargeUpdateView(OperatorRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            pk = request.POST.get('pk')
            attr = self.kwargs['attribute']
            value = request.POST.get('value')
            contenttype = ContentType.objects.get(
                app_label='umapp', model='branchoperator')
            if value is not None and value != "" and float(value) >= 0:
                if attr == 'shipping_charge':
                    try:
                        shipment = Shipment.objects.get(id=pk)
                        ShipmentRemark.objects.create(shipment=shipment, remarks="Shipping Charge Changes from " + str(
                            shipment.shipping_charge) + " to " + str(value) + ".", remarks_by_type=contenttype, remarks_by_id=self.operator.id, status="Pending")
                        shipment.shipping_charge = value
                        shipment.save()
                        return JsonResponse({'status': 'success', 'msg': 'Value changed!'})
                    except Exception as e:
                        return JsonResponse({'status': 'error', 'msg': 'Error!'})
                elif attr == "cod_handling_charge":
                    try:
                        shipment = Shipment.objects.get(id=pk)
                        ShipmentRemark.objects.create(shipment=shipment, remarks="COD Handling Charge Changes from " + str(
                            shipment.cod_handling_charge) + " to " + str(value) + ".", remarks_by_type=contenttype, remarks_by_id=self.operator.id, status="Pending")
                        shipment.cod_handling_charge = value
                        shipment.save()
                        return JsonResponse({'status': 'success', 'msg': 'Value changed!'})
                    except Exception as e:
                        return JsonResponse({'status': 'error', 'msg': 'Error!'})
                elif attr == "rejection_handling_charge":
                    try:
                        shipment = Shipment.objects.get(id=pk)
                        ShipmentRemark.objects.create(shipment=shipment, remarks="Rejection Handling Charge Changes from " + str(
                            shipment.rejection_handling_charge) + " to " + str(value) + ".", remarks_by_type=contenttype, remarks_by_id=self.operator.id, status="Pending")
                        shipment.rejection_handling_charge = value
                        shipment.save()
                        return JsonResponse({'status': 'success', 'msg': 'Value changed!'})
                    except Exception as e:
                        return JsonResponse({'status': 'error', 'msg': 'Error!'})
                else:
                    return JsonResponse({'status': 'error', 'msg': 'Error!'})
            else:
                return JsonResponse({'status': 'error', 'msg': 'Value should be greater or equals to 0'})


class CmpLogisitcsAdminLoadShipmentInfoView(OperatorRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        s_id = self.kwargs.get("pk")
        shipment = Shipment.objects.get(id=s_id)
        context = {
            "shipment": shipment,
        }
        return render(request, "logisticstemplates/cmp/cmplaloadshipmentinfo.html", context)


class LogisticsAdminShipmentCreateView(OperatorRequiredMixin, CreateView):
    template_name = "logisticstemplates/logisticsadminshipmentcreate.html"
    form_class = ShipmentCreateForm
    success_url = reverse_lazy('lmsapp:lashipmentlist')
    success_message = "Your shipment has been created."

    def form_valid(self, form):
        activity_by = self.operator
        form.instance.parcel_origin = self.branch
        shipment = form.save()
        shipment.tracking_code = 'tracking-' + str(shipment.id)
        shipment.save()
        ShipmentActivity.objects.create(status='Active', shipment=shipment, shipment_status='Shipment Requested',
                                        activity='Shipment Created by Logistics Branch', activity_by=activity_by)

        return super().form_valid(form)

    def get_success_url(self):
        return reverse("lmsapp:lashipmentdetail", kwargs={"pk": self.object.id})


class LogisticsAdminShipmentUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = "logisticstemplates/logisticsadminshipmentupdate.html"
    model = Shipment
    form_class = ShipmentCreateForm
    success_url = reverse_lazy('lmsapp:lashipmentlist')
    success_message = "Shipment Updated Successfully"

    def dispatch(self, request, *args, **kwargs):
        try:
            self.operator = request.user.branchoperator
            self.branch = self.operator.logistics_branch
            if self.branch.id == Shipment.objects.get(id=self.kwargs['pk']).parcel_origin.id:
                pass
            else:
                messages.error(request, 'Authorization Failed')
                return redirect('lmsapp:lashipmentlist')
        except Exception as e:
            messages.error(request, 'Authorization Failed')
            print('Dispatch in LogisticsAdminShipmentUpdateView', e)
            return redirect('lmsapp:lashipmentlist')

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        shipment = self.object
        context['shipmentactivityform'] = ShipmentActivityForm(
            instance=shipment.shipmentactivity_set.last())
        return context

    def form_valid(self, form):
        shipmentactivityform = ShipmentActivityForm(
            self.request.POST, self.request.FILES)
        shipment_status = shipmentactivityform.data['shipment_status']
        if shipment_status != self.object.shipmentactivity_set.last().shipment_status:
            ShipmentActivity.objects.create(
                shipment=self.object, shipment_status=shipment_status)

        return super().form_valid(form)

    def get_success_url(self):
        return reverse("lmsapp:lashipmentdetail", kwargs={"pk": self.object.id})


class LogisticsAdminShipmentDetailView(OperatorRequiredMixin, TemplateView):
    template_name = "logisticstemplates/logisticsadminshipmentdetail.html"

    def dispatch(self, request, *args, **kwargs):
        try:
            self.operator = request.user.branchoperator
            self.branch = self.operator.logistics_branch
            if self.branch.id == Shipment.objects.get(id=self.kwargs['pk']).parcel_origin.id:
                pass
            else:
                messages.error(request, 'Authorization Failed')
                return redirect('lmsapp:lashipmentlist')
        except Exception as e:
            messages.error(request, 'Authorization Failed')
            print('Dispatch in LogisticsAdminShipmentDetailView', e)
            return redirect('lmsapp:lashipmentlist')

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['shipment'] = Shipment.objects.get(id=self.kwargs['pk'])
        return context


class CmpLogisticsAdminLoadEnquiryListView(OperatorRequiredMixin, View):

    def get(self, request):
        status = self.request.GET.get('status')
        shipment_id = self.request.GET.get('shipment_id')
        if status == 'notes':
            context = {
                'notelist': ShipmentRemark.objects.filter(is_private=True, shipment__id=shipment_id).order_by('id')
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadnotelist.html"
        elif status == 'activities':
            context = {
                'notelist': ShipmentRemark.objects.filter(shipment__id=shipment_id).order_by('id')
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadnotelist.html"
        elif status == 'partner-chat':
            context = {
                'enquirylist': ShipmentRemark.objects.filter(is_private=False, shipment__id=shipment_id).order_by('id')
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadenquirylist.html"
        else:
            context = {
                'notelist': ShipmentRemark.objects.filter(is_private=True, shipment__id=shipment_id).order_by('id')
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadnotelist.html"

        return render(request, self.template_name, context)


class AjaxLogisticsAdminShipmentActivityCreateView(OperatorRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        shipment_id = self.kwargs['pk']
        change = self.request.POST.get('change')
        shipment = Shipment.objects.get(id=shipment_id)

        if change == 'addnote':
            note = self.request.POST.get('message')
            ctype = self.request.POST.get('c_model')
            ct = ContentType.objects.get(model=ctype.lower())
            ShipmentRemark.objects.create(status='Active', shipment=shipment,
                                          remarks=note, is_private=True, remarks_by_type=ct, remarks_by_id='1')

            return JsonResponse({
                                'success': "New Note Added",
                                })
        elif change == 'addchat':
            chat = self.request.POST.get('message')
            ctype = self.request.POST.get('c_model')
            ct = ContentType.objects.get(model=ctype.lower())
            ShipmentRemark.objects.create(status='Active', shipment=shipment,
                                          remarks=chat, is_private=False, remarks_by_type=ct, remarks_by_id='1')

            return JsonResponse({
                                'success': "Message Sent To Partner",
                                })

        else:
            return JsonResponse({
                                'error': "Something went wrong. Please refersh page and try again",
                                })


class LogisticsAdminBundleListView(OperatorRequiredMixin, TemplateView):
    template_name = "logisticstemplates/logisticsadminbundlelist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['staffform'] = StaffForm(branch_id=self.branch.id)
        return context


class CmpLogisticsAdminLoadBundleListView(OperatorRequiredMixin, View):

    def get(self, request):
        status = self.request.GET.get('status')
        if status == 'staff-assigned':
            context = {
                'bundlelist': ShipmentBundle.objects.filter(source_branch=self.branch, is_sent=False, is_delivered=False).exclude(delivery_person=None)
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadbundleliststaffassigned.html"

        elif status == 'bundle-dispatched':
            context = {
                'bundlelist': ShipmentBundle.objects.filter(source_branch=self.branch, is_sent=True, is_delivered=False)
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadbundlelistdispatched.html"

        elif status == 'bundle-at-receiver':
            context = {
                'bundlelist': ShipmentBundle.objects.filter(source_branch=self.branch, is_sent=True, is_delivered=True)
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadbundlelistatreceived.html"

        elif status == 'bundle-delivered':
            context = {
                'bundlelist': ShipmentBundle.objects.filter(source_branch=self.branch, is_sent=True, is_delivered=True, is_confirmed=True)
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadbundlelistdispatched.html"

        else:
            context = {
                'bundlelist': ShipmentBundle.objects.filter(source_branch=self.branch, is_sent=False, is_delivered=False, delivery_person=None)
                # 'staffform': StaffForm(branch_staff_id=1)
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadbundlelistpending.html"

        return render(request, self.template_name, context)


class AjaxLogisticsAdminShipmentBundleShipmentChargeUpdateView(OperatorRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            pk = request.POST.get('pk')
            attr = self.kwargs['attribute']
            value = request.POST.get('value')
            contenttype = ContentType.objects.get(
                app_label='umapp', model='branchoperator')
            if value is not None and value != "" and float(value) >= 0:
                if attr == 'shipping_charge':
                    try:
                        shipment = ShipmentBundleShipment.objects.get(id=pk)
                        # ShipmentRemark.objects.create(shipment=shipment, remarks="Shipping Charge Changes from "+ str(shipment.shipping_charge) + " to " + str(value) + ".", remarks_by_type=contenttype, remarks_by_id=self.operator.id, status="Pending")
                        shipment.shipping_charge = value
                        shipment.save()
                        return JsonResponse({'status': 'success', 'msg': 'Value changed!'})
                    except Exception as e:
                        return JsonResponse({'status': 'error', 'msg': 'Error!'})
                elif attr == "cod_handling_charge":
                    try:
                        shipment = ShipmentBundleShipment.objects.get(id=pk)
                        # ShipmentRemark.objects.create(shipment=shipment, remarks="COD Handling Charge Changes from "+ str(shipment.cod_handling_charge) + " to " + str(value) + ".", remarks_by_type=contenttype, remarks_by_id=self.operator.id, status="Pending")
                        shipment.cod_handling_charge = value
                        shipment.save()
                        return JsonResponse({'status': 'success', 'msg': 'Value changed!'})
                    except Exception as e:
                        return JsonResponse({'status': 'error', 'msg': 'Error!'})
                elif attr == "rejection_handling_charge":
                    try:
                        shipment = ShipmentBundleShipment.objects.get(id=pk)
                        # ShipmentRemark.objects.create(shipment=shipment, remarks="Rejection Handling Charge Changes from "+ str(shipment.rejection_handling_charge) + " to " + str(value) + ".", remarks_by_type=contenttype, remarks_by_id=self.operator.id, status="Pending")
                        shipment.rejection_handling_charge = value
                        shipment.save()
                        return JsonResponse({'status': 'success', 'msg': 'Value changed!'})
                    except Exception as e:
                        return JsonResponse({'status': 'error', 'msg': 'Error!'})
                else:
                    return JsonResponse({'status': 'error', 'msg': 'Error!'})
            else:
                return JsonResponse({'status': 'error', 'msg': 'Value should be greater or equals to 0'})


class AjaxLogisticsAdminShipmentBundleDestinationOptionModalView(OperatorRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        value = request.GET.get("value")
        destination = request.GET.get("destination")
        if value == "distributioncenter":
            branchlist = LogisticsBranch.objects.filter(
                logistics_company=self.company).exclude(id=self.branch.id)
            return JsonResponse({"message": "success", 'distributioncenter': list(branchlist.values('id', 'branch_city__name', 'logistics_company__company_name'))})
        elif value == "logisticpartner":
            logisticscompany = LogisticsCompany.objects.all().exclude(id=self.company.id)
            return JsonResponse({"message": "success", "logisticpartner": list(logisticscompany.values())})
        elif destination == "logisticpartner":
            branchlist = LogisticsBranch.objects.filter(
                logistics_company__id=value)
            return JsonResponse({"message": "success", 'logisticsbranch': list(branchlist.values('id', 'branch_city__name', 'logistics_company__company_name'))})

        return JsonResponse({"message": "error"})


class CmpLogisitcsAdminLoadBundleInfoView(OperatorRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        b_id = self.kwargs.get("pk")
        bundle = ShipmentBundle.objects.get(id=b_id)
        context = {
            "bundle": bundle,
        }
        return render(request, "logisticstemplates/cmp/cmplaloadbundleinfo.html", context)


class AjaxLogisticsAdminBundleChangeStatusView(OperatorRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        bundle_ids = json.loads(request.GET.get("bundle_ids"))
        bundlelist = ShipmentBundle.objects.filter(id__in=bundle_ids)
        bundle_count = bundlelist.count()
        shipmentbundlelist = ShipmentBundleShipment.objects.filter(
            shipment_bundle__id__in=bundle_ids)
        shipmentbundlelist_count = shipmentbundlelist.count()
        # return JsonResponse({"status": "success", 'shipments_count': shipments_count, 'shipment_ids': [shipments_ids], 'shipmentlistvalues': list(shipmentlist.values('id', 'receiver_name', 'receiver_contact_number', 'receiver_address', 'tracking_code', 'receiver_city__name', 'shipping_charge', 'cod_handling_charge', 'rejection_handling_charge', 'weight', 'weight_unit', 'length', 'length_unit', 'breadth', 'height', 'cod_value'))
        #                      })
        print(shipmentbundlelist.values('id', 'shipment__tracking_code',
              'shipping_charge', 'cod_handling_charge', 'rejection_handling_charge'))
        return JsonResponse({"status": "success", 'bundle_count': bundle_count, 'shipmentbundlelist_count': shipmentbundlelist_count, 'bundle_ids': [bundle_ids],
                            'shipmentbundlelistvalues': list(shipmentbundlelist.values('id', 'shipment__receiver_name', 'shipment__receiver_contact_number', 'shipment__receiver_address', 'shipment__tracking_code', 'shipment__receiver_city__name', 'shipping_charge', 'cod_handling_charge', 'rejection_handling_charge', 'shipment__weight', 'shipment__weight_unit', 'shipment__length', 'shipment__length_unit', 'shipment__breadth', 'shipment__height', 'shipment__cod_value'))
                             })

    def post(self, request, *args, **kwargs):
        bundle_ids = eval(request.POST.get("bundle_ids")
                          ) if request.POST.get("bundle_ids") else ""
        bundlelist = ShipmentBundle.objects.filter(id__in=bundle_ids)
        status = request.POST.get("status")
        if status == 'bundleassignconfirm':
            bundlelist.update(delivery_person=DeliveryPerson.objects.get(
                id=request.POST.get("staff")))
            return JsonResponse({"status": "success"})

        elif status == 'bundlecreateconfirm':
            shipmentbundle = ShipmentBundle.objects.create(
                source_branch=self.branch, status="Active", destination_branch=LogisticsBranch.objects.get(id=request.POST.get("destination")))
            shipmentbundle.bundle_code = 'bundle-' + str(shipmentbundle.id)
            shipmentbundle.save()
            return JsonResponse({"status": "success"})

        elif status == 'bundlereleaseconfirm':
            # [ShipmentBundleShipment.objects.filter(shipment_bundle=bundle).delete() for bundle in bundlelist]
            ShipmentBundleShipment.objects.filter(
                shipment_bundle__in=bundlelist).delete()
            return JsonResponse({"status": "success"})

        elif status == 'bundlesentconfirm':
            bundlelist.update(
                is_sent=True, sent_date=timezone.localtime(timezone.now()))
            # for bundle in bundlelist:
            #     bundle.is_sent = True
            #     bundle.sent_date = timezone.localtime(timezone.now())
            #     bundle.save()

            return JsonResponse({"status": "success"})

        elif status == 'confirmbundles':
            bundlelist.update(
                is_delivered=True, sent_delivered=timezone.localtime(timezone.now()))
            # for bundle in bundlelist:
            #     bundle.is_delivered = True
            #     bundle.sent_delivered = timezone.localtime(timezone.now())
            #     bundle.save()
        elif status == 'staffassignincomingbundles':
            staff = request.POST.get("rider")
            staff = DeliveryPerson.objects.get(id=staff)

            for bundle in bundlelist:
                DeliveryTask.objects.create(shipment_bundle=bundle, delivery_person=staff, task_type='Bundle Pick Up',
                                            task_status='Assigned', scheduled_date=timezone.localtime(timezone.now()))

            return JsonResponse({"status": "success"})

        elif status == 'arrivedincomingbundles':
            for bundle in bundlelist:
                bundle.is_delivered = True
                bundle.sent_delivered = timezone.localtime(timezone.now())
                bundle.save()

            return JsonResponse({"status": "success"})

        elif status == 'confirmincomingbundles':
            for bundle in bundlelist:
                bundle.is_confirmed = True
                bundle.confirmed_date = timezone.localtime(timezone.now())
                bundle.save()

            return JsonResponse({"status": "success"})

        elif status == 'bundlerejectconfirm':
            for bundle in bundlelist:
                print('need to reject bundle++++')

            return JsonResponse({"status": "success"})

        else:
            return JsonResponse({"status": "error"})


class LogisticsAdminBundleUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = "logisticstemplates/logisticsadminbundleupdate.html"
    model = ShipmentBundle
    form_class = BundleCreateForm
    success_url = reverse_lazy('lmsapp:labundlelist')
    success_message = "Bundle Updated Successfully"

    # def dispatch(self, request, *args, **kwargs):
    #     try:
    #         self.operator = request.user.branchoperator
    #         self.branch = self.operator.logistics_branch
    #         if self.branch.id == ShipmentBundle.objects.get(id=self.kwargs['pk']).parcel_origin.id:
    #             pass
    #         else:
    #             messages.error(request, 'Authorization Failed')
    #             return redirect('lmsapp:labundlelist')
    #     except Exception as e:
    #         messages.error(request, 'Authorization Failed')
    #         print('Dispatch in LogisticsAdminBundleUpdateView', e)
    #         return redirect('lmsapp:labundlelist')

    #     return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['bundle'] = self.object
        return context

    def form_valid(self, form):
        # shipmentactivityform = ShipmentActivityForm(
        #     self.request.POST, self.request.FILES)
        # shipment_status = shipmentactivityform.data['shipment_status']
        # if shipment_status != self.object.shipmentactivity_set.last().shipment_status:
        #     ShipmentActivity.objects.create(shipment=self.object, shipment_status=shipment_status)

        return super().form_valid(form)

    def get_success_url(self):
        return reverse("lmsapp:lashipmentdetail", kwargs={"pk": self.object.id})


class AjaxLogisticsAdminBundleShipmentChangeStatusView(OperatorRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        shipments_ids = json.loads(request.GET.get("shipment_ids"))
        shipmentlist = Shipment.objects.filter(id__in=shipments_ids)
        shipments_count = shipmentlist.count()
        return JsonResponse({"status": "success", 'shipments_count': shipments_count, 'shipment_ids': [shipments_ids]
                             })

    def post(self, request, *args, **kwargs):
        status = request.POST.get("status")

        if status == 'releasebundleshipment':
            shipment_ids = eval(request.POST.get("shipment_ids"))
            sbslist = ShipmentBundleShipment.objects.filter(
                id__in=shipment_ids)
            bundle_id = request.POST.get("bundle_id")
            bundle = ShipmentBundle.objects.get(id=bundle_id)
            sbslist.delete()

            return JsonResponse({"status": "success"})

        elif status == 'addbundleshipment':
            bundle_id = request.POST.get("bundle_id")
            bundle = ShipmentBundle.objects.get(id=bundle_id)
            tracking_code = request.POST.get('tracking_code')

            try:
                shipment = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).get(parcel_origin=self.branch, shipmentactivity__id=F(
                    'latest'), tracking_code=tracking_code, shipmentbundleshipment=None, shipmentactivity__shipment_status='Shipment Arrived at Warehouse')
                ShipmentBundleShipment.objects.create(
                    shipment_bundle=bundle, shipment=shipment, shipment_passed_by=self.branch)
                shipment = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).get(parcel_origin=self.branch, shipmentactivity__id=F(
                    'latest'), tracking_code=tracking_code, shipmentbundle=None, shipmentactivity__shipment_status='Shipment Arrived at Warehouse')
                bundle.shipments.add(shipment)

            except Exception as e:
                return JsonResponse({"error": "error"})

            return JsonResponse({"status": "success"})

        else:
            return JsonResponse({"status": "error"})


class LogisticsAdminIncomingBundleListView(OperatorRequiredMixin, TemplateView):
    template_name = "logisticstemplates/logisticsadminincomingbundlelist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['staffform'] = StaffForm(branch_id=self.branch.id)
        return context


class CmpLogisticsAdminLoadIncomingBundleListView(OperatorRequiredMixin, View):

    def get(self, request):
        status = self.request.GET.get('status')
        if status == 'staff-assigned-bundles':
            context = {
                'bundlelist': ShipmentBundle.objects.filter(destination_branch=self.branch, is_sent=True, is_delivered=False, is_confirmed=False).exclude(deliverytask=None)
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadbundlelistincomingstaffassigned.html"

        elif status == 'arrived-bundles':
            context = {
                'bundlelist': ShipmentBundle.objects.filter(destination_branch=self.branch, is_sent=True, is_delivered=True, is_confirmed=False)
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadbundlelistarrived.html"

        elif status == 'confirmed-bundles':
            context = {
                'bundlelist': ShipmentBundle.objects.filter(destination_branch=self.branch, is_sent=True, is_delivered=True, is_confirmed=True)
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadbundlelistconfirmed.html"

        else:
            context = {
                'bundlelist': ShipmentBundle.objects.filter(destination_branch=self.branch, is_sent=True, is_delivered=False, is_confirmed=False, deliverytask=None)
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadbundlelistincoming.html"

        return render(request, self.template_name, context)


class CmpLogisticsAdminLoadSidebarView(OperatorRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        status = self.request.GET.get('status')
        if status == 'shipmentlist':
            selected_ids = json.loads(request.GET.get("selected_ids"))
            shipmentlist = Shipment.objects.filter(id__in=selected_ids)
            shipment_count = shipmentlist.count()
            context = {
                'shipmentlist': shipmentlist,
                'shipment_count': shipment_count,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadsidebarshipmentdetail.html"
        else:
            selected_ids = json.loads(request.GET.get("selected_ids"))
            bundlelist = ShipmentBundle.objects.filter(id__in=selected_ids)
            bundle_count = bundlelist.count()
            context = {
                'bundlelist': bundlelist,
                'bundle_count': bundle_count,
            }
            self.template_name = "logisticstemplates/cmp/cmplaloadsidebarbundledetail.html"
        # return JsonResponse({"status": "success", 'bundle_count': bundle_count, 'bundle_ids': [bundle_ids]
        #                      })
        return render(request, self.template_name, context)


class LogisticAdminShipmentShippingLabelView(OperatorRequiredMixin, TemplateView):
    template_name = "logisticstemplates/lashipmentshippinglabel.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['date_time'] = timezone.localtime(timezone.now())
        context['org'] = self.company
        # if self.kwargs:
        context['shipment'] = Shipment.objects.get(id=self.kwargs['pk'])
        # else:
        #     shipments_ids = json.loads(self.request.GET.get("shipment_ids"))
        #     context['shipmentlist'] = Shipment.objects.filter(id__in=shipments_ids)

        return context

    def post(self, request, *args, **kwargs):
        shipments_ids = json.loads(self.request.POST.get("shipment_ids"))
        context = {
            'date_time': timezone.localtime(timezone.now()),
            'org': self.company,
            'shipmentlist': Shipment.objects.filter(id__in=shipments_ids)
        }

        return render(request, self.template_name, context)


# class CustomerSupportTicketListView(OperatorRequiredMixin,ListView):
#     template_name = 'logisticstemplates/settings/customersupportticketlist.html'
#     model = CustomerSupportTicket


#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         ticket_status = self.request.GET.get('ticket_status', 'Ticket Open')
#         context['selected'] = ticket_status
#         if ticket_status == 'All Tickets':
#             transferrequests  = CustomerSupportTicket.objects.filter(raised_to=self.branch).order_by('-created_at')
#             page = self.request.GET.get('page', 1)
#             paginator = Paginator(transferrequests, 4)
#             try:
#                 results = paginator.page(page)
#             except PageNotAnInteger:
#                 results = paginator.page(1)
#             except EmptyPage:
#                 results = paginator.page(paginator.num_pages)
#             context['customers'] = results

#         elif ticket_status == 'Ticket Reviewing':
#             transferrequests= CustomerSupportTicket.objects.filter(raised_to=self.branch,ticket_status = 'Reviewing').order_by('-created_at')
#             page = self.request.GET.get('page', 1)
#             paginator = Paginator(transferrequests, 4)
#             try:
#                 results = paginator.page(page)
#             except PageNotAnInteger:
#                 results = paginator.page(1)
#             except EmptyPage:
#                 results = paginator.page(paginator.num_pages)
#             context['customers'] = results
#         elif ticket_status == 'Ticket Solved':
#             transferrequests= CustomerSupportTicket.objects.filter(raised_to=self.branch,ticket_status = 'Solved').order_by('-created_at')
#             page = self.request.GET.get('page', 1)
#             paginator = Paginator(transferrequests, 4)
#             try:
#                 results = paginator.page(page)
#             except PageNotAnInteger:
#                 results = paginator.page(1)
#             except EmptyPage:
#                 results = paginator.page(paginator.num_pages)
#             context['customers'] = results
#         else:
#             transferrequests= CustomerSupportTicket.objects.filter(raised_to=self.branch,ticket_status = 'Open').order_by('-created_at')
#             page = self.request.GET.get('page', 1)
#             paginator = Paginator(transferrequests, 4)
#             try:
#                 results = paginator.page(page)
#             except PageNotAnInteger:
#                 results = paginator.page(1)
#             except EmptyPage:
#                 results = paginator.page(paginator.num_pages)
#             context['customers'] = results
#         return context

#     def get_queryset(self): # new
#         search_text = self.request.GET.get('search', 0)
#         print(search_text)
#         return CustomerSupportTicket.objects.filter(
#             Q(raised_by__full_name__icontains=search_text))


class CustomerSupportTicketListView(OperatorRequiredMixin, ListView):
    template_name = 'logisticstemplates/settings/customersupportticketlist.html'
    queryset = CustomerSupportTicket.objects.all().order_by('-created_at')
    context_object_name = "customers"
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ticket_status = self.request.GET.get('ticket_status', 'Open')
        context['selected'] = ticket_status
        context['notifications'] = CustomerSupportTicket.objects.filter(
            status='Pending').count()
        customers_list = CustomerSupportTicket.objects.filter(
            status='Pending', raised_to=self.branch)
        number_of_notices = list(customers_list.order_by('-created_at')[0: 3])
        context['notification_list'] = number_of_notices
        change_status = self.request.GET.get('change_status', 0)
        if change_status == 'Pending':
            customers_list.update(status="Active")
            # status = CustomerSupportTicket.objects.filter(
            #     raised_to=self.branch, status='Pending')
            # for status in status:
            #     status.status = 'Active'
            #     status.save()

        notific_id = self.request.GET.get('notific_id', 0)
        ticket = CustomerSupportTicket.objects.filter(id=notific_id).last()
        if ticket is not None:
            ticket.status = 'Active'
            ticket.save()

        content_type = ContentType.objects.get(
            app_label='umapp', model='logisticscustomer')
        context['opnot'] = CustomerTicketFollowup.objects.filter(
            status='Pending', followup_by_type=content_type, ticket__raised_to=self.branch).count()
        msg = CustomerTicketFollowup.objects.filter(
            status='Pending', followup_by_type=content_type, ticket__raised_to=self.branch)
        msg_number = list(msg.order_by('-created_at')[0: 3])
        context['msglist'] = set(msg_number)
        followup_all__status = self.request.GET.get('change_status_msg')
        if followup_all__status == 'Pending':
            msg.update(status="Active")
            # followmsg = CustomerTicketFollowup.objects.filter(
            #     followup_by_type=content_type, ticket__raised_to=self.branch, status='Pending')
            # for msg in followmsg:
            #     msg.status = 'Active'
            #     msg.save()
        # else:
        #     pass
        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        ticket_status = self.request.GET.get('ticket_status', 'Open')
        keyword = self.request.GET.get('keyword', '')
        paginate_by = self.request.GET.get('paginate_by', 5)
        try:
            paginate_by = int(paginate_by)
        except:
            paginate_by = 25
        self.paginate_by = paginate_by
        new_queryset = queryset.filter(raised_to=self.branch) if ticket_status == 'all' or ticket_status == "" or ticket_status is None\
            else queryset.filter(raised_to=self.branch, ticket_status=ticket_status)
        new_queryset = new_queryset.filter(Q(raised_by__full_name__icontains=keyword) | Q(
            issue_detail__icontains=keyword)) if keyword is not None and keyword != "" else new_queryset
        return new_queryset


class CustomerSupportTicketListAjaxView(OperatorRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        keyword = request.GET.get('keyword', 0)


class CustomerTicketNoticeStatusList(OperatorRequiredMixin, TemplateView):
    template_name = 'logisticstemplates/settings/ticketstatuslistoperator.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ticketstatuslist'] = CustomerSupportTicket.objects.filter(
            status='Pending').order_by('-created_at').distinct('ticket')
        ticket_id = self.request.GET.get('check_id', 0)
        all_status = self.request.GET.get('mark_all_read')
        ticket = CustomerSupportTicket.objects.filter(
            raised_to=self.branch, id=ticket_id).last()
        if ticket is not None:
            ticket.status = 'Active'
            ticket.save()
        else:
            print('no ticket selected yet !')
        if all_status == 'mark_all_read':
            ticket_cust = CustomerSupportTicket.objects.filter(
                status='Pending', raised_to=self.branch)
            for tickets in ticket_cust:
                tickets.status = 'Active'
                tickets.save()
        else:
            print('all not selected')
        return context


class CustomerTicketFollowUpdMessageStatusList(OperatorRequiredMixin, TemplateView):
    template_name = 'logisticstemplates/settings/ticketfollowmessagelistoperator.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        content_type = ContentType.objects.get(
            app_label='umapp', model='logisticscustomer')
        context['messagestatuslist'] = CustomerTicketFollowup.objects.filter(
            status='Pending', followup_by_type=content_type, ticket__raised_to=self.branch)
        message_id = self.request.GET.get('check_id', 0)
        all_status = self.request.GET.get('mark_all_read')
        message = CustomerTicketFollowup.objects.filter(
            status='Pending', followup_by_type=content_type, ticket__raised_to=self.branch, id=message_id).last()
        if message is not None:
            message.status = 'Active'
            message.save()
        else:
            print('no ticket selected yet !')
        if all_status == 'mark_all_read':
            ticket_cust = CustomerTicketFollowup.objects.filter(
                status='Pending', followup_by_type=content_type, ticket__raised_to=self.branch)
            for tickets in ticket_cust:
                tickets.status = 'Active'
                tickets.save()
        else:
            print('all not selected')
        return context


class CustomerSupportTicketDetailView(OperatorRequiredMixin, DetailView):
    template_name = 'logisticstemplates/settings/customersupportticketdetail.html'
    model = CustomerSupportTicket
    context_object_name = 'ticketdetail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ticket_status = self.request.GET.get('ticket_status', 'Open')
        ticket_id = self.kwargs['pk']
        ticket = CustomerSupportTicket.objects.get(id=ticket_id)
        customer_id = ticket.raised_by.id
        customer = LogisticsCustomer.objects.get(id=customer_id)
        all_tickets = customer.customersupportticket_set.all()
        context['messageform'] = CustomerTicketFollowupForm
        content_type = ContentType.objects.get(
            app_label='umapp', model='logisticscustomer'
        )
        msg = CustomerTicketFollowup.objects.filter(
            ticket=ticket, followup_by_type=content_type)
        if ticket_status == 'All':
            ticket_detail = all_tickets.filter(
                raised_to=self.branch).order_by('-created_at')
        elif ticket_status == 'Reviewing':
            ticket_detail = all_tickets.filter(
                raised_to=self.branch, ticket_status='Reviewing').order_by('-created_at')
        elif ticket_status == 'Solved':
            ticket_detail = all_tickets.filter(
                raised_to=self.branch, ticket_status='Solved').order_by('-created_at')
        else:
            ticket_detail = all_tickets.filter(
                raised_to=self.branch, ticket_status='Open').order_by('-created_at')
        context['customers_ticket'] = ticket_detail
        active = self.request.GET.get('Active', 0)
        if active == 'Active':
            ticket.status = 'Active'
            ticket.save()
            for msg in msg:
                msg.status = 'Active'
                msg.save()
        else:
            pass
        return context


class CustomerSupportTicketAjaxView(View):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        text = request.GET.get('text', 0)
        request_from = request.GET.get('request_from', 0)
        raised_ticket = CustomerSupportTicket.objects.get(id=pk)
        status = request.GET.get('status', 0)
        customer_ticket = CustomerSupportTicket.objects.get(id=pk)
        if status == 'Open':
            customer_ticket.ticket_status = 'Open'
            customer_ticket.save()
        elif status == 'Reviewing':
            customer_ticket.ticket_status = 'Reviewing'
            customer_ticket.save()
        elif status == 'Solved':
            customer_ticket.ticket_status = 'Solved'
            customer_ticket.save()
        else:
            pass
        if request_from == 'operator':
            content_type = ContentType.objects.get(
                app_label='umapp', model='logisticsbranch'
            )
            branch = self.request.user.branchoperator.logistics_branch
            try:
                CustomerTicketFollowup.objects.create(status="Pending",
                                                      ticket=raised_ticket, followup_by_type=content_type, followup_by_id=branch.id, followup_message=text)
            except Exception as e:
                print(e)
        return JsonResponse({'message': 'success', 'text': text})


class CustomerSupportTicketUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = 'logisticstemplates/settings/customersupportticketupdate.html'
    model = CustomerSupportTicket
    form_class = CustomerSupportTicketUpdateForm
    success_message = 'Ticket Updated Successfully'
    success_url = reverse_lazy('lmsapp:operatorcustomerticketlist')

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('lmsapp:operatorcustomerticketdetail', kwargs={'pk': self.kwargs['pk']})
        else:
            return reverse_lazy('lmsapp:operatorcustomerticketlist')


class LogisticsNoticeListView(OperatorRequiredMixin, ListView):
    template_name = 'logisticstemplates/settings/logisticsnoticelist.html'
    queryset = LogisticsNotice.objects.all().order_by('-created_at')
    context_object_name = 'opnoticelist'
    paginate_by = 8

    def get_queryset(self):
        queryset = LogisticsNotice.objects.filter(
            logistics_company=self.company).order_by('-created_at')
        return queryset


class LogisticsNoticeCreateView(OperatorRequiredMixin, CreateView):
    template_name = 'logisticstemplates/settings/logisticsnoticecreate.html'
    form_class = LogisticsNoticeForm
    success_url = reverse_lazy('lmsapp:opnoticelist')

    def form_valid(self, form):
        form.instance.status = 'Active'
        form.instance.logistics_company = self.company
        form.instance.logistics_branch = self.branch
        form.instance.created_by = self.operator
        form.save()
        messages.success(self.request, 'successfully created !!')
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # if self.request.is_ajax():
        #     term = self.request.GET.get("term")
        #     logisticcustomers = LogisticsCustomer.objects.all().filter(full_name__icontains=term)
        #     response_content = list(logisticcustomers.values())
        #     return JsonResponse(response_content, safe=False)
        return context


class AjaxLALogisticCustomerListView(OperatorRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        term = self.request.GET.get("term")
        logisticcustomers = LogisticsCustomer.objects.all().filter(full_name__icontains=term)
        response_content = list(logisticcustomers.values())
        return JsonResponse(response_content, safe=False)


class LogisticsNoticeUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = 'logisticstemplates/settings/logisticsnoticeupdate.html'
    model = LogisticsNotice
    form_class = LogisticsNoticeForm
    success_message = 'Successfully Updated !!'
    success_url = reverse_lazy('lmsapp:opnoticelist')


class LogisticsNoticeDetailView(OperatorRequiredMixin, DetailView):
    template_name = 'logisticstemplates/settings/logisticsnoticedetail.html'
    model = LogisticsNotice
    context_object_name = 'opnoticedetail'


class LogisticsAdminLogisticsCustomerShipmentPaymentListView(OperatorRequiredMixin, ListView):
    template_name = "logisticstemplates/lalcshipmentpaymentlist.html"
    queryset = Shipment.objects.all()
    context_object_name = "shipmentlist"


class LogisticsCustomerRequestQuotationFromBranch(OperatorRequiredMixin, ListView):
    template_name = 'logisticstemplates/settings/customerrequestquotationfrombranch.html'
    model = LogisticsCustomerContract
    context_object_name = 'shipmentrequest'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['requestshipmentlist'] = LogisticsCustomerContract.objects.filter(
            status="Active", is_signed=False, first_party=self.branch).order_by('-created_at')
        return context


class LogisticsCustomerRequestQuotationFromBranchApproved(OperatorRequiredMixin, ListView):
    template_name = 'logisticstemplates/settings/customerrequestquotationfrombranchapproved.html'
    model = LogisticsCustomerContract
    context_object_name = 'shipmentrequestapproved'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['requestshipmentapprovedlist'] = LogisticsCustomerContract.objects.filter(
            status="Active", is_signed=False, first_party=self.branch).order_by('-created_at')
        return context


class LogisticsCustomerSignedRequestQuotationFromBranchApproved(OperatorRequiredMixin, ListView):
    template_name = 'logisticstemplates/settings/customerrequestquotationfrombranchapprovedsigned.html'
    model = LogisticsCustomerContract
    context_object_name = 'shipmentrequestapproved'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['requestshipmentapprovedsignedlist'] = LogisticsCustomerContract.objects.filter(
            status="Active", is_signed=True, first_party=self.branch).order_by('-created_at')
        return context


class LogisticsCustomerRequestQuotationFromBranchApprovedAjaxView(OperatorRequiredMixin, View):
    def get(self, request):
        keyword = request.GET.get('keyword')
        keyword2 = request.GET.get('keyword2')
        if keyword:
            customer = LogisticsCustomerContract.objects.filter(
                first_party=self.branch, is_signed=False, status='Active').filter(second_party__full_name__icontains=keyword)
            zonal = LogisticCustomerZonalContract.objects.all()
        else:
            customer = LogisticsCustomerContract.objects.filter(
                first_party=self.branch, is_signed=False)
        return JsonResponse({"customerlist": list(customer.values('id', 'second_party__full_name', 'second_party__id', 'created_at', 'second_party__city__name'))})


class LogisticsCustomerRequestQuotationFromBranchApprovedAjaxView2(OperatorRequiredMixin, View):
    def get(self, request):
        keyword2 = request.GET.get('keyword2')
        print(keyword2)
        if keyword2:
            customer2 = LogisticsCustomerContract.objects.filter(
                first_party=self.branch, is_signed=True).filter(second_party__full_name__icontains=keyword2)
        else:
            customer2 = LogisticsCustomerContract.objects.filter(
                first_party=self.branch, is_signed=True)
        return JsonResponse({"customerlist": list(customer2.values('id', 'second_party__full_name', 'second_party__id', 'created_at', 'second_party__city__name'))})


class LogisticsCustomerRequestQuotationFromBranchCreateView(OperatorRequiredMixin, UpdateView):
    template_name = 'logisticstemplates/settings/customerrequestquotationfrombranchcreate.html'
    form_class = LogisticsCustomerContractForm
    model = LogisticsCustomerContract
    context_object_name = 'shipmentrequestupdate'
    success_url = reverse_lazy('lmsapp:customerrequestshipment')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["shipments"] = LogisticsBranchShippingZone.objects.filter(
            logistics_branch=self.branch)
        return context

    def form_valid(self, form):
        form.instance.first_party = self.branch
        form.instance.status = 'Active'
        shipping = form.save()
        shippingzone = LogisticsBranchShippingZone.objects.filter(
            logistics_branch=self.branch)
        for ship in shippingzone:
            shipping_zone = "shipping_zone__" + str(ship.id)
            shipping_charge = "shipping_charge__" + str(ship.id)
            cod_handling_charge = "cod_handling__" + str(ship.id)
            rejection_handling_charge = "rejection_charge__" + str(ship.id)
            shippingcharge = self.request.POST.get(shipping_charge, 0)
            rejectionhandlingcharge = self.request.POST.get(
                rejection_handling_charge, 0)
            codhandlingcharge = self.request.POST.get(
                cod_handling_charge, 0)
            shippingzone_id = self.request.POST.get(shipping_zone, 0)
            LogisticCustomerZonalContract.objects.create(logistic_customer_contract=shipping, service_provider=self.branch, shipping_zone=ship, shipping_charge=shippingcharge,
                                                         cod_handling_charge_pct=codhandlingcharge, rejection_handling_charge_pct=rejectionhandlingcharge, is_confirmed=False, status='Active')
            messages.success(
                self.request, 'Logistics Customer Contract Created Successfully !!')
        else:
            pass
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)

    def get_success_url(self, **kwargs):
        return reverse_lazy("lmsapp:customerrequestquotationdetail", kwargs={'pk': self.object.id})


class LogisticsCustomerRequestQuotationFromBranchUpdateView(OperatorRequiredMixin, UpdateView):
    template_name = 'logisticstemplates/settings/customerrequestquotationfrombranchupdate.html'
    form_class = LogisticsCustomerContractForm
    model = LogisticsCustomerContract
    context_object_name = 'shipmentrequestupdate'
    success_url = reverse_lazy('lmsapp:customerrequestshipment')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        customer_contract = LogisticsCustomerContract.objects.get(
            id=self.kwargs['pk'])
        context["shipments"] = LogisticCustomerZonalContract.objects.filter(
            service_provider=self.branch)
        return context

    def form_valid(self, form):
        form.instance.first_party = self.branch
        form.instance.status = 'Active'
        main_contract = form.save()
        shippingzone = LogisticCustomerZonalContract.objects.filter(
            logistic_customer_contract=main_contract)
        for ship in shippingzone:
            shipping_zone = "shipping_zone__" + str(ship.id)
            shipping_charge = "shipping_charge__" + str(ship.id)
            cod_handling_charge = "cod_handling__" + str(ship.id)
            rejection_handling_charge = "rejection_charge__" + str(ship.id)
            shippingcharge = self.request.POST.get(shipping_charge, 0)
            rejectionhandlingcharge = self.request.POST.get(
                rejection_handling_charge, 0)
            codhandlingcharge = self.request.POST.get(
                cod_handling_charge, 0)
            ship.shipping_charge = shippingcharge
            ship.cod_handling_charge_pct = codhandlingcharge
            ship.rejection_handling_charge_pct = rejectionhandlingcharge
            ship.save()
            messages.success(
                self.request, 'Logistics Customer Contract Updated Successfully !!')
        else:
            pass
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)

    def get_success_url(self, **kwargs):
        return reverse_lazy("lmsapp:customerrequestquotationdetail", kwargs={'pk': self.object.id})


class LogisticsCustomerRequestQuotationFromBranchDetail(OperatorRequiredMixin, DetailView):
    template_name = 'logisticstemplates/settings/customerrequestquotationfrombranchdetail.html'
    model = LogisticsCustomerContract
    context_object_name = 'shipmentrequestdetail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['shippingzone'] = LogisticsBranchShippingZone.objects.filter(
            logistics_branch=self.branch, status='Active')
        context['contractshipment'] = LogisticsCustomerContract.objects.get(
            id=self.kwargs['pk'])
        return context


class LogisticAdminLogisticsCustomerShipmentPaymentView(OperatorRequiredMixin, ListView):
    template_name = "logisticstemplates/settings/lalcshipmentpayment.html"
    paginate_by = 25
    context_object_name = "shipmentslist"

    def get_queryset(self):
        customer = LogisticsCustomer.objects.get(id=self.kwargs['pk'])
        shipments = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(Q(shipmentactivity__shipment_status='Shipment Delivered to the Receiver') | Q(shipmentactivity__shipment_status='Shipment Delivered to the Receiver in DC') | Q(shipmentactivity__shipment_status='Shipment Returned to the Sender'), shipmentactivity__id=F('latest'), customer=customer, parcel_origin=self.branch, service_payment_status=False)
        keyword = self.request.GET.get("keyword", "")
        if "keyword" in self.request.GET and keyword != "":
            shipments = shipments.filter(Q(tracking_code__icontains=keyword) | Q(parcel_type__icontains=keyword) | Q(cod_info__iexact=keyword) | Q(receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword))
        if "paginate_by" in self.request.GET:
            paginate_by = self.request.GET.get("paginate_by")
            try:
                self.paginate_by = int(paginate_by)
            except Exception as e:
                print(e, "LogisticAdminLogisticsCustomerShipmentPaymentView pagination")
                self.paginate_by = 25
        return shipments

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['customer'] = LogisticsCustomer.objects.get(id=self.kwargs['pk'])
        context['paginate_by'] = self.paginate_by
        return context


class LogisticsAdminLogisticsCustomerShipmentProcessPaymentView(OperatorRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        params = json.loads(request.GET.get("shipment_ids"))
        shipmentlist = Shipment.objects.filter(id__in=params)
        # for shipping charge to receive
        shipping_charge = shipmentlist.aggregate(total=Sum('shipping_charge'))
        cod_charge = shipmentlist.filter(cod_info="Cash on Delivery").exclude(Q(shipmentactivity__shipment_status='Shipment Returned to the Sender')).aggregate(total=Sum('cod_handling_charge'))
        rejection_charge = shipmentlist.filter(Q(shipmentactivity__shipment_status='Shipment Returned to the Sender')).aggregate(total=Sum('rejection_handling_charge'))
        extra_charge = shipmentlist.aggregate(total=Sum('extra_charge'))
        # final charge
        final_shipping_charge = shipping_charge['total'] if shipping_charge['total'] else 0
        final_cod_charge = cod_charge['total'] if cod_charge['total'] else 0
        final_rejection_charge = rejection_charge['total'] if rejection_charge['total'] else 0
        final_extra_charge = extra_charge['total'] if extra_charge['total'] else 0
        final_total_charge = final_shipping_charge + final_cod_charge + final_rejection_charge + final_extra_charge
        # for cod amount to return
        parcel_total = shipmentlist.filter(Q(shipmentactivity__shipment_status='Shipment Delivered to the Receiver') | 
        Q(shipmentactivity__shipment_status='Shipment Delivered to the Receiver in DC'), cod_info="Cash on Delivery").aggregate(total=Sum('cod_value'))
        parcel_total = parcel_total['total'] if parcel_total['total'] else 0

        net_amount = parcel_total - final_total_charge
        return JsonResponse({"status": "success", "total_charge": round(final_total_charge, 2), "shipment_ids": [params], "parcel_total": round(parcel_total, 2), "p_or_r": "Pay" if net_amount > 0 else "Receive", "net_charge": abs(net_amount)})

    def post(self, request, *args, **kwargs):
        shipment_ids = request.POST.get("shipment_ids")
        shipment_ids = shipment_ids.split(",")
        shipmentlist = Shipment.objects.filter(id__in=shipment_ids)
        # for shipping charge to receive
        shipping_charge = shipmentlist.aggregate(total=Sum('shipping_charge'))
        cod_charge = shipmentlist.filter(cod_info="Cash on Delivery").exclude(Q(shipmentactivity__shipment_status='Shipment Returned to the Sender')).aggregate(total=Sum('cod_handling_charge'))
        rejection_charge = shipmentlist.filter(Q(shipmentactivity__shipment_status='Shipment Returned to the Sender')).aggregate(total=Sum('rejection_handling_charge'))
        extra_charge = shipmentlist.aggregate(total=Sum('extra_charge'))
        # final charge
        final_shipping_charge = shipping_charge['total'] if shipping_charge['total'] else 0
        final_cod_charge = cod_charge['total'] if cod_charge['total'] else 0
        final_rejection_charge = rejection_charge['total'] if rejection_charge['total'] else 0
        final_extra_charge = extra_charge['total'] if extra_charge['total'] else 0
        final_total_charge = final_shipping_charge + final_cod_charge + final_rejection_charge + final_extra_charge
        # for cod amount to return
        parcel_total = shipmentlist.annotate(latest=Max('shipmentactivity__id')).filter(Q(shipmentactivity__shipment_status='Shipment Delivered to the Receiver') | 
        Q(shipmentactivity__shipment_status='Shipment Delivered to the Receiver in DC'), shipmentactivity__id=F('latest'), cod_info="Cash on Delivery").aggregate(total=Sum('cod_value'))
        parcel_total = parcel_total['total'] if parcel_total['total'] else 0
        # net amount
        net_amount = parcel_total - final_total_charge

        shipmentlist.update(service_payment_status=True)
        for shipment in shipmentlist:
            shipment.service_payment_amount = shipment.total_charge
            shipment.save()
        
        customer_payment_obj = CustomerPayment.objects.create(logistics_branch=self.branch, logistics_customer=shipmentlist.last().customer, total_shipping_charge=final_shipping_charge, total_cod_handling_charge=final_cod_charge, total_rejection_handling_charge=final_rejection_charge, discount_or_extra=final_extra_charge, total_service_charge=final_total_charge, total_cod_value=parcel_total, net_paid_or_received=net_amount, payment_confirmed_by=self.operator)
        customer_payment_obj.shipments.add(*shipmentlist)

        return JsonResponse({"status": "success"})


class LogisticAdminLogisticsCustomerShipmentProcessPaymentPrintView(OperatorRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        shipmentlist = Shipment.objects.filter(id__in=request.GET.get('shipment_ids').split(','))
        # for shipping charge to receive
        shipping_charge = shipmentlist.aggregate(total=Sum('shipping_charge'))
        cod_charge = shipmentlist.filter(cod_info="Cash on Delivery").exclude(Q(shipmentactivity__shipment_status='Shipment Returned to the Sender')).aggregate(total=Sum('cod_handling_charge'))
        rejection_charge = shipmentlist.filter(Q(shipmentactivity__shipment_status='Shipment Returned to the Sender')).aggregate(total=Sum('rejection_handling_charge'))
        extra_charge = shipmentlist.aggregate(total=Sum('extra_charge'))
        # final charge
        final_shipping_charge = shipping_charge['total'] if shipping_charge['total'] else 0
        final_cod_charge = cod_charge['total'] if cod_charge['total'] else 0
        final_rejection_charge = rejection_charge['total'] if rejection_charge['total'] else 0
        final_extra_charge = extra_charge['total'] if extra_charge['total'] else 0
        final_total_charge = final_shipping_charge + final_cod_charge + final_rejection_charge + final_extra_charge
        # for cod amount to return
        parcel_total = shipmentlist.annotate(latest=Max('shipmentactivity__id')).filter(Q(shipmentactivity__shipment_status='Shipment Delivered to the Receiver') | 
        Q(shipmentactivity__shipment_status='Shipment Delivered to the Receiver in DC'), shipmentactivity__id=F('latest'), cod_info="Cash on Delivery").aggregate(total=Sum('cod_value'))
        parcel_total = parcel_total['total'] if parcel_total['total'] else 0
        customer_name = shipmentlist.last().sender_name
        printed_at = timezone.localtime(timezone.now())
        return render(self.request, 'logisticstemplates/lalcshipmentpaymentprint.html', {
            'shipmentlist': shipmentlist, 'final_total_charge': final_total_charge, 'customer_name': customer_name,
            'printed_at': printed_at, 'total_cod_handover': parcel_total
        })
        return response


class LogisticAdminLogisticsCustomerShipmentProcessPaymentDownloadView(OperatorRequiredMixin, View):

    def get(self, request, *args, **kwargs):   
        params = request.GET.get('shipment_ids')
        print(params, "ppppppppppppppppppp")
        params = params.split(',')
        shipmentlist = Shipment.objects.filter(id__in=params)
        customer= shipmentlist.last().customer

        # for shipping charge to receive
        shipping_charge = shipmentlist.aggregate(total=Sum('shipping_charge'))
        cod_charge = shipmentlist.filter(cod_info="Cash on Delivery").exclude(Q(shipmentactivity__shipment_status='Shipment Returned to the Sender')).aggregate(total=Sum('cod_handling_charge'))
        rejection_charge = shipmentlist.filter(Q(shipmentactivity__shipment_status='Shipment Returned to the Sender')).aggregate(total=Sum('rejection_handling_charge'))
        extra_charge = shipmentlist.aggregate(total=Sum('extra_charge'))
        # final charge
        final_shipping_charge = shipping_charge['total'] if shipping_charge['total'] else 0
        final_cod_charge = cod_charge['total'] if cod_charge['total'] else 0
        final_rejection_charge = rejection_charge['total'] if rejection_charge['total'] else 0
        final_extra_charge = extra_charge['total'] if extra_charge['total'] else 0
        final_total_charge = final_shipping_charge + final_cod_charge + final_rejection_charge + final_extra_charge
        # for cod amount to return
        parcel_total = shipmentlist.annotate(latest=Max('shipmentactivity__id')).filter(Q(shipmentactivity__shipment_status='Shipment Delivered to the Receiver') | 
        Q(shipmentactivity__shipment_status='Shipment Delivered to the Receiver in DC'), shipmentactivity__id=F('latest'), cod_info="Cash on Delivery").aggregate(total=Sum('cod_value'))
        parcel_total = parcel_total['total'] if parcel_total['total'] else 0
        # net amount
        net_amount = parcel_total - final_total_charge
        printed_at = timezone.localtime(timezone.now())

        import csv
        response = HttpResponse(content_type='text/csv', headers={'Content-Disposition': 'attachment; filename="payment.csv"'},)
        writer = csv.writer(response)
        writer.writerow([])
        writer.writerow(['Payment By:' + "  " + str(self.company), '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Date:'+"  " + str(printed_at)])
        writer.writerow(['Payment To:' + "  " + str(customer)])
        writer.writerow(['', '', '', '', '', '', '', '', 'PAYMENT INFORMATION'])
        writer.writerow([])
        writer.writerow([])
        writer.writerow(['',
            'S.N', 'Shipment Code', 'Created At',
            'PickUp Information', 'DropOff Information', 'DropOff City',
            'Mode of Payment', 'Shipment Status', 'Shipping Charge',
            'Cod Handling Charge', 'Rejection Handling Charge', 'Service Charge',
            'Total COD Amount', 'Delivered Amount'])
        a = 1
        for ship in shipmentlist:
            pickup = str(ship.sender_name) + ' ' + \
                str(ship.sender_address)
            dropoff = str(ship.receiver_name) + '(' + str(ship.receiver_contact_number) + \
                ') ' + str(ship.receiver_address)
            if ship.cod_info == "Cash on Delivery" and ship.lshipmentstatus in ['Shipment Delivered to the Receiver', 'Shipment Delivered to the Receiver in DC']:
                cod_handling_charge = ship.cod_handling_charge
            else:
                cod_handling_charge = 0
            if ship.lshipmentstatus == "Shipment Returned to the Sender":
                rejection_handling_charge = ship.rejection_handling_charge
                delivery_amount = 0
            else:
                rejection_handling_charge = 0
                delivery_amount = ship.cod_value
            
            if ship.receiver_city:
                dropoff_city_title = ship.receiver_city.name
            else:
                dropoff_city_title = 'None'

            writer.writerow(['', a,
                             ship.tracking_code, ship.created_at.date(),
                             pickup, dropoff, dropoff_city_title,
                             ship.cod_info, ship.lshipmentstatus, ship.shipping_charge,
                             cod_handling_charge, rejection_handling_charge,
                             ship.total_charge,
                             ship.cod_value,
                             delivery_amount
                             ])
            a += 1
        writer.writerow(['','', '', '', '', '', '', '', '', '',
                         '', 'Total', final_total_charge,'', parcel_total])
        writer.writerow(['', '', '', '', '', '', '', '', '', '', '', 'Net Total',
                         str(parcel_total) + '-' + str(final_total_charge), '',  net_amount])
        writer.writerow(['', '', '', '', '', '', '', '', '', '','', '', '=' + str(net_amount)])
        
        
        return response


# -----------------------------------------------------------------------
# 3 Logistic Customers Views

class LogisticsCustomerDashboardView(CustomerRequiredMixin, TemplateView):
    template_name = 'customertemplates/logisticscustomerhome.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        notices = LogisticsNotice.objects.exclude(
            excluded_customers=self.request.user.logisticscustomer).filter(logistics_branch=self.request.user.logisticscustomer.creator_branch).order_by('-created_at')
        notice_list = list(notices[0:20])
        context["notices"] = notice_list
        return context

class LogisticCustomerProfileView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/customerprofile.html"

    def post(self, request):
        current_password = self.request.POST['currentpword']
        new_password = self.request.POST['npword']
        conform_password = self.request.POST["cpword"]
        if new_password != "" and conform_password != "" and current_password !="" and new_password == conform_password:
            user = authenticate(username=request.user.username, password=current_password)
            if user is not None:
                user.set_password(new_password)
                user.save()
                return JsonResponse({"message":"success"})
            else:
                print("error")
                return JsonResponse({"message":"error"})
        else:
            return JsonResponse({"message":"error"})

class LogisticCustomerProfileUpdateView(CustomerRequiredMixin, UpdateView):
    template_name = "customertemplates/customerprofileupdate.html"
    model = LogisticsCustomer
    form_class = CustomerProfileUpdate
    success_url = reverse_lazy('lmsapp:customerprofileview')
    success_message = "Profile Updated Successfully"

# shipment management

class CustomerShipmentListView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/customershipmentlist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        status = self.request.GET.get('status')
        keyword = self.request.GET.get('keyword')
        all_shipments = Shipment.objects.annotate(latest=Max('shipmentactivity__id')).filter(
            customer=self.customer)
        if status == 'requested':
            shipmentlist = all_shipments.filter(
                shipmentactivity__id=F('latest'), shipmentactivity__shipment_status="Shipment Requested").order_by('-id')

        elif status == 'processing':
            shipmentlist = all_shipments.filter(
                shipmentactivity__id=F('latest'), shipmentactivity__shipment_status__in=[
                    "Rider Assigned for Pickup", "Rider Picked up the Shipment", "Shipment Arrived at Warehouse", "Rider Assigned for Dispatch to the Receiver", "Rider Assigned for Dispatch to DC"]).order_by('-id')
        elif status == 'dispatched':
            shipmentlist = all_shipments.filter(
                shipmentactivity__id=F('latest'), shipmentactivity__shipment_status__in=[
                    "Rider Picked up the Shipment for Dispatch", "Shipment Delivered to the DC", "Rider Assigned for Dispatch in DC", "Shipment Dispatched to the Receiver in DC"]).order_by('-id')
        elif status == 'delivered':
            shipmentlist = all_shipments.filter(
                shipmentactivity__id=F('latest'), shipmentactivity__shipment_status__in=[
                    "Shipment Delivered to the Receiver", "Shipment Delivered to the Receiver in DC"]).order_by('-id')
        elif status == 'rejected':
            shipmentlist = all_shipments.filter(
                shipmentactivity__id=F('latest'), shipmentactivity__shipment_status__in=[
                    "Shipment Rejected", "Shipment Rejected in DC"]).order_by('-id')
        elif status == 'returned':
            shipmentlist = all_shipments.filter(
                shipmentactivity__id=F('latest'), shipmentactivity__shipment_status__in=[
                    "Shipment Returned to Logistics", "Shipment Ready to Return by DC", "Rider Assigned for Return to Logistics by DC", "Shipment Returned to the Logistics by DC", "Ready to Return to Sender", "Rider Assigned for Returning to the Sender", "Rider Picked up the Shipment for Return", "Shipment Returned to the Sender"]).order_by('-id')
        elif status == 'canceled':
            shipmentlist = all_shipments.filter(
                shipmentactivity__id=F('latest'),
                shipmentactivity__shipment_status="Shipment Canceled").order_by('-id')
        else:
            shipmentlist = all_shipments
        context["status"] = status

        if keyword:
            shipmentlist = shipmentlist.filter(Q(tracking_code__icontains=keyword) | Q(parcel_origin__logistics_company__company_name__icontains=keyword) | Q(
                receiver_name__icontains=keyword) | Q(receiver_city__name__icontains=keyword) | Q(receiver_address__icontains=keyword) | Q(receiver_contact_number__icontains=keyword))
            context['keyword'] = keyword

        daterange = self.request.GET.get("date")
        if daterange:
            date = daterange.split(' - ')
            startdate = date[0]
            enddate = date[1]
            startdate_splited = startdate.split('/')
            startdate_reverser = [startdate_splited[2],
                                  startdate_splited[0], startdate_splited[1]]
            listToStrStart = '-'.join([str(elem)
                                      for elem in startdate_reverser])
            enddate_splited = enddate.split('/')
            enddate_reverser = [enddate_splited[2],
                                enddate_splited[0], enddate_splited[1]]
            listToStrEnd = '-'.join([str(elem) for elem in enddate_reverser])
            date_start = parse_date(listToStrStart)
            date_end = parse_date(listToStrEnd)
            shipmentlist = shipmentlist.filter(
                created_at__date__range=[date_start, date_end])
            context['startdate'] = startdate
            context['enddate'] = enddate
            context['date'] = daterange

        page = self.request.GET.get('page', 1)
        paginator = Paginator(shipmentlist, 25)
        try:
            results = paginator.page(page)
        except PageNotAnInteger:
            results = paginator.page(1)
        except EmptyPage:
            results = paginator.page(paginator.num_pages)
        context['shipmentlist'] = results
        return context


class CustomerShipmentDetailView(CustomerRequiredMixin, DetailView):
    template_name = 'customertemplates/customershipmentdetail.html'
    model = Shipment
    context_object_name = "shipment"


class CustomerShipmentCreateView(CustomerRequiredMixin, CreateView):
    template_name = 'customertemplates/customershipmentcreate.html'
    form_class = CustomerShipmentCreateForm
    success_message = "Your shipment has been created"

    def form_valid(self, form):
        # sender information
        form.instance.customer = self.customer
        form.instance.sender_name = self.customer.full_name
        form.instance.sender_city = self.customer.city
        form.instance.sender_address = self.customer.address
        form.instance.sender_contact_number = self.customer.contact_number
        # Shipment information 
        branch_id = form.cleaned_data.get('branch_id')
        city_id = form.cleaned_data.get('city_id')
        branch = LogisticsBranch.objects.get(id=branch_id)
        shipping_zone = LogisticsBranchShippingZone.objects.filter(
            logistics_branch__id=branch_id, cities__id=city_id).first()
        delivery_type = shipping_zone.shippingzonedeliverytype_set.all().first()

        weight = form.cleaned_data.get('weight')
        if weight == "" or weight is None:
            weight = 200
        form.instance.weight = weight
        if delivery_type is not None:
            form.instance.delivery_type = delivery_type
            form.instance.shipping_charge = delivery_type.shipping_charge_per_kg
        else:
            form.instance.shipping_charge = 0
        form.instance.extra_charge = 0
        # amount information 
        if form.instance.cod_info == "Cash on Delivery":
            form.instance.cod_handling_charge = delivery_type.cod_handling_charge / 100 * form.instance.cod_value
            # form.instance.cod_handling_charge = delivery_type.cod_handling_charge
            form.instance.rejection_handling_charge = delivery_type.rejection_handling_charge  / 100 * form.instance.cod_value
            # form.instance.rejection_handling_charge = delivery_type.rejection_handling_charge
        else:
            form.instance.cod_handling_charge = 0
            form.instance.rejection_handling_charge = form.instance.shipping_charge / 2
        
        unit = form.cleaned_data.get('length_unit', "Cm")
        form.instance.parcel_origin = branch
        form.instance.status = 'Active'
        form.instance.breadth_unit = unit
        form.instance.height_unit = unit
        shipment = form.save()
        shipment.tracking_code = "tracking_" + str(shipment.id)
        shipment.save()
        ShipmentActivity.objects.create(status='Active', shipment=shipment,
                                        shipment_status='Shipment Requested', activity='Shipment created by customer')
        shipment.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)

    def get_success_url(self, **kwargs):
        return reverse_lazy("lmsapp:customershipmentdetail", kwargs={'pk': self.object.id})


class CustomerGetFilteredLogisticsView(CustomerRequiredMixin, View):
    def get(self, request):
        city_id = self.request.GET.get('selected', None)
        print(city_id,"ddddddddddd")
        branch_list = self.customer.connected_logistics.filter(
            branch_coverage=self.customer.city, logisticsbranchshippingzone__cities__id=city_id)
        return render(request, 'customertemplates/customerfilteredlogisticbranch.html', {"branches": branch_list, 'city_id': city_id})


class CustomerLogisticsBranchCost(CustomerGetFilteredLogisticsView):
    def get(self, request):
        branchid = self.request.GET.get('selected', '')
        city_id = self.request.GET.get('cityId', '')
        zone = LogisticsBranchShippingZone.objects.filter(
            logistics_branch=branchid)
        shippingzonedeliverytype = list(ShippingZoneDeliveryType.objects.filter(shippng_zone__logistics_branch__id=self.request.GET.get(
            'selected', ''), shippng_zone__cities__id=city_id).values("shipping_charge_per_kg", "additional_shipping_charge_per_kg", "cod_handling_charge", "rejection_handling_charge"))
        return JsonResponse({"message": "success", 'shippingzonedeliverytype': shippingzonedeliverytype})


class CustomerBulkShipmentUpload(CustomerRequiredMixin, FormView):
    form_class = CustomerBulkShipmentUploadForm
    template_name = "customertemplates/customerbulkshipmentcreate.html"
    success_url = reverse_lazy('lmsapp:customerbulkshipmentupload')

    def form_valid(self, form):
        filename = form.cleaned_data.get('file')
        print(type(filename))
        if not filename.name.endswith('.csv'):
            print('not csv')
        else:
            print('csv ho')

        return super().form_valid(form)


# connected logistics

class CustomerConnectedLogisticsView(CustomerRequiredMixin, ListView):
    template_name = "customertemplates/customerconnectedlogistics.html"
    queryset = LogisticsCustomer.objects.all()
    context_object_name = 'conloglist'
# def CustomerBulkShipmentUpload(request):

    # def get(self, request):
    #     x=request.FILES['csvfile']
    #     print(x)
    #     return render(request, "customertemplates/customerbulkshipmentcreate.html")

    # def post(self, request):
    #     # x=request.POST['csvfile']

    #     return render(request, "customertemplates/customerbulkshipmentcreate.html")


class CustomerConnectedLogisticsView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/customerconnectedlogistics.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["conloglist"] = LogisticsCustomer.objects.filter(
            user=self.user).last().connected_logistics.all()
        return context


class CustomerQuotationListView(CustomerRequiredMixin, ListView):
    template_name = "customertemplates/customerquotationlist.html"
    queryset = LogisticCustomerZonalContract.objects.all()
    context_object_name = 'customerquotationlist'

    def get_queryset(self, **kwargs):

        queryset = LogisticsCustomerContract.objects.filter(
            second_party=self.customer, is_signed=False, status='Active')
        customerquotationlist = []

        for i in queryset:
            if i.logisticcustomerzonalcontract_set.all():
                customerquotationlist.append(i)
        return customerquotationlist


class CustomerQuotationDetailView(CustomerRequiredMixin, DetailView):
    template_name = "customertemplates/customerquotationdetail.html"
    model = LogisticsCustomerContract
    context_object_name = "customerquotationdetail"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["zonalcontract"] = LogisticCustomerZonalContract.objects.filter(
            logistic_customer_contract=self.kwargs['pk'])
        status1 = self.request.GET.get('confirm', 0)
        status2 = self.request.GET.get('reject', 0)

        quot = LogisticsCustomerContract.objects.get(id=self.kwargs['pk'])
        context["quotcontext"] = quot

        if status1 == 'confirm':
            quot.is_signed = True
            quot.save()
        elif status2 == 'reject':
            quot.status = "Disabled"
            quot.is_signed = False
            quot.save()
        else:
            pass
        return context


class CustomerContractsStatusListView(CustomerRequiredMixin, ListView):
    template_name = "customertemplates/customercontractsstatuslist.html"
    queryset = LogisticsCustomerContract.objects.all()
    context_object_name = 'customercontractstatus'

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset1 = LogisticsCustomerContract.objects.filter(
            second_party=self.customer, is_signed=True, status='Active')
        queryset2 = LogisticsCustomerContract.objects.filter(
            second_party=self.customer, is_signed=False, status='Disabled')

        queryset = queryset1.union(queryset2)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["signed"] = LogisticsCustomerContract.objects.filter(
            second_party=self.customer, is_signed=True, status='Active')
        context["rejected"] = LogisticsCustomerContract.objects.filter(
            second_party=self.customer, is_signed=False, status='Disabled')

        return context


class CustomerSupportTicketRaiseListView(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/customerticketlist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["ticketlist"] = CustomerSupportTicket.objects.filter(
            raised_by=self.request.user.logisticscustomer).order_by('-created_at')
        context["ticketexists"] = CustomerSupportTicket.objects.filter(
            raised_by=self.request.user.logisticscustomer).last()
        return context


class CustomerSupportTicketCreateView(CustomerRequiredMixin, CreateView):
    template_name = 'customertemplates/customerticketcreate.html'
    form_class = CustomerTicketRaiseForm
    success_message = 'Ticket Created We will Contact You Soon !!'
    success_url = reverse_lazy('lmsapp:customerticketlist')

    def get_form_kwargs(self):
        kwargs = super(CustomerSupportTicketCreateView, self).get_form_kwargs()
        kwargs['user'] = self.request.user.logisticscustomer
        return kwargs

    def form_valid(self, form):
        customer = self.request.user.logisticscustomer
        ticket = CustomerSupportTicket.objects.filter(
            raised_by=customer).last()
        if ticket is not None and ticket.ticket_status == 'Solved':
            form.instance.raised_by = self.request.user.logisticscustomer
            # form.instance.raised_to = self.request.user.logisticscustomer.creator_branch
            form.instance.status = 'Pending'
            form.instance.ticket_status = 'Open'
            ticket = form.save()
            content_type = ContentType.objects.get(
                app_label='umapp', model='logisticsbranch'
            )
            branch_id = self.request.user.logisticscustomer.creator_branch.id
            CustomerTicketFollowup.objects.create(
                ticket=ticket, followup_by_type=content_type, followup_by_id=branch_id, followup_message='ticket raised...!')
            return redirect('lmsapp:customerticketlist')
        elif ticket is None:
            form.instance.raised_by = self.request.user.logisticscustomer
            # form.instance.raised_to = self.request.user.logisticscustomer.creator_branch
            form.instance.status = 'Pending'
            form.instance.ticket_status = 'Open'
            ticket = form.save()
            content_type = ContentType.objects.get(
                app_label='umapp', model='logisticsbranch'
            )
            branch_id = self.branch.id
            CustomerTicketFollowup.objects.create(
                ticket=ticket, followup_by_type=content_type, followup_by_id=branch_id, followup_message='ticket raised...!')
            return redirect('lmsapp:customerticketlist')
        else:
            messages.warning(self.request, 'Ticket not solved yet!!')
            return redirect('lmsapp:customerticketlist')


class CustomerSupportTicketDetail(CustomerRequiredMixin, DetailView):
    template_name = 'customertemplates/customerticketdetail.html'
    model = CustomerSupportTicket
    context_object_name = 'custometicketdetail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['messageform'] = CustomerTicketFollowupForm
        status = self.request.GET.get('Active')
        ticket_id = self.request.GET.get('id')
        ticket = CustomerSupportTicket.objects.filter(id=ticket_id)
        content_type = ContentType.objects.get(
            app_label='umapp', model='logisticsbranch'
        )
        if status == 'Active':
            messages = CustomerTicketFollowup.objects.filter(
                status='Pending', followup_by_type=content_type)
            for message in messages:
                message.status = 'Active'
                message.save()
        else:
            pass
        return context


class CustomerSupportAjaxTicketDetail(CustomerRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        text = request.GET.get('text', 0)
        request_from = request.GET.get('request_from', 0)
        ticket_id = self.kwargs['pk']
        ticket = CustomerSupportTicket.objects.get(id=ticket_id)
        if request_from == 'customer':
            content_type = ContentType.objects.get(
                app_label='umapp', model='logisticscustomer'
            )
            customer = self.request.user.logisticscustomer
            try:
                CustomerTicketFollowup.objects.create(
                    status='Pending',
                    ticket=ticket, followup_by_type=content_type, followup_by_id=customer.id, followup_message=text)
            except Exception as e:
                print(e)
        return JsonResponse({'message': 'success', 'text': text})


class CustomerNoticeDetailView(CustomerRequiredMixin, DetailView):
    template_name = "customertemplates/customernoticedetail.html"
    model = LogisticsNotice
    context_object_name = 'noticedetail'


class CustomerLogisticCompaniesListView(CustomerRequiredMixin, ListView):
    template_name = 'customertemplates/customerlogisticcompanieslist.html'
    queryset = LogisticsCompany.objects.filter(status="Active")
    context_object_name = "logisticcompanies"
    paginate_by = "10"

    def get_queryset(self):
        queryset = super().get_queryset()
        keyword = self.request.GET.get('keyword')
        page = self.request.GET.get('page', 1)
        if keyword is not None and keyword != "":
            new_queryset = queryset.filter(
                logisticsbranch__branch_coverage__name__icontains=keyword).distinct()
        else:
            new_queryset = queryset
        return new_queryset


class CustomerLogisticCompaniessDetailView(CustomerRequiredMixin, DetailView):
    template_name = "customertemplates/customerlogisticcompanydetail.html"
    model = LogisticsCompany
    context_object_name = 'logisticcompanydetail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['branch'] = LogisticsCustomerContract.objects.filter(
            second_party=self.customer, first_party__in=self.object.logisticsbranch_set.all()).values('first_party__id')
        return context


class CustomerLogisticCompanyBranchList(CustomerRequiredMixin, TemplateView):
    template_name = "customertemplates/customerlogisticcompanybranchlist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['companylist'] = LogisticsCompany.objects.all()
        return context


class CustomerRequestContractView(CustomerRequiredMixin, View):
    def post(self, requset, *args, **kwargs):
        branch_obj = LogisticsBranch.objects.get(
            id=self.request.POST['branch_id'])
        # print(branch_obj)
        # print(self.customer)
        if LogisticsCustomerContract.objects.filter(first_party=branch_obj, second_party=self.customer, status="Active").exists():
            messages.error(self.request, 'Contract Aready Exists!!')
        else:
            LogisticsCustomerContract.objects.create(
                first_party=branch_obj, second_party=self.customer, status='Active')
            self.customer.connected_logistics.add(branch_obj)
            messages.success(self.request, 'Contract Requested Successfully!!')
        return JsonResponse({
            "success": 'message'
        })


class CustomerConnectLogisticView(CustomerRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        branch_obj = LogisticsBranch.objects.get(
            id=self.request.POST['branch_id2'])
        if LogisticsCustomer.objects.filter(user=self.user ,connected_logistics=branch_obj).exists():
            messages.error(request, "You are already connected to this branch")
        else:
            self.customer.connected_logistics.add(branch_obj)
            messages.success(request, "You are connected to this branch")
        return JsonResponse({
            'message': 'success'
        })


class CustomerRequestPaymentView(CustomerRequiredMixin, FormView):
    template_name = "customertemplates/customerrequestpayment.html"
    form_class = CustomerPaymentRequestForm
    success_url = reverse_lazy("lmsapp:lcdashboard")

    def get_form_kwargs(self):
        kwargs = super(CustomerRequestPaymentView, self).get_form_kwargs()
        kwargs['customer'] = self.user
        return kwargs


class CustomerPaymentBranchListView(CustomerRequiredMixin, ListView):
    template_name = 'customertemplates/customerpaymentlist.html'
    model = LogisticsCustomer

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["branches"] = self.customer.connected_logistics.all()
        return context


class CustomerBranchPaymentDetailView(CustomerRequiredMixin, DetailView):
    template_name = 'customertemplates/customerpaymentdetail.html'
    model = LogisticsBranch
    context_object_name = 'branchdetail'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["payment_list"] = CustomerPayment.objects.filter(
            logistics_customer=self.customer, logistics_branch=self.object)
        return context


class CustomerRequestPaymentCreateView(CustomerRequiredMixin, FormView):
    template_name = "customertemplates/customerrequestpayment.html"
    form_class = CustomerPaymentRequestForm
    success_message = 'Payment Requested Successfully !!'
    success_url = reverse_lazy('lmsapp:customerticketlist')

    def get_form_kwargs(self):
        kwargs = super(CustomerRequestPaymentCreateView,
                       self).get_form_kwargs()
        customer = self.request.user.logisticscustomer
        kwargs['user'] = customer
        return kwargs


class OperatorCustomerPaymentListView(OperatorRequiredMixin, ListView):
    template_name = 'logisticstemplates/opcustomerpaymentlist.html'
    paginate_by = 5
    queryset = CustomerPayment.objects.all()
    context_object_name = "customerpaymentlist"

    def get_queryset(self, **kwargs):
        # queryset = LogisticsCustomer.objects.filter(creator_branch = self.branch)
        queryset = CustomerPayment.objects.filter(logistics_branch=self.branch)
        return queryset


class OperatorCustomerPaymentDetailView(OperatorRequiredMixin, DetailView):
    template_name = "logisticstemplates/opcustomerpaymentdetail.html"
    model = CustomerPayment
    context_object_name = "customerpaymentdetail"
    






# 4 Client Views
class ClientMixin(SuccessMessageMixin, object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class ClientHomeView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clienthome.html"
