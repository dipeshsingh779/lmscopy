from django.urls import path
from .views import *
from . import views


app_name = "lmsapp"
urlpatterns = [

    # 1 System Operator Views


    # 2 Logistics Operator Views
    path("operator/dashboard/", LogisticsAdminHomeView.as_view(), name="lahome"),
    path("operator/shipments/list/",
         LogisticsAdminShipmentListView.as_view(), name="lashipmentlist"),
    path("operator/shipments/new-create/",
         LogisticsAdminShipmentCreateView.as_view(), name="lashipmentcreate"),
    path("operator/shipments/update-<int:pk>/",
         LogisticsAdminShipmentUpdateView.as_view(), name="lashipmentupdate"),
    path("operator/shipments/detail-<int:pk>/",
         LogisticsAdminShipmentDetailView.as_view(), name="lashipmentdetail"),

    path("operator/bundles/list/",
         LogisticsAdminBundleListView.as_view(), name="labundlelist"),
    path("operator/bundle/update-<int:pk>/",
         LogisticsAdminBundleUpdateView.as_view(), name="labundleupdate"),
    path("operator/bundles/incoming-list/",
         LogisticsAdminIncomingBundleListView.as_view(), name="laincomingbundlelist"),

    path("operator/bundles/cmp/laloadbundlelist/",
         CmpLogisticsAdminLoadBundleListView.as_view(), name="cmplaloadbundlelist"),
    path("operator/bundles/cmp/labundleinfo-<int:pk>/",
         CmpLogisitcsAdminLoadBundleInfoView.as_view(), name="cmplabundleinfo"),
    path("operator/bundles/cmp/laloadincomingbundlelist/",
         CmpLogisticsAdminLoadIncomingBundleListView.as_view(), name="cmplaloadincomingbundlelist"),

    path("operator/bundles/ajax/labundlechangestatus/",
         AjaxLogisticsAdminBundleChangeStatusView.as_view(), name="ajaxlabundlechangestatus"),
    path("operator/shipments-bundle/ajax/shipment-charge/<attribute>/update/",
         AjaxLogisticsAdminShipmentBundleShipmentChargeUpdateView.as_view(), name="ajaxlashipmentbundleshipmentchargeupdate"),
    path("operator/shipments/ajax/labundlechangestatus/",
         AjaxLogisticsAdminBundleShipmentChangeStatusView.as_view(), name="ajaxlabundleshipmentchangestatus"),
    # shipment bundle create modal
    path("operator/shipment-bundle/ajax/destination-option-modal/",
         AjaxLogisticsAdminShipmentBundleDestinationOptionModalView.as_view(), name="ajaxlashipmentbundledestinationoptionmodal"),

    path("operator/shipments/cmp/laloadshipmentlist/",
         CmpLogisticsAdminLoadShipmentListView.as_view(), name="cmplaloadshipmentlist"),
    path("operator/shipments/cmp/lashipmentinfo-<int:pk>/",
         CmpLogisitcsAdminLoadShipmentInfoView.as_view(), name="cmplashipmentinfo"),
    path("operator/shipments/cmp/laloadenquirylist/",
         CmpLogisticsAdminLoadEnquiryListView.as_view(), name="cmplaloadenquirylist"),

    path("operator/shipments/ajax/lashipmentactivity-<int:pk>/",
         AjaxLogisticsAdminShipmentActivityCreateView.as_view(), name="ajaxlashipmentactivitycreate"),
    path("operator/shipments/ajax/lashipmentchangestatus/",
         AjaxLogisticsAdminShipmentChangeStatusView.as_view(), name="ajaxlashipmentchangestatus"),
    path("operator/shipments/ajax/shipment-charge/<attribute>/update/",
         AjaxLogisticsAdminShipmentChargeUpdateView.as_view(), name="ajaxlashipmentchargeupdate"),

    path("operator/shipment/shipping-label/<int:pk>/",
         LogisticAdminShipmentShippingLabelView.as_view(), name="lashipmentshippinglabel"),
    # path("operator/shipment/bulk-shipping-label/",
    #      LogisticAdminShipmentBulkShippingLabelView.as_view(), name-"lashipmentbulkshipmentlabel"),
    path("operator/shipment/bulk-shipping-label/",
         LogisticAdminShipmentShippingLabelView.as_view(), name="lashipmentbulkshipmentlabel"),

    path("settings/", LogisticsAdminSettingsView.as_view(), name="lasettings"),

    path('operator/branch-operator/customer/ticket/list/',
         CustomerSupportTicketListView.as_view(), name='operatorcustomerticketlist'),

    path('operator/branch-operator/customer/ticket/detail/<int:pk>/',
         CustomerSupportTicketDetailView.as_view(), name='operatorcustomerticketdetail'),

    path('operator/branch-operator/customer/ticket/update/<int:pk>/',
         CustomerSupportTicketUpdateView.as_view(), name='operatorcustomerticketupdate'),

    path('operator/branch-operator/customer/ticket/support/ajax/<int:pk>/',
         CustomerSupportTicketAjaxView.as_view(), name='ticketraiseajax'),

    path('operator/branch-operator/customer/ticket/status/list/',
         CustomerTicketNoticeStatusList.as_view(), name='ticketstatuslist'),
    path('operator/branch-operator/customer/message/status/list/',
         CustomerTicketFollowUpdMessageStatusList.as_view(), name='messagestatuslist'),
    # path('operator/branch-operator/customer/ticket/support/ajax/list/', CustomerSupportTicketListAjaxView.as_view(), name='operatorcustomerticketajaxlist'),

    path('operator/branch-operator/customer/notice/list/',
         LogisticsNoticeListView.as_view(), name='opnoticelist'),
    path('operator/branch-operator/customer/notice/create/',
         LogisticsNoticeCreateView.as_view(), name='opnoticecreate'),
    path('operator/branch-operator/customer/notice/update/<int:pk>/',
         LogisticsNoticeUpdateView.as_view(), name='opnoticeupdate'),
    path('operator/branch-operator/customer/notice/detail/<int:pk>/',
         LogisticsNoticeDetailView.as_view(), name='opnoticedetail'),

    path('operator/logistic-customer/ajax/list/',
         AjaxLALogisticCustomerListView.as_view(), name="ajaxlalogisticcustomerlist"),

    path('operator/logistic-customer/<int:pk>/shipments-list/payment/',
         LogisticsAdminLogisticsCustomerShipmentPaymentListView.as_view(), name="lalcshipmentpaymentlist"),
    

    path('operator/logistic-customer/quotation/request/list/',
         LogisticsCustomerRequestQuotationFromBranch.as_view(), name="customerrequestquotation"),
    path('operator/logistic-customer/quotation/request/approved/list/',
         LogisticsCustomerRequestQuotationFromBranchApproved.as_view(), name="customerrequestquotationapproved"),
    path('operator/logistic-customer/signed/quotation/request/approved/list/',
         LogisticsCustomerSignedRequestQuotationFromBranchApproved.as_view(), name="customerrequestsignedquotationapproved"),
    path('operator/logistic-customer/quotation/request/create/<int:pk>/',
         LogisticsCustomerRequestQuotationFromBranchCreateView.as_view(), name="customerrequestquotationcreate"),
    path('operator/logistic-customer/quotation/request/update/<int:pk>/',
         LogisticsCustomerRequestQuotationFromBranchUpdateView.as_view(), name="customerrequestquotationupdate"),
    path('operator/logistic-customer/quotation/request/detail/<int:pk>/',
         LogisticsCustomerRequestQuotationFromBranchDetail.as_view(), name="customerrequestquotationdetail"),

    path('operator/logistic-customer/quotation/request/ajax/',
         LogisticsCustomerRequestQuotationFromBranchApprovedAjaxView.as_view(), name="customerrequestquotationajax"),
    path('operator/logistic-customer/quotation/request/ajax2/',
         LogisticsCustomerRequestQuotationFromBranchApprovedAjaxView2.as_view(), name="customerrequestquotationajax2"),

    # Customer Payment
    path('operator/payment/lc-<int:pk>-payment/',
         LogisticAdminLogisticsCustomerShipmentPaymentView.as_view(), name="lalcshipmentpayment"),
    path('operator/logistic-customer/shipment-process-payment/',
         LogisticsAdminLogisticsCustomerShipmentProcessPaymentView.as_view(), name="lalcshipmentprocesspayment"),
    path('operator/customer-payment/list/',
         OperatorCustomerPaymentListView.as_view(), name="opcustomerpaymentlist"),
    path('operator/customer-payment/detail/<int:pk>/',
         OperatorCustomerPaymentDetailView.as_view(), name="opcustomerpaymentdetail"),
    path('operator/logistic-customer/shipment-process-payment/print/',
         LogisticAdminLogisticsCustomerShipmentProcessPaymentPrintView.as_view(), name="lalcshipmentprocesspaymentprint"),
    path('operator/logistic-customer/shipment-process-payment/download/',           
         LogisticAdminLogisticsCustomerShipmentProcessPaymentDownloadView.as_view(), name="lalcshipmentprocesspaymentdownload"),

    # logistics admin sidebar
    path("operator/cmp/sidebardetail/",
         CmpLogisticsAdminLoadSidebarView.as_view(), name="cmplasidebardetail"),


    # --------------------------------------------
    # Logistics customer panel urls
    # 3 Logistic Customers Views

    path("customer/dashboard/",
         LogisticsCustomerDashboardView.as_view(), name="lcdashboard"),
     path("customer/profile/", LogisticCustomerProfileView.as_view(), name="customerprofileview"),
     path("customer/profile/update/<int:pk>/", LogisticCustomerProfileUpdateView.as_view(), name="customerprofilupdate"),

    # shipment management
    path('customer/shipment-list/', CustomerShipmentListView.as_view(),
         name='customershipmentlist'),
    path('customer/shipment-<int:pk>-detail/',
         CustomerShipmentDetailView.as_view(), name='customershipmentdetail'),
    path('customer/request-shipment/',
         CustomerShipmentCreateView.as_view(), name='customershipmentcreate'),
    path('customer/get-filtered-branches/',
         CustomerGetFilteredLogisticsView.as_view(), name='getfilteredlogistics'),
    path('customer/logsitic-company/cost/',
         CustomerLogisticsBranchCost.as_view(), name='branchcost'),


    path('customer/bulk-shipment/upload/',
         CustomerBulkShipmentUpload.as_view(), name='customerbulkshipmentupload'),

    # tickets
    path('customer/support/ticket/list/',
         CustomerSupportTicketRaiseListView.as_view(), name='customerticketlist'),
    path('customer/support/ticket/create/',
         CustomerSupportTicketCreateView.as_view(), name='customerticketcreate'),
    path('customer/support/ticket/detail/<int:pk>/',
         CustomerSupportTicketDetail.as_view(), name='customerticketdetail'),
    path('customer/support/ticket/ajax/detail/<int:pk>/',
         CustomerSupportAjaxTicketDetail.as_view(), name='customerajaxticketdetail'),
    # notice
    path('customer/notice/detail/<int:pk>/',
         CustomerNoticeDetailView.as_view(), name='customernoticedetail'),



    # payment
    path('customer/request/payment/',
         CustomerRequestPaymentCreateView.as_view(), name='customerpaymentrequest'),
    path('customer/payment/list/',
         CustomerPaymentBranchListView.as_view(), name='customerpaymentlist'),
    path('customer/payment/detail/<int:pk>/',
         CustomerBranchPaymentDetailView.as_view(), name='customerpaymentdetail'),



    # logistics company in customer pannel
    path("customer/logistic/companies/list/",
         CustomerLogisticCompaniesListView.as_view(), name='customerlogisticcompanieslist'),
    path('customer/logistic/company/<int:pk>/detail/',
         CustomerLogisticCompaniessDetailView.as_view(), name='customerlogisticcompanydetail'),
    path("customer/logistic/company/branch/list/",
         CustomerLogisticCompanyBranchList.as_view(), name="customerlogisticbranchlist"),
    path('customer/request/contract/',
         CustomerRequestContractView.as_view(), name='customerrequestcontract'),
    path('customer/connected-logistics/',
         CustomerConnectedLogisticsView.as_view(), name='customerconnectedlogistics'),
    path("customer/connect/logistics/",
         CustomerConnectLogisticView.as_view(), name="customerconnectlogistics"),
    path("cusotmer/request/payment/",
         CustomerRequestPaymentView.as_view(), name="customerrequestpayment"),
    # path("ajax/customer-request-payment/shipmentlist/", AjaxCustomerPaymentRequestView.as_view(), name = 'ajaxcustomerpaymentrequestshipmentlist'),
    # quotation
    path('customer/quotation-list/', CustomerQuotationListView.as_view(),
         name='customerquotationlist'),
    path('customer/quotation-detail/<int:pk>',
         CustomerQuotationDetailView.as_view(), name='customerquotationdetail'),
    path('customer/contracts-status-list/',
         CustomerContractsStatusListView.as_view(), name='customercontractstatus'),

    # --------------------------------------------
    # Logistics Systemclient panel urls

    path("", ClientHomeView.as_view(), name="clienthome"),





]
