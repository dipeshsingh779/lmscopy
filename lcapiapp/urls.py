from rest_framework_simplejwt.views import TokenRefreshView
from django.urls import path
from .views import *


app_name = "lcapiapp"
urlpatterns = [
    #get-token pair for login credentials
    path("get-token-pair/", CustomerGetTokenPairView.as_view(), name="customergettokenpair"),
    path('refresh-token/', TokenRefreshView.as_view(), name='customerrefreshtoken'),

    #Customer Managemment
    path("profile/", CustomerProfileAPIView.as_view(), name="customerprofileapi"),
    path("registration/", CustomerRegistrationAPIView.as_view(), name="customer-register"),
    path("change/password/", ChangeCustomerPasswordView.as_view(), name='customer-change-password'),

    #Customer Reset Password
    path('request-reset-email/', RequestPasswordResetEmail.as_view(),
         name="request-reset-email"),
    path('password-reset/check/token/<uidb64>/<token>/',
         PasswordTokenCheckAPI.as_view(), name='token-check'),
    path('password-reset-complete/', SetNewPasswordAPIView.as_view(),
         name='password-reset-complete'),

    #Customer Connected Logistics
    path('connected/logistics/', LogisticsCustomerConnectedLogisticsView.as_view(), name='customerconnectedlogistics'),

    #customer Support Ticket
    path('followup/ticket/', CustomerSupportTicketListView.as_view(), name='customerfollowupticket'),
    path('followup/ticket/<int:pk>/', CustomerSupportTicketView.as_view(), name='customerfollowupticketdetail'),


    #logistics company
    path('logistics/company/', LogisticsCompanyListApiView.as_view(), name='logisticscompany'),
    path('logistics/company/<int:pk>/', LogisticsCompanyDetailApiView.as_view(), name='logisticscompanydetail'),


    #Connect Logistics branch as service provider to current user
    path('connected/logistics/', ConnectedLogisticsListApiView.as_view(), name='connectelogisticslist'),#for list of connected branches
    path('connected/logistics/<int:pk>/', ConnectedLogisticsDetailApiView.as_view(), name='connectedlogisticsdetail'), #for detail of connected branch
    path('connect/logistics/<int:company_pk>/<int:branch_pk>/', ConnectLogisticsBranchFromCompanyDetailApiView.as_view(), name='connectnewlogistcs'), #to connect new logistics for customer


    #request for contract
    path('request/contract/<int:company_pk>/<int:branch_pk>/', LogisticsCustomerReqestForContractApiView.as_view(), name='requestnewcontract'),
    path('quotation/', LogisticsCustomerQuotationListApiView.as_view(), name="quotationlist"),
    path('quotation/<int:pk>/', LogisticsCustomerQuotationDetailApiView.as_view(), name="quotationdetail"),
    path('accept/quotation/<int:contract_id>/', LogisticsCustomerAcceptQuotationApiView.as_view(), name='acceptcontract'),
    path('reject/quotation/<int:contract_id>/', LogisticsCustomerRejectQuotationApiView.as_view(), name='rejectcontract'),


    #Customer Request for Single Shipment
    path('get-related-branches/<int:pk>/',GetRelatedBranchesApiView.as_view(),),
    path('single/shipment/', CustomerSingleShipmentApiView.as_view(),name='singleshipmentcreate'),
    path('single/shipment/<int:pk>/', CustomerSingleShipmentDetailApiView.as_view(),name='singleshipmentdetail'),
    path('single/shipment/update/<int:pk>/', SingleShipmentUpdateApiView.as_view(),name='singleshipssmentdetail'),
]