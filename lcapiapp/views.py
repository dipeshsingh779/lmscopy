from rest_framework_simplejwt.views import TokenObtainPairView
from django.contrib.contenttypes.models import ContentType
from rest_framework.response import Response
from django.contrib.auth import authenticate
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from .utils import *
from .serializers import *
from django.core.mail import send_mail
from django.conf import settings
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.encoding import smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.shortcuts import get_current_site
from rest_framework.pagination import PageNumberPagination
from collections import OrderedDict
from django.urls import reverse
from .utils import Util
from lmsapp.models import *
from umapp.models import *


class CustomerGetTokenPairView(APIView):
    def post(self, request):
        serializer = CustomerLoginSerializer(data=request.data)
        serializer.is_valid()
        email = serializer.validated_data.get("email")
        password = serializer.validated_data.get("password")
        user = authenticate(username=email, password=password)
        try:
            customer = user.logisticscustomer
            resp = get_tokens_for_user(user)
        except Exception as e:
            print(e)
            resp = {
                "message": "Invalid credentials of the customer.."
            }
        return Response(resp)


class CustomerProfileAPIView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def get(self, request):
        serializer = CustomerProfileSerializer(request.user.logisticscustomer)
        resp = {
            "status": "success",
            "data": serializer.data
        }
        return Response(resp)
    
    def patch(self, request):
        customer_obj = LogisticsCustomer.objects.get(id=request.user.logisticscustomer.id)
        if customer_obj:
            serializer = CustomerProfileSerializer(customer_obj, data=request.data, partial = True)
            if serializer.is_valid():
                serializer.save()
                resp = {
                    "status" : "success",
                    "data" : serializer.data
                }
                return Response(resp)
            else:
                resp = {
                    "message" : serializer.data
                }
                return Response(resp)
        else:
            resp = {
                "message" : "Authorization Failed"
            }
            return Response(resp)


class CustomerRegistrationAPIView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        serializer = CustomerRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            customer_email = serializer.validated_data['email']
            serializer.save()
            send_mail(
            'Signup Succesful',
            'Hi! You have successfully signed up in lms system. Thank You for being part of Sarobar Logictics.',
            settings.EMAIL_HOST_USER,
            [customer_email],
            fail_silently=False)
            resp = {
                "status":"success",
                "data":serializer.data
            }
            return Response(resp)
        else:
            resp = {
                "message":serializer.errors
            }
            return Response(resp)


class ChangeCustomerPasswordView(APIView):
    permission_classes = [CustomerOnlyPermission]

    def patch(self, request, *args, **kwargs):
        self.object = request.user
        serializer = ChangePasswordSerializer(data=request.data)
        if serializer.is_valid():
            if not self.object.check_password(serializer.data.get("old_password")):
                resp = {
                    'message' : 'wrong password',
                }
                return Response(resp)
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            resp = {
                'status':'success',
                'data' : serializer.data
            }
            return Response(resp)

        resp = {
            'message' : serializer.errors
        }
        return Response(resp)


class RequestPasswordResetEmail(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        serializer = ResetPasswordEmailRequestSerializer(data=request.data)
        # email = request.data.get('email', '')
        if User.objects.filter(email=request.data.get('email')).exists():
            user = User.objects.get(email=request.data.get('email'))
            uidb64 = urlsafe_base64_encode(smart_bytes(user.id))
            token = PasswordResetTokenGenerator().make_token(user)
            current_site = get_current_site(
                request=request).domain
            relativeLink = reverse(
                'lcapiapp:token-check', kwargs={'uidb64': uidb64, 'token': token})

            absurl = 'http://'+current_site + relativeLink
            email_body = 'Hello, \n Use link below to reset your password  \n' + \
                absurl
            data = {'email_body': email_body, 'to_email': user.email,
                    'email_subject': 'Reset your passsword'}
            Util.send_email(data)
            resp = {
                "status" : "success",
                "message" : 'We have sent you a link to reset your password'
            }
            return Response(resp)
        
        #Fake response for fake user
        resp = {
                "status" : "success",
                "message" : 'We have sent you a link to reset your password'
            }
        return Response(resp)


class PasswordTokenCheckAPI(APIView):

    def get(self, request, uidb64, token):
        try:
            id = smart_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)

            if not PasswordResetTokenGenerator().check_token(user, token):
                resp = {
                    "message" : "Token in not valid please request new one"
                }
                return Response(resp)
            resp = {
                "message" : "Token is Valid"
            }
            return Response(resp)

        except DjangoUnicodeDecodeError as identifier:
            if not PasswordResetTokenGenerator().check_token(user):
                resp = {
                    "message" : "Invalid Token"
                }
                return Response(resp)


class SetNewPasswordAPIView(APIView):
    def patch(self, request):
        serializer = SetNewPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'status': "success", 'message': 'Password reset success'})


class LogisticsCustomerConnectedLogisticsView(APIView):
    permission_classes = [CustomerOnlyPermission ]

    def get(self, request):
        customer_obj = LogisticsCustomer.objects.get(id=request.user.logisticscustomer.id)
        serializer = LogisticsCustomerConnectedLogisticsSerializer(customer_obj)
        resp = {
            "status" : "success", 
            "data" : serializer.data
        }
        return Response(resp)


class CustomerSupportTicketListView(APIView, PageNumberPagination):
    permission_classes = [CustomerOnlyPermission ]
    max_page_size = 10

    def get_paginated_response(self, data, page, page_num):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('current', page),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('page_size', page_num),
            ('results', data)
        ]))

    def get_queryset(self, request):
        branch = self.request.GET.get('branch')
        if branch is not None:
            ticket_obj = CustomerSupportTicket.objects.filter(raised_by=request.user.logisticscustomer).filter(raised_to__branch_city__name__icontains=branch).order_by('-created_at')
        else:
            ticket_obj = CustomerSupportTicket.objects.filter(raised_by=request.user.logisticscustomer).order_by('-created_at')
        return self.paginate_queryset(ticket_obj, self.request)

    def get(self, request):
        page = self.request.GET.get('page', 1)
        page_size = self.request.GET.get('page_size', 1000)
        ticket_list = self.get_queryset(request)
        serializer = CustomerSupportTicketListSerializer(ticket_list, many=True)
        return self.get_paginated_response(serializer.data, page, page_size)
    
    
    def post(self, request):
        serializer = CustomerSupportTicketCreateSerializer(data=request.data)
        if serializer.is_valid():
            customer = request.user.logisticscustomer
            ticket = CustomerSupportTicket.objects.filter(raised_by=customer).last()
            if ticket is not None and ticket.ticket_status == 'Solved':
                ticket = serializer.save(raised_by=request.user.logisticscustomer)
                ticket.status = 'Pending'
                ticket.ticket_status = "Open"
                ticket.save()
                content_type = ContentType.objects.get(
                    app_label='umapp', model='logisticscustomer'
                )
                branch_id = ticket.raised_to.branchoperator_set.all().first().id
                followup_message = CustomerTicketFollowup.objects.create(status = "Pending", ticket=ticket, followup_by_type= content_type, followup_by_id = branch_id, followup_message = "ticket raised...!")
                resp = {
                    "status" : "success",
                    "data" : serializer.data
                }
                return Response(resp)
            elif ticket is None:
                ticket = serializer.save(raised_by=request.user.logisticscustomer)
                ticket.status = 'Pending'
                ticket.ticket_status = "Open"
                ticket.save()
                content_type = ContentType.objects.get(
                    app_label='umapp', model='logisticscustomer'
                )
                branch_id = ticket.raised_to.branchoperator_set.all().first().id
                followup_message = CustomerTicketFollowup.objects.create(status = "Pending", ticket=ticket, followup_by_type= content_type, followup_by_id = branch_id, followup_message = "ticket raised...!")
                resp = {
                    "status" : "success",
                    "data" : serializer.data
                }
                return Response(resp)
            else:
                resp = {
                    "message" : "Ticket Issue not solved yet!!"
                }
                return Response(resp)
        else:
            resp = {
                "message" : serializer.errors
            }
            return Response(resp)


class CustomerSupportTicketView(APIView):
    permission_classes = [CustomerOnlyPermission, ]
    def get(self, request, pk):
        try:
            ticket_obj = CustomerSupportTicket.objects.get(id=pk)
            serializer = CustomerSupportTicketDetailSerializer(ticket_obj)
            
            resp = {
                "status" : "success",
                "data" : serializer.data
            }
        except Exception as e:
            print(e)
            resp = {
                "message" : "Not Found"
            }
        return Response(resp)

    def post(self, request, pk):
        try:
            ticket_obj = CustomerSupportTicket.objects.get(id=pk)
            serializer = CustomerSupportTicketFollowupSerializer(data=request.data)
            if serializer.is_valid():
                message = serializer.data['messages']
                content_type = ContentType.objects.get(
                    app_label='umapp', model='logisticscustomer')
                branch_id = ticket_obj.raised_to.branchoperator_set.all().first().id
                followup_message = CustomerTicketFollowup.objects.create(status = "Pending", ticket=ticket_obj, followup_by_type= content_type, followup_by_id = branch_id, followup_message = message)
                resp = {
                    "status": "ok",
                    "data" : serializer.data
                }
                return Response(resp)
            else:
                resp = {
                    "status": serializer.errors
                }
                return Response(resp)
        except Exception as e:
            print(e)
            resp = {
                "message" : "Not Found"
            }
            return Response(resp)

class LogisticsCompanyListApiView(APIView, PageNumberPagination):
    permission_classes = [CustomerOnlyPermission, ]
    max_page_size = 10

    def get_paginated_response(self, data, page, page_num):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('current', page),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('page_size', page_num),
            ('results', data)
        ]))

    def get_queryset(self, request):
        company_name = self.request.GET.get('company_name')
        if company_name is not None:
            company = LogisticsCompany.objects.filter(company_name__icontains=company_name).order_by('company_name')
        else:
            company = LogisticsCompany.objects.all().order_by('company_name')
        return self.paginate_queryset(company, self.request)

    def get(self, request):
        page = self.request.GET.get('page', 1)
        page_size = self.request.GET.get('page_size', 1000)
        company_list = self.get_queryset(request)
        serializer = LogisticsCompanyListSerializer(company_list, many=True)
        return self.get_paginated_response(serializer.data, page, page_size)


class LogisticsCompanyDetailApiView(APIView):
    permission_classes = [CustomerOnlyPermission, ]

    def get(self, request, pk=None):
        company_obj = LogisticsCompany.objects.get(id=pk)
        serializer = LogisticsCompanyDetailSerializer(company_obj)
        res = {
            "status" : "success",
            "data" : serializer.data
        }
        return Response(res)


class ConnectedLogisticsListApiView(APIView):
    permission_classes = [CustomerOnlyPermission, ]

    def get(self, request):
        branches = request.user.logisticscustomer.connected_logistics.all()
        serializer = ConnectedLogisticsListSerializer(branches, many=True)
        res = {
            "status" : "success",
            "data" : serializer.data
        }
        return Response(res)

class ConnectedLogisticsDetailApiView(APIView):
    permission_classes = [CustomerOnlyPermission, ]

    def get(self, request, pk=None):
        try:
            branch = LogisticsBranch.objects.get(id=pk)
            serializer = ConnectedLogisticsDetailSerializer(branch)
            res = {
                "status" : "success",
                "data" : serializer.data
            }
        except Exception as e:
            print(e)
            res = {
                "message" : "Not Found"
            }
        return Response(res)

class ConnectLogisticsBranchFromCompanyDetailApiView(APIView):
    permission_classes = [CustomerOnlyPermission, ]

    def post(self, request, company_pk=None, branch_pk=None):
        try:
            company = LogisticsCompany.objects.get(id=company_pk)
            branch = company.logisticsbranch_set.all().get(id=branch_pk)
            if branch is not None:
                try:
                    customer_id = request.user.logisticscustomer.id
                    connected = LogisticsCustomer.objects.filter(id=customer_id, connected_logistics=branch)
                    if connected:
                        res = {
                            "status" : "branch already connected"
                        }
                        return Response(res)
                    else:
                        customer = request.user.logisticscustomer.connected_logistics.add(branch)
                        res = {
                            "status" : "success"
                        }
                        return Response(res)
                except Exception as e:
                    print(e)
                    res = {
                            "status" : "error"
                        }
                    return Response(res)
            else:
                res = {
                    "status" : "Not Found"
                }
                return Response(res)
        except Exception as e:
            print(e)
            res  ={
                "message" : "Not Found"
            }
            return Response(res)


class LogisticsCustomerReqestForContractApiView(APIView):
    permission_classes = [CustomerOnlyPermission, ]

    def post(self, request, company_pk=None, branch_pk=None):
        try:
            company = LogisticsCompany.objects.get(id=company_pk)
            branch = company.logisticsbranch_set.all().get(id=branch_pk)
            customer = request.user.logisticscustomer
            if branch is not None:
                if LogisticsCustomerContract.objects.filter(first_party=branch, second_party=customer, status="Active").exists() is False:
                    request_contract = LogisticsCustomerContract.objects.create(first_party=branch, second_party=customer, status='Active')
                    customer.connected_logistics.add(branch)
                    res = {
                        "status" : "success"
                    }
                else:
                    res = {
                        "message" : "contract already exists !!"
                    }
                return Response(res)
            else:
                res = {
                    "message" : "logistics branch is none !"
                }
                return Response(res)
        except Exception as e:
            print(e)
            res = {
                "message" : "Not Found"
            }
            return Response(res)


class LogisticsCustomerQuotationListApiView(APIView):
    permission_classes = [CustomerOnlyPermission, ]

    def get(self, request):
        customer = self.request.user.logisticscustomer
        quotation_list = LogisticsCustomerContract.objects.filter(second_party=customer)
        serializer = LogisticsCustomerQuotationListSerializer(quotation_list, many=True)
        res = {
            "status" : "success",
            "data" : serializer.data
        }
        return Response(res)


class LogisticsCustomerQuotationDetailApiView(APIView):
    permission_classes = [CustomerOnlyPermission, ]

    def get(self, request, pk=None):
        quotation = LogisticsCustomerContract.objects.get(id=pk)
        serializer = LogisticsCustomerQuotationDetailSerializer(quotation)
        res = {
            "status" : "success",
            "data" : serializer.data
        }
        return Response(res)


class LogisticsCustomerAcceptQuotationApiView(APIView):
    permission_classes = [CustomerOnlyPermission, ]

    def post(self, request, contract_id=None):
        contract = LogisticsCustomerContract.objects.get(id=contract_id)
        if contract:
            contract.is_signed = True
            contract.save()
            res = {
                "status" : "success"
            }
        else:
            res = {
                "message" : "contract error"
            }
        return Response(res)


class LogisticsCustomerRejectQuotationApiView(APIView):
    permission_classes = [CustomerOnlyPermission, ]

    def post(self, request, contract_id=None):
        contract = LogisticsCustomerContract.objects.get(id=contract_id)
        if contract:
            contract.is_signed = False
            contract.status = "Disabled"
            contract.save()
            res = {
                "status" : "success"
            }
        else:
            res = {
                "message" : "contract error"
            }
        return Response(res)


class CustomerSingleShipmentApiView(APIView, PageNumberPagination):
    permission_classes = [CustomerOnlyPermission, ]
    max_page_size = 10

    def get_paginated_response(self, data, page, page_num):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('current', page),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('page_size', page_num),
            ('results', data)
        ]))

    def get_queryset(self, request):
        city = self.request.GET.get('city')
        if city is not None:
            shipment = Shipment.objects.filter(receiver_city__name__icontains=city)
        else:
            shipment = Shipment.objects.all()
        return self.paginate_queryset(shipment, self.request)

    def get(self, request):
        page = self.request.GET.get('page', 1)
        page_size = self.request.GET.get('page_size', 1000)
        shipment_list = self.get_queryset(request)
        serializer = ShipmentListSerializer(shipment_list, many=True)
        return self.get_paginated_response(serializer.data, page, page_size)

    def post(self, request):
        customer = self.request.user.logisticscustomer
        serializer = ShipmentCreateSerializer(data=request.data)
        if serializer.is_valid():
            shipment = serializer.save(customer=customer, sender_city=customer.city)
            shipment.sender_name = customer.full_name
            shipment.status = "Active"
            shipment.sender_address = customer.address
            shipment.sender_contact_number = customer.contact_number
            shipment.sender_email = request.user.email
            shipment.tracking_code = 'tracking ' + str(Shipment.objects.all().count() + 1)
            # shippingzone = LogisticsBranchShippingZone.objects.filter(shipping_zone='Standard', cities=request.data['receiver_city']).last()
            # print(shippingzone)
            # delivery_type = ShippingZoneDeliveryType.objects.filter(shippng_zone=shippingzone)
            # print(delivery_type)
            # shipment.shipping_charge = delivery_type.shipping_charge_per_kg
            # shipment.cod_handling_charge = delivery_type.cod_handling_charge
            # shipment.rejection_handling_charge =delivery_type.rejection_handling_charge
            # shipment.extra_charge = delivery_type.additional_shipping_charge_per_kg
            shipment.save()
            ShipmentActivity.objects.create(status='Active', shipment=shipment, shipment_status='Shipment Requested', activity='Shipment created by customer')
            content_type = ContentType.objects.get(
                    app_label='umapp', model='logisticscustomer'
                )
            ShipmentRemark.objects.create(status='Active',shipment=shipment, remarks='shipment created by customer', remarks_by_type=content_type, remarks_by_id=customer.id)
            res = {
                'status' : "success",
                'data' : serializer.data
            }
        else:
            print("invalid")
            res = {
                'message' : serializer.errors
            }
        return Response(res)

    

class SingleShipmentUpdateApiView(APIView):
    permission_classes = [CustomerOnlyPermission, ]

    def patch(self, request, pk=None):
        shipment = Shipment.objects.get(id=pk)
        serializer = ShipmentPartialUpdateSerializer(shipment, data=request.data, partial=True)
        if serializer.is_valid():
            shipment = serializer.save()
            content_type = ContentType.objects.get(
                    app_label='umapp', model='logisticscustomer'
                )
            ShipmentRemark.objects.create(status='Active',shipment=shipment, remarks='shipment updated by customer', remarks_by_type=content_type, remarks_by_id=request.user.logisticscustomer.id)
            res = {
                'status' : 'success',
                'data' : serializer.data
            }
        else:
            res = {
                'message' : serializer.errors
            }
        return Response(res)



class CustomerSingleShipmentDetailApiView(APIView):
    permission_classes = [CustomerOnlyPermission, ]

    def get(self, request, pk=None):
        try:
            shipment = Shipment.objects.get(id=pk)
            serializer = ShipmentDetailSerializer(shipment)
            res = {
                'status' : 'success',
                'serializer' : serializer.data
            }
        except Exception as e:
            print(e)
            res = {
                'message' : 'Not Found'
            }
        return Response(res)

class GetRelatedBranchesApiView(APIView):
    permission_classes = [CustomerOnlyPermission, ]

    def get(self, request, pk=None):
        city = City.objects.get(id=pk)
        connected_branches = request.user.logisticscustomer.connected_logistics.all()
        shipping_zones = city.logisticsbranchshippingzone_set.all()
        branches = []
        for sz in shipping_zones.all():
            if sz.logistics_branch in connected_branches:
                branches.append({
                    'id' : sz.logistics_branch.id,
                    'name' : sz.logistics_branch.logistics_company.company_name,
                    'shipping_zone_id' : sz.id,
                    'shipping_zone_name' : sz.shipping_zone,
                    'delivery_types':[{'id': dt.id,'shipping_charge_per_kg': dt.shipping_charge_per_kg} for dt in sz.shippingzonedeliverytype_set.all()]
                })
        res = {
            'status':'success',
            'branchs': branches
        }
        return Response(res)