from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.contrib.auth.models import User
from rest_framework import serializers
from umapp.models import LogisticsCustomer, LogisticsBranch, LogisticsCompany, City, LogisticsCustomerContract, LogisticCustomerZonalContract, LogisticsBranchShippingZone, \
ShippingZoneDeliveryType
from lmsapp.models import CustomerSupportTicket, CustomerTicketFollowup,Shipment
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.contrib.contenttypes.models import ContentType



class CustomerLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()


class CustomerProfileSerializer(serializers.ModelSerializer):
    email = serializers.SerializerMethodField()
    city = serializers.StringRelatedField()
    creator_branch = serializers.StringRelatedField()

    class Meta:
        model = LogisticsCustomer
        fields = ["email", "customer_type", "full_name", "company_name", "contact_number", "alternative_contact_number", "city", "address", "creator_branch", "connected_logistics"]

    def get_email(self, obj):
        return obj.user.username


class CustomerRegistrationSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    password = serializers.CharField(min_length = 4, write_only=True, style={'input_type':'password'})
    class Meta:
        model = LogisticsCustomer
        fields = ['id','customer_type', 'full_name', 'company_name', 'contact_number', 'alternative_contact_number', 'city', 'address', 'document1', 'document2', 'document3', 'email', 'password']
    
    def create(self, validated_data):
        email = validated_data.get('email')
        user = User.objects.create_user(username=email, email=email)
        if user is not None:
            customer = LogisticsCustomer.objects.create(user=user,status='Active',customer_type=validated_data['customer_type'], full_name=validated_data['full_name'], company_name=validated_data['company_name'], contact_number=validated_data['contact_number'], alternative_contact_number=validated_data['alternative_contact_number'], city=validated_data['city'], address=validated_data['address'], document1=validated_data['document1'], document2=validated_data['document2'], document3=validated_data['document3'])
            user.set_password(validated_data['password'])
            user.save()
            return validated_data
        else:
            raise serializers.ValidationError({'incorrect credentials'})
    
    def validate(self, attrs):
        email = attrs['email']
        if User.objects.filter(email=email).exists():
            raise serializers.ValidationError({"email":"email already exists"})
        return attrs
    
    def validate_password(self, value):
        if value.isalnum():
            raise serializers.ValidationError('password must have atleast one special character.')
        return value


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True, min_length = 4)
    new_password = serializers.CharField(required=True, min_length = 4)
    confirm_new_password = serializers.CharField(required=True, min_length = 4)

    def validate(self, attrs):
        if attrs['new_password'] != attrs['confirm_new_password']:
            raise serializers.ValidationError({'new_password':"password didn't match !!"})
        return attrs


class ResetPasswordEmailRequestSerializer(serializers.Serializer):
    email = serializers.EmailField(min_length=2)
    class Meta:
        fields = ['email']


class SetNewPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(
        min_length=6, max_length=68, write_only=True)
    token = serializers.CharField(
        min_length=1, write_only=True)
    uidb64 = serializers.CharField(
        min_length=1, write_only=True)

    class Meta:
        fields = ['password', 'token', 'uidb64']

    def validate(self, attrs):
        try:
            password = attrs.get('password')
            token = attrs.get('token')
            uidb64 = attrs.get('uidb64')

            id = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)
            if not PasswordResetTokenGenerator().check_token(user, token):
                raise AuthenticationFailed('The reset link is invalid', 401)

            user.set_password(password)
            user.save()

            return (user)
        except Exception as e:
            raise serializers.ValidationError('The reset link is invalid')
        return super().validate(attrs)


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsCompany
        fields = ['company_name', 'company_logo']

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ['name']

class LogisticsBranchSerializer(serializers.ModelSerializer):
    branch_city = CitySerializer(read_only=True)
    logistics_company = CompanySerializer(read_only=True)

    class Meta:
        model = LogisticsBranch
        fields = ['branch_city', 'logistics_company', 'branch_contact', 'branch_address']


class LogisticsCustomerConnectedLogisticsSerializer(serializers.ModelSerializer):
    connected_logistics = LogisticsBranchSerializer(read_only=True, many=True)
    class Meta:
        model = LogisticsCustomer
        fields = ['id', 'connected_logistics']


class CustomerTicketFollowupSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerTicketFollowup
        fields = ['followup_by_id','followup_by_type','followup_message']
        depth = 1


class CustomerSupportTicketListSerializer(serializers.ModelSerializer):
    raised_to = LogisticsBranchSerializer(read_only=True)
    class Meta:
        model = CustomerSupportTicket
        fields = ['id','ticket_status', 'raised_to', 'issue_detail', 'created_at', 'related_file']


class CustomerSupportTicketDetailSerializer(serializers.ModelSerializer):
    messages = CustomerTicketFollowupSerializer(many=True, source="customerticketfollowup_set", read_only=True)
    raised_to = LogisticsBranchSerializer(read_only=True)
    class Meta:
        model = CustomerSupportTicket
        fields = ['id','ticket_status', 'raised_to', 'issue_detail', 'created_at', 'related_file', 'messages']
    

class CustomerSupportTicketCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerSupportTicket
        fields = ['raised_to', 'issue_detail', 'related_file']


class CustomerSupportTicketFollowupSerializer(serializers.Serializer):
    messages = serializers.CharField(min_length=5)


class LogisticsBranchForCompanySerializer(serializers.ModelSerializer):
    branch_city = CitySerializer(read_only=True)

    class Meta:
        model = LogisticsBranch
        fields = ['branch_city']


class LogisticsCompanyListSerializer(serializers.ModelSerializer):
    branch = LogisticsBranchForCompanySerializer(source='logisticsbranch_set',many=True)
    class Meta:
        model = LogisticsCompany
        fields = ['id', 'company_name', 'company_logo', 'branch']


class LogisticsBranchForCompanyDetailSerializer(serializers.ModelSerializer):
    branch_city = CitySerializer(read_only=True)

    class Meta:
        model = LogisticsBranch
        fields = ['id','branch_city', 'branch_contact', 'branch_address', 'is_main_branch']

class LogisticsCompanyDetailSerializer(serializers.ModelSerializer):
    branch = LogisticsBranchForCompanyDetailSerializer(source='logisticsbranch_set',many=True)
    class Meta:
        model = LogisticsCompany
        fields = ['id', 'company_name', 'company_logo', 'branch']


class LogisticsCompanyForConnectedLogisticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsCompany
        fields = ['id', 'company_name']


class ConnectedLogisticsListSerializer(serializers.ModelSerializer):
    branch_city = CitySerializer(read_only=True)
    logistics_company = LogisticsCompanyForConnectedLogisticsSerializer(read_only=True)
    class Meta:
        model = LogisticsBranch
        fields = ['id', 'logistics_company', 'branch_city']


class ConnectedLogisticsDetailSerializer(serializers.ModelSerializer):
    branch_city = CitySerializer(read_only=True)
    logistics_company = LogisticsCompanyForConnectedLogisticsSerializer(read_only=True)
    class Meta:
        model = LogisticsBranch
        fields = ['id', 'logistics_company', 'branch_city', 'branch_contact', 'branch_alternative_contact', 'branch_email', 'branch_address', 'is_main_branch']

class LogisticsCustomerQuotationListSerializer(serializers.ModelSerializer):
    first_party = LogisticsBranchForCompanySerializer(read_only=True)
    class Meta:
        model = LogisticsCustomerContract
        fields = ['id', 'first_party', 'is_signed']


class LogisticsBranchShippingZoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsBranchShippingZone
        fields = ['id','shipping_zone']


class LogisticCustomerZonalContractSerializer(serializers.ModelSerializer):
    shipping_zone = LogisticsBranchShippingZoneSerializer(read_only=True)
    class Meta:
        model = LogisticCustomerZonalContract
        fields = ['id','shipping_zone', 'shipping_charge', 'cod_handling_charge_pct', 'rejection_handling_charge_pct', 'is_confirmed']


class LogisticsCustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsCustomer
        fields = ['full_name']



class LogisticsCustomerQuotationDetailSerializer(serializers.ModelSerializer):
    first_party = LogisticsBranchForCompanySerializer(read_only=True)
    second_party = LogisticsCustomerSerializer(read_only=True)
    zonal_contract = LogisticCustomerZonalContractSerializer(source="logisticcustomerzonalcontract_set", many=True, read_only=True)
    class Meta:
        model = LogisticsCustomerContract
        fields = ['id', 'first_party','second_party','is_signed', 'zonal_contract']

class LogisticsBranchForShipmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogisticsBranch
        fields = ['logistics_company', 'branch_city']


class ShipmentListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipment
        fields = ['id','tracking_code', 'cod_info', 'receiver_name']


class ShipmentDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipment
        fields = ['id', 'sender_name', 'sender_city', 'sender_address', 'sender_contact_number', 'sender_email', 
        'receiver_name', 'receiver_city', 'receiver_address', 'receiver_contact_number', 'tracking_code', 'parcel_origin', 
        'created_at', 'weight', 'length', 'breadth', 'height', 'parcel_type', 'delivery_type', 'cod_info', 'cod_value', 
        'pickup_charge', 'dropoff_charge', 'shipping_charge', 'cod_handling_charge', 'rejection_handling_charge', 
        'extra_charge']


class ShipmentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipment
        fields = ['weight', 'weight_unit', 'length', 'length_unit', 'breadth',
        'height', 'parcel_type', 'cod_info', 'request_pickup',
        'receiver_name', 'receiver_city', 'receiver_address', 'receiver_contact_number',
        'request_home_delivery', 'delivery_type', 'parcel_origin']


class ShipmentPartialUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipment
        fields = ['receiver_name', 'receiver_city', 'receiver_address', 'receiver_contact_number']
