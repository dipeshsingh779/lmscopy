from django.apps import AppConfig


class LcapiappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lcapiapp'
